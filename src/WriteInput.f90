SUBROUTINE WriteInput
  
    USE IntrType
    USE BlockStr
    USE PriVars
    USE File

  IMPLICIT NONE
  
! THE PURPOSE OF THIS ROUTINE IS to calculate the residual for continuity.
!
! Programmed by ;
!   NAME         : Tim Drzewiecki 
!   ORGANIZATION : University of Michigan  
!   DATE         : 05/10/2012
!
! Subroutine Argument Descriptions:
! 
! Variable Declarations;
!   Subroutine Arguments:
!   Local Variables:
    INTEGER(sik) :: i
    INTEGER(sik) :: j
    INTEGER(sik) :: vidx
    INTEGER(sik) :: jidx

    OPEN(OutFile,file=OutFileName, status='replace')  
  
    WRITE(OutFile,'(a100)') '*************************************** Fluid Block Input ***************************************'
    DO i=1,numFB
        WRITE(OutFile,'(2a10,a15)') 'ID','Cells','Roughness'
        WRITE(OutFile,'(a10,i10,ES15.4)')  trim(FB(i)%id),FB(i)%cells,FB(i)%roughness
        WRITE(OutFile,'(a4,5a12,2a14,a12,a7)') 'Cell','Volume','Area','Length','Height','HydDia','Pm','hm','VF','idx'
        DO j=1,FB(i)%cells
            vidx=FB(i)%vidx(j)
            WRITE(OutFile,'(i4,5ES12.4,2ES14.6,ES12.4,i7)') j,Volume(vidx),VArea(vidx),Length(vidx),Height(vidx),HydDia(vidx),Pm(vidx),hm(vidx),alpha(vidx),vidx
        END DO   
        WRITE(OutFile,'(a4,5a12)') 'Jun','jform','Kloss','JFlag','UW','idx'
        DO j=1,(FB(i)%cells-1_sik)
            jidx=FB(i)%jidx(j)
            WRITE(OutFile,'(i4,a12,ES12.4,a12,ES12.4,i12)') j,JForm(jidx),KFloss(jidx),FB(i)%jflag(j),FB(i)%UW(j),jidx
        END DO      
        WRITE(OutFile,'(a1)') ' '    
    END DO

    WRITE(OutFile,'(a100)') '************************************* Boundary Block Input **************************************'
    DO i=1,numBB
        WRITE(OutFile,'(6a10)') 'ID','Type','Pm','hm','VF','vidx'
        WRITE(OutFile,'(2a10,4i10)') trim(BB(i)%id),trim(BB(i)%bType),BB(i)%Pmff,BB(i)%hmff,BB(i)%alphaff,BB(i)%vidx
        WRITE(OutFile,'(a1)') ' ' 
    END DO

    WRITE(OutFile,'(a100)') '************************************* Junction Block Input **************************************'
    DO i=1,numJB
        jidx=JB(i)%jidx
        WRITE(OutFile,'(7a10)') 'ID','InFace','InId','ExFace','ExId','BFlag','UWFF'
        WRITE(OutFile,'(6a10,i10)') trim(JB(i)%id),trim(JB(i)%inFace),trim(JB(i)%inID),trim(JB(i)%exFace),trim(JB(i)%exID),trim(JB(i)%BFlag),JB(i)%uwff
        WRITE(OutFile,'(7a10)') 'JForm','KFLoss','KRLoss','JFlag','UW','jidx','Class'
        WRITE(OutFile,'(a10,2F10.4,a10,F10.4,2i10)') JForm(jidx),KFLoss(jidx),KRLoss(jidx),trim(JB(i)%jflag),JB(i)%UW,jidx,JuncClass(jidx)
        WRITE(OutFile,'(a1)') ' ' 
    END DO
           
    CLOSE(OutFile)

END SUBROUTINE WriteInput
