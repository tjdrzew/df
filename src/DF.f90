PROGRAM DF
! @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  VERSION 1.0  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
! @                                                                                   @
! @                                   August 2012                                     @
! @                                                                                   @
! @                        A Drift Flux Subchannel Code                               @
! @                                                                                   @
! @ Developed by  Tim Drzewiecki                                                      @
! @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

IMPLICIT NONE

!   1. READ INPUT FILE
    CALL ReadInput

!   2. ALLOCATE MEMORY FOR PRIMARY VARIABLES
    CALL PriVarsAlloc

!   3. PROCESS INPUT
    CALL ProcInput

!   4. PROBLEM INITIALIZATION
    CALL Init

!   5. WRITE INPUT 
    CALL WriteInput

!   6. CALL SOLVER
    CALL Solve

!   7. WRITE OUTPUT 
    CALL WriteOutput

END PROGRAM DF
