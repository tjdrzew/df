MODULE SteamProp
!
    USE IntrType

    IMPLICIT NONE

    REAL(sdk), PARAMETER :: R=0.461526_sdk !kJ/kg-K
    REAL(sdk), PARAMETER :: Tc=647.096_sdk !K
    REAL(sdk), PARAMETER :: pc=22.064_sdk  !MPa
    REAL(sdk), PARAMETER :: rhoc=322.0_sdk !kg/m^3

    REAL(sdk),DIMENSION(5) :: nR2tR3

    DATA nR2tR3 / 0.34805185628969E3_sdk,  &
                 -0.11671859879975E1_sdk,  &
                  0.10192970039326E-2_sdk, &
                  0.57254459862746E3_sdk,  &
                  0.13918839778870E2_sdk/

    REAL(sdk), PARAMETER :: psR1=16.53_sdk  !MPa
    REAL(sdk), PARAMETER :: TsR1=1386.0_sdk !K

    INTEGER(sik),DIMENSION(34) :: IR1
    INTEGER(sik),DIMENSION(34) :: JR1
    REAL(sdk),DIMENSION(34)    :: nR1

    DATA IR1 /  0_sik, 0_sik, 0_sik, 0_sik, 0_sik, 0_sik, 0_sik, 0_sik, 1_sik, 1_sik, &
               1_sik, 1_sik, 1_sik, 1_sik, 2_sik, 2_sik, 2_sik, 2_sik, 2_sik, 3_sik, &
               3_sik, 3_sik, 4_sik, 4_sik, 4_sik, 5_sik, 8_sik, 8_sik,21_sik,23_sik, &
              29_sik,30_sik,31_sik,32_sik /

    DATA JR1 / -2_sik, -1_sik,  0_sik,  1_sik,  2_sik,  3_sik,  4_sik,  5_sik, -9_sik, -7_sik, &
              -1_sik,  0_sik,  1_sik,  3_sik, -3_sik,  0_sik,  1_sik,  3_sik, 17_sik, -4_sik, &
               0_sik,  6_sik, -5_sik, -2_sik, 10_sik, -8_sik,-11_sik, -6_sik,-29_sik,-31_sik, &
             -38_sik,-39_sik,-40_sik,-41_sik /

    DATA nR1  / 0.14632971213167_sdk,    &
               -0.84548187169114_sdk,    &
               -0.37563603672040E1_sdk,  &
                0.33855169168385E1_sdk,  &
               -0.95791963387872_sdk,    &
                0.15772038513228_sdk,    &
               -0.16616417199501E-1_sdk, &
                0.81214629983568E-3_sdk, &
                0.28319080123804E-3_sdk, &
               -0.60706301565874E-3_sdk, &
               -0.18990068218419E-1_sdk, &
               -0.32529748770505E-1_sdk, &
               -0.21841717175414E-1_sdk, &
               -0.52838357969930E-4_sdk, &
               -0.47184321073267E-3_sdk, &
               -0.30001780793026E-3_sdk, &
                0.47661393906987E-4_sdk, &
               -0.44141845330846E-5_sdk, &
               -0.72694996297594E-15_sdk,&
               -0.31679644845054E-4_sdk, &
               -0.28270797985312E-5_sdk, &
               -0.85205128120103E-9_sdk, &
               -0.22425281908000E-5_sdk, &
               -0.65171222895601E-6_sdk, &
               -0.14341729937924E-12_sdk,&
               -0.40516996860117E-6_sdk, &
               -0.12734301741641E-8_sdk, &
               -0.17424871230634E-9_sdk, &
               -0.68762131295531E-18_sdk,&
                0.14478307828521E-19_sdk,&
                0.26335781662795E-22_sdk,&
               -0.11947622640071E-22_sdk,&
                0.18228094581404E-23_sdk,&
               -0.93537087292458E-25_sdk /

    REAL(sdk), PARAMETER :: hsR1=2500.0_sdk !kJ/kg

    INTEGER(sik),DIMENSION(20) :: TIR1
    INTEGER(sik),DIMENSION(20) :: TJR1
    REAL(sdk),DIMENSION(20)    :: TnR1

    DATA TIR1 / 0_sik, 0_sik, 0_sik, 0_sik, 0_sik, 0_sik, 1_sik, 1_sik, 1_sik, 1_sik, &
                1_sik, 1_sik, 1_sik, 2_sik, 2_sik, 3_sik, 3_sik, 4_sik, 5_sik, 6_sik /

    DATA TJR1 / 0_sik,  1_sik,  2_sik,  6_sik, 22_sik, 32_sik,  0_sik,  1_sik,  2_sik,  3_sik, &
                4_sik, 10_sik, 32_sik, 10_sik, 32_sik, 10_sik, 32_sik, 32_sik, 32_sik, 32_sik /

    DATA TnR1 / -0.23872489924521E3_sdk,  &
                 0.40421188637945E3_sdk,  &
                 0.11349746881718E3_sdk,  &
                -0.58457616048039E1_sdk,  &
                -0.15285482413140E-3_sdk, &
                -0.10866707695377E-5_sdk, &
                -0.13391744872602E2_sdk,  &
                 0.43211039183559E2_sdk,  &
                -0.54010067170506E2_sdk,  &
                 0.30535892203916E2_sdk,  &
                -0.65964749423638E1_sdk,  &
                 0.93965400878363E-2_sdk, &
                 0.11573647505340E-6_sdk, &
                -0.25858641282073E-4_sdk, &
                -0.40644363084799E-8_sdk, &
                 0.66456186191635E-7_sdk, &
                 0.80670734103027E-10_sdk,&
                -0.93477771213947E-12_sdk,&
                 0.58265442020601E-14_sdk,&
                -0.15020185953503E-16_sdk /

    REAL(sdk), PARAMETER :: psR2=1.0_sdk   !MPa
    REAL(sdk), PARAMETER :: TsR2=540.0_sdk !K

    INTEGER(sik),DIMENSION(9) :: J0R2
    REAL(sdk),DIMENSION(9)    :: n0R2

    DATA J0R2 / 0_sik, 1_sik,-5_sik,-4_sik,-3_sik,-2_sik,-1_sik, 2_sik, 3_sik /
    DATA n0R2 / -0.96927686500217E1_sdk,  &
                 0.10086655968018E2_sdk,  &
                -0.56087911283020E-2_sdk, &
                 0.71452738081455E-1_sdk, &
                -0.40710498223928_sdk,    &
                 0.14240819171444E1_sdk,  &
                -0.43839511319450E1_sdk,  &
                -0.28408632460772_sdk,    &
                 0.21268463753307E-1_sdk /

    INTEGER(sik),DIMENSION(43) :: IrR2
    INTEGER(sik),DIMENSION(43) :: JrR2
    REAL(sdk),DIMENSION(43)    :: nrR2

    DATA IrR2 / 1_sik, 1_sik, 1_sik, 1_sik, 1_sik, 2_sik, 2_sik, 2_sik, 2_sik, 2_sik, &
                3_sik, 3_sik, 3_sik, 3_sik, 3_sik, 4_sik, 4_sik, 4_sik, 5_sik, 6_sik, &
                6_sik, 6_sik, 7_sik, 7_sik, 7_sik, 8_sik, 8_sik, 9_sik,10_sik,10_sik, &
               10_sik,16_sik,16_sik,18_sik,20_sik,20_sik,20_sik,21_sik,22_sik,23_sik, &
               24_sik,24_sik,24_sik /

    DATA JrR2 / 0_sik, 1_sik, 2_sik, 3_sik, 6_sik, 1_sik, 2_sik, 4_sik, 7_sik,36_sik, &
                0_sik, 1_sik, 3_sik, 6_sik,35_sik, 1_sik, 2_sik, 3_sik, 7_sik, 3_sik, &
               16_sik,35_sik, 0_sik,11_sik,25_sik, 8_sik,36_sik,13_sik, 4_sik,10_sik, &
               14_sik,29_sik,50_sik,57_sik,20_sik,35_sik,48_sik,21_sik,53_sik,39_sik, &
               26_sik,40_sik,58_sik /

    DATA nrR2 / -0.17731742473213E-2_sdk,  &
                -0.17834862292358E-1_sdk,  &
                -0.45996013696365E-1_sdk,  &
                -0.57581259083432E-1_sdk,  &
                -0.50325278727930E-1_sdk,  &
                -0.33032641670203E-4_sdk,  &
                -0.18948987516315E-3_sdk,  &
                -0.39392777243355E-2_sdk,  &
                -0.43797295650573E-1_sdk,  &
                -0.26674547914087E-4_sdk,  &
                 0.20481737692309E-7_sdk,  &
                 0.43870667284435E-6_sdk,  &
                -0.32277677238570E-4_sdk,  &
                -0.15033924542148E-2_sdk,  &
                -0.40668253562649E-1_sdk,  &
                -0.78847309559367E-9_sdk,  &
                 0.12790717852285E-7_sdk,  &
                 0.48225372718507E-6_sdk,  &
                 0.22922076337661E-5_sdk,  &
                -0.16714766451061E-10_sdk, &
                -0.21171472321355E-2_sdk,  &
                -0.23895741934104E2_sdk,   &
                -0.59059564324270E-17_sdk, &
                -0.12621808899101E-5_sdk,  &
                -0.38946842435739E-1_sdk,  &
                 0.11256211360459E-10_sdk, &
                -0.82311340897998E1_sdk,   &
                 0.19809712802088E-7_sdk,  &
                 0.10406965210174E-18_sdk, &
                -0.10234747095929E-12_sdk, &
                -0.10018179379511E-8_sdk,  &
                -0.80882908646985E-10_sdk, &
                 0.10693031879409_sdk,     &
                -0.33662250574171_sdk,     &
                 0.89185845355421E-24_sdk, &
                 0.30629316876232E-12_sdk, &
                -0.42002467698208E-5_sdk,  &
                -0.59056029685639E-25_sdk, &
                 0.37826947613457E-5_sdk,  &
                -0.12768608934681E-14_sdk, &
                 0.73087610595061E-28_sdk, &
                 0.55414715350778E-16_sdk, &
                -0.94369707241210E-6_sdk /

    REAL(sdk),DIMENSION(5) :: nR2btR2c

    DATA nR2btR2c / 0.90584278514723E3_sdk,  &
                   -0.67955786399241_sdk,    &
                    0.12809002730136E-3_sdk, &
                    0.26526571908428E4_sdk,  &
                    0.45257578905948E1_sdk /

    INTEGER(sik),DIMENSION(34) :: TIR2a
    INTEGER(sik),DIMENSION(34) :: TJR2a
    REAL(sdk),DIMENSION(34)    :: TnR2a

    DATA TIR2a / 0_sik, 0_sik, 0_sik, 0_sik, 0_sik, 0_sik, 1_sik, 1_sik, 1_sik, 1_sik, &
                 1_sik, 1_sik, 1_sik, 1_sik, 1_sik, 2_sik, 2_sik, 2_sik, 2_sik, 2_sik, &
                 2_sik, 2_sik, 2_sik, 3_sik, 3_sik, 4_sik, 4_sik, 4_sik, 5_sik, 5_sik, &
                 5_sik, 6_sik, 6_sik, 7_sik /

    DATA TJR2a / 0_sik, 1_sik, 2_sik, 3_sik, 7_sik,20_sik, 0_sik, 1_sik, 2_sik, 3_sik, &
                 7_sik, 9_sik,11_sik,18_sik,44_sik, 0_sik, 2_sik, 7_sik,36_sik,38_sik, &
                40_sik,42_sik,44_sik,24_sik,44_sik,12_sik,32_sik,44_sik,32_sik,36_sik, &
                42_sik,34_sik,44_sik,28_sik /

    DATA TnR2a / 0.10898952318288E4_sdk, &
                 0.84951654495535E3_sdk, &
                -0.10781748091826E3_sdk, &
                 0.33153654801263E2_sdk, &
                -0.74232016790248E1_sdk, &
                 0.11765048724356E2_sdk, &
                 0.18445749355790E1_sdk, &
                -0.41792700549624E1_sdk, &
                 0.62478196935812E1_sdk, &
                -0.17344563108114E2_sdk, &
                -0.20058176862096E3_sdk, &
                 0.27196065473796E3_sdk, &
                -0.45511318285818E3_sdk, &
                 0.30919688604755E4_sdk, &
                 0.25226640357872E6_sdk, &
                -0.61707422868339E-2_sdk,&
                -0.31078046629583_sdk,   &
                 0.11670873077107E2_sdk, &
                 0.12812798404046E9_sdk, &
                -0.98554909623276E9_sdk, &
                 0.28224546973002E10_sdk, &
                -0.35948971410703E10_sdk, &
                 0.17227349913197E10_sdk, &
                -0.13551334240775E5_sdk, &
                 0.12848734664650E8_sdk, &
                 0.13865724283226E1_sdk, &
                 0.23598832556514E6_sdk, &
                -0.13105236545054E8_sdk, &
                 0.73999835474766E4_sdk, &
                -0.55196697030060E6_sdk, &
                 0.37154085996233E7_sdk, &
                 0.19127729239660E5_sdk, &
                -0.41535164835634E6_sdk, &
                -0.62459855192507E2_sdk /

    INTEGER(sik),DIMENSION(38) :: TIR2b
    INTEGER(sik),DIMENSION(38) :: TJR2b
    REAL(sdk),DIMENSION(38)    :: TnR2b

    DATA TIR2b / 0_sik, 0_sik, 0_sik, 0_sik, 0_sik, 0_sik, 0_sik, 0_sik, 1_sik, 1_sik, &
                 1_sik, 1_sik, 1_sik, 1_sik, 1_sik, 1_sik, 2_sik, 2_sik, 2_sik, 2_sik, &
                 3_sik, 3_sik, 3_sik, 3_sik, 4_sik, 4_sik, 4_sik, 4_sik, 4_sik, 4_sik, &
                 5_sik, 5_sik, 5_sik, 6_sik, 7_sik, 7_sik, 9_sik, 9_sik /


    DATA TJR2b / 0_sik, 1_sik, 2_sik,12_sik,18_sik,24_sik,28_sik,40_sik, 0_sik, 2_sik, &
                 6_sik,12_sik,18_sik,24_sik,28_sik,40_sik, 2_sik, 8_sik,18_sik,40_sik, &
                 1_sik, 2_sik,12_sik,24_sik, 2_sik,12_sik,18_sik,24_sik,28_sik,40_sik, &
                18_sik,24_sik,40_sik,28_sik, 2_sik,28_sik, 1_sik,40_sik /

    DATA TnR2b / 0.14895041079516E4_sdk,  &
                 0.74307798314034E3_sdk,  &
                -0.97708318797837E2_sdk,  &
                 0.24742464705674E1_sdk,  &
                -0.63281320016026_sdk,    &
                 0.11385952129658E1_sdk,  &
                -0.47811863648625_sdk,    &
                 0.85208123431544E-2_sdk, &
                 0.93747147377932_sdk,    &
                 0.33593118604916E1_sdk,  &
                 0.33809355601454E1_sdk,  &
                 0.16844539671904_sdk,    &
                 0.73875745236695_sdk,    &
                -0.47128737436186_sdk,    &
                 0.15020273139707_sdk,    &
                -0.21764114219750E-2_sdk, &
                -0.21810755324761E-1_sdk, &
                -0.10829784403677_sdk,    &
                -0.46333324635812E-1_sdk, &
                 0.71280351959551E-4_sdk, &
                 0.11032831789999E-3_sdk, &
                 0.18955248387902E-3_sdk, &
                 0.30891541160537E-2_sdk, &
                 0.13555504554949E-2_sdk, &
                 0.28640237477456E-6_sdk, &
                -0.10779857357512E-4_sdk, &
                -0.76462712454814E-4_sdk, &
                 0.14052392818316E-4_sdk, &
                -0.31083814331434E-4_sdk, &
                -0.10302738212103E-5_sdk, &
                 0.28217281635040E-6_sdk, &
                 0.12704902271945E-5_sdk, &
                 0.73803353468292E-7_sdk, &
                -0.11030139238909E-7_sdk, &
                -0.81456365207833E-13_sdk,&
                -0.25180545682962E-10_sdk,&
                -0.17565233969407E-17_sdk,&
                 0.86934156344163E-14_sdk /

    INTEGER(sik),DIMENSION(23) :: TIR2c
    INTEGER(sik),DIMENSION(23) :: TJR2c
    REAL(sdk),DIMENSION(23)    :: TnR2c

    DATA TIR2c /-7_sik,-7_sik,-6_sik,-6_sik,-5_sik,-5_sik,-2_sik,-2_sik,-1_sik,-1_sik, &
                 0_sik, 0_sik, 1_sik, 1_sik, 2_sik, 6_sik, 6_sik, 6_sik, 6_sik, 6_sik, &
                 6_sik, 6_sik, 6_sik /

    DATA TJR2c / 0_sik, 4_sik, 0_sik, 2_sik, 0_sik, 2_sik, 0_sik, 1_sik, 0_sik, 2_sik, &
                 0_sik, 1_sik, 4_sik, 8_sik, 4_sik, 0_sik, 1_sik, 4_sik,10_sik,12_sik, &
                16_sik,20_sik,22_sik /

    DATA TnR2c / -0.32368398555242E13_sdk, &
                  0.73263350902181E13_sdk, &
                  0.35825089945447E12_sdk, &
                 -0.58340131851590E12_sdk, &
                 -0.10783068217470E11_sdk, &
                  0.20825544563171E11_sdk, &
                  0.61074783564516E6_sdk,  &
                  0.85977722535580E6_sdk,  &
                 -0.25745723604170E5_sdk,  &
                  0.31081088422714E5_sdk,  &
                  0.12082315865936E4_sdk,  &
                  0.48219755109255E3_sdk,  &
                  0.37966001272486E1_sdk,  &
                 -0.10842984880077E2_sdk,  &
                 -0.45364172676660E-1_sdk, &
                  0.14559115658698E-12_sdk,&
                  0.11261597407230E-11_sdk,&
                 -0.17804982240686E-10_sdk,&
                  0.12324579690832E-6_sdk, &
                 -0.11606921130984E-5_sdk, &
                  0.27846367088554E-4_sdk, &
                 -0.59270038474176E-3_sdk, &
                  0.12918582991878E-2_sdk /

    INTEGER(sik),DIMENSION(40) :: IR3
    INTEGER(sik),DIMENSION(40) :: JR3
    REAL(sdk),DIMENSION(40)    :: nR3

    DATA IR3 /  0_sik, 0_sik, 0_sik, 0_sik, 0_sik, 0_sik, 0_sik, 0_sik, 1_sik, 1_sik, &
                1_sik, 1_sik, 2_sik, 2_sik, 2_sik, 2_sik, 2_sik, 2_sik, 3_sik, 3_sik, &
                3_sik, 3_sik, 3_sik, 4_sik, 4_sik, 4_sik, 4_sik, 5_sik, 5_sik, 5_sik, &
                6_sik, 6_sik, 6_sik, 7_sik, 8_sik, 9_sik, 9_sik,10_sik,10_sik,11_sik /

    DATA JR3 /  0_sik, 0_sik, 1_sik, 2_sik, 7_sik,10_sik,12_sik,23_sik, 2_sik, 6_sik, &
               15_sik,17_sik, 0_sik, 2_sik, 6_sik, 7_sik,22_sik,26_sik, 0_sik, 2_sik, &
                4_sik,16_sik,26_sik, 0_sik, 2_sik, 4_sik,26_sik, 1_sik, 3_sik,26_sik, &
                0_sik, 2_sik,26_sik, 2_sik,26_sik, 2_sik,26_sik, 0_sik, 1_sik,26_sik /

    DATA nR3/    0.10658070028513E1_sdk, &
                -0.15732845290239E2_sdk, &
                 0.20944396974307E2_sdk, &
                -0.76867707878716E1_sdk, &
                 0.26185947787954E1_sdk, &
                -0.28080781148620E1_sdk, &
                 0.12053369696517E1_sdk, &
                -0.84566812812502E-2_sdk,&
                -0.12654315477714E1_sdk, &
                -0.11524407806681E1_sdk, &
                 0.88521043984318_sdk,   &
                -0.64207765181607_sdk,   &
                 0.38493460186671_sdk,   &
                -0.85214708824206_sdk,   &
                 0.48972281541877E1_sdk, &
                -0.30502617256965E1_sdk, &
                 0.39420536879154E-1_sdk,&
                 0.12558408424308_sdk,   &
                -0.27999329698710_sdk,   &
                 0.13899799569460E1_sdk, &
                -0.20189915023570E1_sdk, &
                -0.82147637173963E-2_sdk,&
                -0.47596035734923_sdk,   &
                 0.43984074473500E-1_sdk,&
                -0.44476435428739_sdk,   &
                 0.90572070719733_sdk,   &
                 0.70522450087967_sdk,   &
                 0.10770512626332_sdk,   &
                -0.32913623258954_sdk,   &
                -0.50871062041158_sdk,   &
                -0.22175400873096E-1_sdk,&
                 0.94260751665092E-1_sdk,&
                 0.16436278447961_sdk,   &
                -0.13503372241348E-1_sdk,&
                -0.14834345352472E-1_sdk,&
                 0.57922953628084E-3_sdk,&
                 0.32308904703711E-2_sdk,&
                 0.80964802996215E-4_sdk,&
                -0.16557679795037E-3_sdk,&
                -0.44923899061815E-4_sdk /

    REAL(sdk),DIMENSION(10)    :: nR4

    DATA nR4/    0.11670521452767E4_sdk, &
                -0.72421316703206E6_sdk, &
                -0.17073846940092E2_sdk, &
                 0.12020824702470E5_sdk, &
                -0.32325550322333E7_sdk, &
                 0.14915108613530E2_sdk, &
                -0.48232657361591E4_sdk, &
                 0.40511340542057E6_sdk, &
                -0.23855557567849_sdk,   &
                 0.65017534844798E3_sdk  /

    REAL(sdk),DIMENSION(4)      :: mu0H
    REAL(sdk),DIMENSION(6,7)    :: mu1H

    DATA mu0H/  1.67752_sdk,  &
                2.20462_sdk,  &
                0.6366564_sdk,&
               -0.241605_sdk  /

!    DATA mu1H / 5.20094E-1_sdk, 2.22531E-1_sdk,-2.81378E-1_sdk, 1.61913E-1_sdk,-3.25372E-2_sdk, 0.0_sdk       ,  0.0_sdk      ,&
!                8.50895E-2_sdk, 9.99115E-1_sdk,-9.06851E-1_sdk, 2.57399E-1_sdk, 0.0_sdk       , 0.0_sdk       ,  0.0_sdk      ,&
!               -1.08374_sdk   , 1.88797_sdk   ,-7.72479E-1_sdk, 0.0_sdk       , 0.0_sdk       , 0.0_sdk       ,  0.0_sdk      ,&
!               -2.89555E-1_sdk, 1.26613_sdk   ,-4.89837E-1_sdk, 0.0_sdk       , 6.98452E-2_sdk, 0.0_sdk       ,-4.35673E-3_sdk,&
!                0.0_sdk       , 0.0_sdk       ,-2.57040E-1_sdk, 0.0_sdk       , 0.0_sdk       , 8.72102E-3_sdk, 0.0_sdk       ,&
!                0.0_sdk       , 1.20573E-1_sdk, 0.0_sdk       , 0.0_sdk       , 0.0_sdk       , 0.0_sdk       ,-5.93264E-4_sdk /

    DATA mu1H /  5.20094E-1_sdk, 8.50895E-2_sdk,-1.08374_sdk   ,-2.89555E-1_sdk, 0.0_sdk       ,  0.0_sdk      , &
                 2.22531E-1_sdk, 9.99115E-1_sdk, 1.88797_sdk   , 1.26613_sdk   , 0.0_sdk       , 1.20573E-1_sdk, &
                -2.81378E-1_sdk,-9.06851E-1_sdk,-7.72479E-1_sdk,-4.89837E-1_sdk,-2.57040E-1_sdk, 0.0_sdk       , &
                 1.61913E-1_sdk, 2.57399E-1_sdk, 0.0_sdk       , 0.0_sdk       , 0.0_sdk       , 0.0_sdk       , &
                -3.25372E-2_sdk, 0.0_sdk       , 0.0_sdk       , 6.98452E-2_sdk, 0.0_sdk       , 0.0_sdk       , &
                 0.0_sdk       , 0.0_sdk       , 0.0_sdk       , 0.0_sdk       , 8.72102E-3_sdk, 0.0_sdk       , &
                 0.0_sdk       , 0.0_sdk       , 0.0_sdk       ,-4.35673E-3_sdk, 0.0_sdk       ,-5.93264E-4_sdk /

   CONTAINS

  !P(T) Boundary between regions 2 and 3
    FUNCTION PR23(T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk) :: PR23

        PR23=nR2tR3(1)+nR2tR3(2)*T+nR2tR3(3)*T*T
        RETURN
    END FUNCTION PR23

   !T(p) Boundary between regions 2 and 3
    FUNCTION TR23(p)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: p
       !Local Variables
        REAL(sdk) :: TR23

        TR23=nR2tR3(4)+SQRT((p-nR2tR3(5))/nR2tR3(3))
        RETURN
    END FUNCTION TR23

   !G(pi,tau) in Region 1
    FUNCTION GR1(pi,tau)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: pi
        REAL(sdk),INTENT(in)  :: tau
       !Local Variables
        REAL(sdk)    :: GR1
        INTEGER(sik) :: i

        GR1=0.0_sdk
        DO i=1,34
            GR1=GR1+nR1(i)*((7.1_sdk-pi)**IR1(i))*((tau-1.222_sdk)**JR1(i))
        END DO
        RETURN
    END FUNCTION GR1

   !dGdp(pi,tau) in Region 1
    FUNCTION dGdpR1(pi,tau)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: pi
        REAL(sdk),INTENT(in)  :: tau
       !Local Variables
        REAL(sdk)    :: dGdpR1
        INTEGER(sik) :: i

        dGdpR1=0.0_sdk
        DO i=1,34
            dGdpR1=dGdpR1-nR1(i)*IR1(i)*((7.1_sdk-pi)**(IR1(i)-1_sik))*((tau-1.222_sdk)**JR1(i))
        END DO
        RETURN
    END FUNCTION dGdpR1

   !dGdt(pi,tau) in Region 1
    FUNCTION dGdtR1(pi,tau)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: pi
        REAL(sdk),INTENT(in)  :: tau
       !Local Variables
        REAL(sdk)    :: dGdtR1
        INTEGER(sik) :: i

        dGdtR1=0.0_sdk
        DO i=1,34
            dGdtR1=dGdtR1+nR1(i)*((7.1_sdk-pi)**IR1(i))*JR1(i)*((tau-1.222_sdk)**(JR1(i)-1_sik))
        END DO
        RETURN
    END FUNCTION dGdtR1

   !d2Gdt2(pi,tau) in Region 1
    FUNCTION d2Gdt2R1(pi,tau)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: pi
        REAL(sdk),INTENT(in)  :: tau
       !Local Variables
        REAL(sdk)    :: d2Gdt2R1
        INTEGER(sik) :: i

        d2Gdt2R1=0.0_sdk
        DO i=1,34
            d2Gdt2R1=d2Gdt2R1+nR1(i)*((7.1_sdk-pi)**IR1(i))*JR1(i)*(JR1(i)-1_sik)*((tau-1.222_sdk)**(JR1(i)-2_sik))
        END DO
        RETURN
    END FUNCTION d2Gdt2R1

   !theta(pi,eta) in Region 1
    FUNCTION thetaR1(pi,eta)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: pi
        REAL(sdk),INTENT(in)  :: eta
       !Local Variables
        REAL(sdk)    :: thetaR1
        INTEGER(sik) :: i

        thetaR1=0.0_sdk
        DO i=1,20
            thetaR1=thetaR1+TnR1(i)*(pi**TIR1(i))*((eta+1.0_sdk)**TJR1(i))
        END DO
        RETURN
    END FUNCTION thetaR1

   !v(p,T) in Region 1
    FUNCTION vR1(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: p
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk)    :: vR1
        REAL(sdk)    :: pMPa
        REAL(sdk)    :: pi
        REAL(sdk)    :: tau
        REAL(sdk)    :: dGdp

        pMPa=p*1.0E-6_sdk
        pi=pMPa/psR1
        tau=TsR1/T
        dGdp=dGdpR1(pi,tau)
        vR1=0.001_sdk*R*T*dGdp/psR1
        RETURN
    END FUNCTION vR1

   !rho(p,T) in Region 1
    FUNCTION rhoR1(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: p
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk)    :: rhoR1
        REAL(sdk)    :: pMPa
        REAL(sdk)    :: pi
        REAL(sdk)    :: tau
        REAL(sdk)    :: dGdp

        pMPa=p*1.0E-6_sdk
        pi=pMPa/psR1
        tau=TsR1/T
        dGdp=dGdpR1(pi,tau)
        rhoR1=1000.0_sdk*psR1/(R*T*dGdp)
        RETURN
    END FUNCTION rhoR1

   !h(p,T) in Region 1
    FUNCTION hR1(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: p
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk)    :: hR1
        REAL(sdk)    :: pMPa
        REAL(sdk)    :: pi
        REAL(sdk)    :: tau
        REAL(sdk)    :: dGdt

        pMPa=p*1.0E-6_sdk
        pi=pMPa/psR1
        tau=TsR1/T
        dGdt=dGdtR1(pi,tau)
        hR1=R*T*tau*dGdt
        RETURN
    END FUNCTION hR1

   !u(p,T) in Region 1
    FUNCTION uR1(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: p
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk)    :: uR1
        REAL(sdk)    :: pMPa
        REAL(sdk)    :: pi
        REAL(sdk)    :: tau
        REAL(sdk)    :: dGdp
        REAL(sdk)    :: dGdt

        pMPa=p*1.0E-6_sdk
        pi=pMPa/psR1
        tau=TsR1/T
        dGdp=dGdpR1(pi,tau)
        dGdt=dGdtR1(pi,tau)
        uR1=R*T*(tau*dGdt-pi*dGdp)
        RETURN
    END FUNCTION uR1

   !s(p,T) in Region 1
    FUNCTION sR1(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: p
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk)    :: sR1
        REAL(sdk)    :: pMPa
        REAL(sdk)    :: pi
        REAL(sdk)    :: tau
        REAL(sdk)    :: G
        REAL(sdk)    :: dGdt

        pMPa=p*1.0E-6_sdk
        pi=pMPa/psR1
        tau=TsR1/T
        G=GR1(pi,tau)
        dGdt=dGdtR1(pi,tau)
        sR1=R*(tau*dGdt-G)
        RETURN
    END FUNCTION sR1

   !cp(p,T) in Region 1
    FUNCTION cpR1(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: p
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk)    :: cpR1
        REAL(sdk)    :: pMPa
        REAL(sdk)    :: pi
        REAL(sdk)    :: tau
        REAL(sdk)    :: d2Gdt2

        pMPa=p*1.0E-6_sdk
        pi=pMPa/psR1
        tau=TsR1/T
        d2Gdt2=d2Gdt2R1(pi,tau)
        cpR1=-R*tau*tau*d2Gdt2
        RETURN
    END FUNCTION cpR1

   !T(p,h) in Region 1
    FUNCTION TR1(p,h)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: p
        REAL(sdk),INTENT(in)  :: h
       !Local Variables
        REAL(sdk)    :: TR1
        REAL(sdk)    :: pi
        REAL(sdk)    :: eta
        REAL(sdk)    :: theta

        pi=p*1.0E-6_sdk
        eta=h/hsR1
        TR1=thetaR1(pi,eta)
        RETURN
    END FUNCTION TR1

   !G(pi,tau) in Region 2
    FUNCTION GR2(pi,tau)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: pi
        REAL(sdk),INTENT(in)  :: tau
       !Local Variables
        REAL(sdk)    :: GR2
        REAL(sdk)    :: G0R2
        REAL(sdk)    :: GrR2
        INTEGER(sik) :: i

        G0R2=log(pi)
        DO i=1,9
            G0R2=G0R2+n0R2(i)*(tau**J0R2(i))
        END DO
        GrR2=0.0_sdk
        DO i=1,43
            GrR2=GrR2+nrR2(i)*(pi**IrR2(i))*((tau-0.5_sdk)**JrR2(i))
        END DO
        GR2=G0R2+GrR2
        RETURN
    END FUNCTION GR2

   !dGdp(pi,tau) in Region 2
    FUNCTION dGdpR2(pi,tau)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: pi
        REAL(sdk),INTENT(in)  :: tau
       !Local Variables
        REAL(sdk)    :: dGdpR2
        REAL(sdk)    :: dG0dp
        REAL(sdk)    :: dGrdp
        INTEGER(sik) :: i

        dG0dp=1.0_sdk/pi
        dGrdp=0.0_sdk
        DO i=1,43
            dGrdp=dGrdp+nrR2(i)*IrR2(i)*(pi**(IrR2(i)-1_sik))*((tau-0.5_sdk)**JrR2(i))
        END DO
        dGdpR2=dG0dp+dGrdp
        RETURN
    END FUNCTION dGdpR2

   !dGdt(pi,tau) in Region 2
    FUNCTION dGdtR2(pi,tau)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: pi
        REAL(sdk),INTENT(in)  :: tau
       !Local Variables
        REAL(sdk)    :: dGdtR2
        REAL(sdk)    :: dG0dt
        REAL(sdk)    :: dGrdt
        INTEGER(sik) :: i

        dG0dt=0.0_sdk
        DO i=1,9
            dG0dt=dG0dt+n0R2(i)*J0R2(i)*(tau**(J0R2(i)-1_sik))
        END DO
        dGrdt=0.0_sdk
        DO i=1,43
            dGrdt=dGrdt+nrR2(i)*(pi**IrR2(i))*JrR2(i)*((tau-0.5_sdk)**(JrR2(i)-1_sik))
        END DO
        dGdtR2=dG0dt+dGrdt
        RETURN
    END FUNCTION dGdtR2

   !dG2dt2(pi,tau) in Region 2
    FUNCTION d2Gdt2R2(pi,tau)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: pi
        REAL(sdk),INTENT(in)  :: tau
       !Local Variables
        REAL(sdk)    :: d2Gdt2R2
        REAL(sdk)    :: d2G0dt2
        REAL(sdk)    :: d2Grdt2
        INTEGER(sik) :: i

        d2G0dt2=0.0_sdk
        DO i=1,9
            d2G0dt2=d2G0dt2+n0R2(i)*J0R2(i)*(J0R2(i)-1_sik)*(tau**(J0R2(i)-2_sik))
        END DO
        d2Grdt2=0.0_sdk
        DO i=1,43
            d2Grdt2=d2Grdt2+nrR2(i)*(pi**IrR2(i))*JrR2(i)*(JrR2(i)-1_sik)*((tau-0.5_sdk)**(JrR2(i)-2_sik))
        END DO
        d2Gdt2R2=d2G0dt2+d2Grdt2
        RETURN
    END FUNCTION d2Gdt2R2

   !theta(pi,tau) in Region 2a
    FUNCTION thetaR2a(pi,eta)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: pi
        REAL(sdk),INTENT(in)  :: eta
       !Local Variables
        REAL(sdk)    :: thetaR2a
        INTEGER(sik) :: i

        thetaR2a=0.0_sdk
        DO i=1,34
            thetaR2a=thetaR2a+TnR2a(i)*(pi**TIR2a(i))*((eta-2.1_sdk)**TJR2a(i))
        END DO
        RETURN
    END FUNCTION thetaR2a

   !theta(pi,tau) in Region 2b
    FUNCTION thetaR2b(pi,eta)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: pi
        REAL(sdk),INTENT(in)  :: eta
       !Local Variables
        REAL(sdk)    :: thetaR2b
        INTEGER(sik) :: i

        thetaR2b=0.0_sdk
        DO i=1,38
            thetaR2b=thetaR2b+TnR2b(i)*((pi-2.0_sdk)**TIR2b(i))*((eta-2.6_sdk)**TJR2b(i))
        END DO
        RETURN
    END FUNCTION thetaR2b

   !theta(pi,tau) in Region 2c
    FUNCTION thetaR2c(pi,eta)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: pi
        REAL(sdk),INTENT(in)  :: eta
       !Local Variables
        REAL(sdk)    :: thetaR2c
        INTEGER(sik) :: i

        thetaR2c=0.0_sdk
        DO i=1,23
            thetaR2c=thetaR2c+TnR2c(i)*((pi+25.0_sdk)**TIR2c(i))*((eta-1.8_sdk)**TJR2c(i))
        END DO
        RETURN
    END FUNCTION thetaR2c

   !v(p,T) in Region 2
    FUNCTION vR2(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: p
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk)    :: vR2
        REAL(sdk)    :: pMPa
        REAL(sdk)    :: pi
        REAL(sdk)    :: tau
        REAL(sdk)    :: dGdp

        pMPa=p*1.0E-6_sdk
        pi=pMPa/psR2
        tau=TsR2/T
        dGdp=dGdpR2(pi,tau)
        vR2=0.001_sdk*R*T*dGdp/psR2
        RETURN
    END FUNCTION vR2

   !rho(p,T) in Region 2
    FUNCTION rhoR2(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: p
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk)    :: rhoR2
        REAL(sdk)    :: pMPa
        REAL(sdk)    :: pi
        REAL(sdk)    :: tau
        REAL(sdk)    :: dGdp

        pMPa=p*1.0E-6_sdk
        pi=pMPa/psR2
        tau=TsR2/T
        dGdp=dGdpR2(pi,tau)
        rhoR2=1000.0_sdk*psR2/(R*T*dGdp)
        RETURN
    END FUNCTION rhoR2

   !h(p,T) in Region 2
    FUNCTION hR2(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: p
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk)    :: hR2
        REAL(sdk)    :: pMPa
        REAL(sdk)    :: pi
        REAL(sdk)    :: tau
        REAL(sdk)    :: dGdt

        pMPa=p*1.0E-6_sdk
        pi=pMPa/psR2
        tau=TsR2/T
        dGdt=dGdtR2(pi,tau)
        hR2=R*T*tau*dGdt
        RETURN
    END FUNCTION hR2

   !u(p,T) in Region 2
    FUNCTION uR2(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: p
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk)    :: uR2
        REAL(sdk)    :: pMPa
        REAL(sdk)    :: pi
        REAL(sdk)    :: tau
        REAL(sdk)    :: dGdp
        REAL(sdk)    :: dGdt

        pMPa=p*1.0E-6_sdk
        pi=pMPa/psR2
        tau=TsR2/T
        dGdp=dGdpR2(pi,tau)
        dGdt=dGdtR2(pi,tau)
        uR2=R*T*(tau*dGdt-pi*dGdp)
        RETURN
    END FUNCTION uR2

   !s(p,T) in Region 2
    FUNCTION sR2(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: p
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk)    :: sR2
        REAL(sdk)    :: pMPa
        REAL(sdk)    :: pi
        REAL(sdk)    :: tau
        REAL(sdk)    :: G
        REAL(sdk)    :: dGdt

        pMPa=p*1.0E-6_sdk
        pi=pMPa/psR2
        tau=TsR2/T
        G=GR2(pi,tau)
        dGdt=dGdtR2(pi,tau)
        sR2=R*(tau*dGdt-G)
        RETURN
    END FUNCTION sR2

   !cp(p,T) in Region 2
    FUNCTION cpR2(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: p
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk)    :: cpR2
        REAL(sdk)    :: pMPa
        REAL(sdk)    :: pi
        REAL(sdk)    :: tau
        REAL(sdk)    :: d2Gdt2

        pMPa=p*1.0E-6_sdk
        pi=pMPa/psR2
        tau=TsR2/T
        d2Gdt2=d2Gdt2R2(pi,tau)
        cpR2=-R*tau*tau*d2Gdt2
        RETURN
    END FUNCTION cpR2

   !pi(h) Boundary between Region 2b and 2c
    FUNCTION piR2bc(h)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: h
       !Local Variables
        REAL(sdk)    :: piR2bc
        REAL(sdk)    :: pi
        piR2bc=nR2btR2c(1)+nR2btR2c(2)*h+nR2btR2c(3)*h*h
        RETURN
    END FUNCTION piR2bc

   !T(p,h) in Region 2
    FUNCTION TR2(p,h)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: p
        REAL(sdk),INTENT(in)  :: h
       !Local Variables
        REAL(sdk)    :: TR2
        REAL(sdk)    :: pi
        REAL(sdk)    :: eta
        REAL(sdk)    :: pib

        pi=p*1.0E-6_sdk
        eta=h/2000.0_sdk
        IF (pi>4.0_sdk) THEN
            pib=piR2bc(h)
            IF (pi<pib) THEN
                TR2=thetaR2b(pi,eta)
            ELSE
                TR2=thetaR2c(pi,eta)
            END IF
        ELSE
            TR2=thetaR2a(pi,eta)
        END IF
        RETURN
    END FUNCTION TR2

   !f(del,tau) in Region 3
    FUNCTION fR3(del,tau)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: del
        REAL(sdk),INTENT(in)  :: tau
       !Local Variables
        REAL(sdk)    :: fR3
        INTEGER(sik) :: i

        fR3=nR3(1)*log(del)
        DO i=2,40
            fR3=fR3+nR3(i)*(del**IR3(i))*(tau**JR3(i))
        END DO
        RETURN
    END FUNCTION fR3

   !dfddel(del,tau) in Region 3
    FUNCTION dfddelR3(del,tau)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: del
        REAL(sdk),INTENT(in)  :: tau
       !Local Variables
        REAL(sdk)    :: dfddelR3
        INTEGER(sik) :: i

        dfddelR3=nR3(1)/del
        DO i=2,40
            dfddelR3=dfddelR3+nR3(i)*IR3(i)*(del**(IR3(i)-1_sik))*(tau**JR3(i))
        END DO
        RETURN
    END FUNCTION dfddelR3

   !dfdtau(del,tau) in Region 3
    FUNCTION dfdtauR3(del,tau)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: del
        REAL(sdk),INTENT(in)  :: tau
       !Local Variables
        REAL(sdk)    :: dfdtauR3
        INTEGER(sik) :: i

        dfdtauR3=0.0_sdk
        DO i=2,40
            dfdtauR3=dfdtauR3+nR3(i)*(del**IR3(i))*JR3(i)*(tau**(JR3(i)-1_sik))
        END DO
        RETURN
    END FUNCTION dfdtauR3

   !p(rho,T) in Region 3
    FUNCTION pR3(rho,T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: rho
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk)    :: pR3
        REAL(sdk)    :: pMPa
        REAL(sdk)    :: del
        REAL(sdk)    :: tau
        REAL(sdk)    :: dfddel

        del=rho/rhoc
        tau=Tc/T
        dfddel=dfddelR3(del,tau)
        pMPa=0.001_sdk*rho*R*T*del*dfddel
        pR3=pMPa*1.0E6_sdk
        RETURN
    END FUNCTION pR3

   !h(rho,T) in Region 3
    FUNCTION hR3(rho,T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: rho
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk)    :: hR3
        REAL(sdk)    :: del
        REAL(sdk)    :: tau
        REAL(sdk)    :: dfdtau
        REAL(sdk)    :: dfddel

        del=rho/rhoc
        tau=Tc/T
        dfdtau=dfdtauR3(del,tau)
        dfddel=dfddelR3(del,tau)
        hR3=R*T*(tau*dfdtau+del*dfddel)
        RETURN
    END FUNCTION hR3


   !u(rho,T) in Region 3
    FUNCTION uR3(rho,T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: rho
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk)    :: uR3
        REAL(sdk)    :: del
        REAL(sdk)    :: tau
        REAL(sdk)    :: dfdtau

        del=rho/rhoc
        tau=Tc/T
        dfdtau=dfdtauR3(del,tau)
        uR3=R*Tc*dfdtau
        RETURN
    END FUNCTION uR3

   !s(rho,T) in Region 3
    FUNCTION sR3(rho,T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: rho
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk)    :: sR3
        REAL(sdk)    :: del
        REAL(sdk)    :: tau
        REAL(sdk)    :: f
        REAL(sdk)    :: dfdtau

        del=rho/rhoc
        tau=Tc/T
        dfdtau=dfdtauR3(del,tau)
        f=fR3(del,tau)
        sR3=R*(tau*dfdtau-f)
        RETURN
    END FUNCTION sR3

   !psat(T) Region 4
    FUNCTION psat(T)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: T
       !Local Variables
        REAL(sdk)    :: psat
        REAL(sdk)    :: pMPa
        REAL(sdk)    :: nu
        REAL(sdk)    :: A
        REAL(sdk)    :: B
        REAL(sdk)    :: C

        nu=T+nR4(9)/(T-nR4(10))
        A=       nu*nu+nR4(1)*nu+nR4(2)
        B=nR4(3)*nu*nu+nR4(4)*nu+nR4(5)
        C=nR4(6)*nu*nu+nR4(7)*nu+nR4(8)

        pMPa=(2.0_sdk*C/(sqrt(B*B-4.0_sdk*A*C)-B))**4_sik
        psat=pMPa*1.0E6_sdk
        RETURN
    END FUNCTION psat

   !Tsat(p) Region 4
    FUNCTION Tsat(p)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: p
       !Local Variables
        REAL(sdk)    :: Tsat
        REAL(sdk)    :: pMpa
        REAL(sdk)    :: beta
        REAL(sdk)    :: D
        REAL(sdk)    :: E
        REAL(sdk)    :: F
        REAL(sdk)    :: G

        pMPa=p*1.0E-6_sdk
        beta=pMPa**0.25_sdk
        E=       beta*beta+nR4(3)*beta+nR4(6)
        F=nR4(1)*beta*beta+nR4(4)*beta+nR4(7)
        G=nR4(2)*beta*beta+nR4(5)*beta+nR4(8)
        D=-2.0_sdk*G/(sqrt(F*F-4.0_sdk*E*G)+F)

        Tsat=0.5_sdk*(nR4(10)+D-sqrt((nR4(10)+D)*(nR4(10)+D)-4.0_sdk*(nR4(9)+nR4(10)*D)))
        RETURN
    END FUNCTION Tsat

   !x12(h,p)
    FUNCTION x12(h,p)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: h
        REAL(sdk),INTENT(in)  :: p
       !Local Variables
        REAL(sdk)    :: x12
        REAL(sdk)    :: Ts
        REAL(sdk)    :: hfs
        REAL(sdk)    :: hgs
        REAL(sdk)    :: hfg

        Ts=Tsat(p)
        hfs=hR1(p,Ts)
        hgs=hR2(p,Ts)
        hfg=hgs-hfs
        x12=(h-hfs)/hfg

        RETURN
    END FUNCTION x12

   !mu0(T)
    FUNCTION mu0(Tb)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: Tb
       !Local Variables
        REAL(sdk)    :: mu0
        REAL(sdk)    :: den
        INTEGER(sik) :: i

        den=0.0_sdk
        DO i=1,4
            den=den+mu0H(i)/(Tb**(i-1_sik))
        END DO
        mu0=100.0_sdk*sqrt(Tb)/den
        RETURN
    END FUNCTION mu0

   !mu1(T)
    FUNCTION mu1(Tb,rhob)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: Tb
        REAL(sdk),INTENT(in)  :: rhob
       !Local Variables
        REAL(sdk)    :: mu1
        REAL(sdk)    :: Ts
        REAL(sdk)    :: rhos
        REAL(sdk)    :: temp1
        REAL(sdk)    :: temp2
        INTEGER(sik) :: i
        INTEGER(sik) :: j

        Ts=1.0_sdk/Tb - 1.0_sdk
        rhos=rhob-1.0_sdk
        temp1=0.0_sdk
        DO i=1,6
            temp2=0.0_sdk
            DO j=1,7
                temp2=temp2+mu1H(i,j)*(rhos**(j-1_sik))
            END DO
            temp1=temp1+(Ts**(i-1_sik))*temp2
        END DO
        mu1=exp(rhob*temp1)
        RETURN
    END FUNCTION mu1

    !mu(T,rho)
    FUNCTION mu(T,rho)
        IMPLICIT NONE
       !Arguments
        REAL(sdk),INTENT(in)  :: T
        REAL(sdk),INTENT(in)  :: rho
       !Local Variables
        REAL(sdk)    :: mu
        REAL(sdk)    :: Tb
        REAL(sdk)    :: rhob    
        REAL(sdk)    :: mu0eh

        Tb=T/647.096_sdk
        rhob=rho/322.0_sdk
        mu=mu0(Tb)*mu1(Tb,rhob)*1.0E-6_sdk

    END FUNCTION mu

END MODULE SteamProp
