SUBROUTINE Init

    USE IntrType
    USE PriVars
    USE BlockStr
    USE Allocs
    USE Solver
    USE Table
    USE ConstitutiveRel
    USE SIMPLE

    IMPLICIT NONE

   !Local variables
    INTEGER(sik) :: i
    INTEGER(sik) :: j
    INTEGER(sik) :: vidx
    INTEGER(sik) :: jidx
    INTEGER(sik) :: TBLidx
    REAL(sdk)    :: sp
    REAL(sdk)    :: Ts
    REAL(sdk)    :: rhos
    REAL(sdk)    :: mus

    IF (NumOfSlvrCards > 0_sik) time=TCards(1)%tbegin   
    
   !Initialize Volume Data
    DO i=1,NumBB
        vidx=BB(i)%vidx
        TBLidx=BB(i)%Pmff
        sp=TableLookup(TBLidx,time)
        Pm(vidx)=sp
        TBLidx=BB(i)%hmff
        sp=TableLookup(TBLidx,time)
        hm(vidx)=sp
        TBLidx=BB(i)%alphaff
        sp=TableLookup(TBLidx,time)
        alpha(vidx)=sp
    END DO

    DO i=1,NumFB
        DO j=1,FB(i)%cells
            vidx=FB(i)%vidx(j)
            Pm(vidx)=FB(i)%Pm(j)
            hm(vidx)=FB(i)%hm(j)
            alpha(vidx)=FB(i)%alpha(j)
        END DO
    END DO
    
   !Initialize Junction Data 
    CALL CalcQuality
    CALL CalcTemperature
    CALL CalcDensity 

    DO i=1,NumJB
        jidx=JB(i)%jidx
        IF (JB(i)%bFlag=='U') THEN
            TBLidx=JB(i)%uwff
            sp=TableLookup(TBLidx,time) 
            um(jidx)=sp   
        ELSEIF (JB(i)%bFlag=='W') THEN
            TBLidx=JB(i)%uwff
            sp=TableLookup(TBLidx,time)    
            um(jidx)=sp/(JArea(jidx)*rhoJun(jidx))
        ELSEIF (JB(i)%jFlag=='U') THEN
            um(jidx)=JB(i)%uw
        ELSEIF (JB(i)%jFlag=='W') THEN
            um(jidx)=JB(i)%uw/(JArea(jidx)*rhoJun(jidx))
        END IF
    END DO

    DO i=1,NumFB
        DO j=1,(FB(i)%cells-1)
            jidx=FB(i)%jidx(j)
            IF (FB(i)%jFlag(j)=='U') THEN
                um(jidx)=FB(i)%uw(j)
            ELSEIF (FB(i)%jFlag(j)=='W') THEN
                um(jidx)=FB(i)%uw(j)/(JArea(jidx)*rhoJun(jidx))
            END IF
        END DO
    END DO

    CALL CalcVF
    CALL CalcViscosity
    CALL SetupW
    CALL CalcReynolds
    CALL CalcDarcyF
    CALL CalcThCond
    CALL CalcVdj
    CALL SetupAum
    CALL sortja(Aum,jAum,iAum,juncs,nzAum)
    CALL SetupApm
    CALL sortja(Apm,jApm,iApm,vols,nzApm)
    CALL SetupAhm
    CALL sortja(Ahm,jAhm,iAhm,vols,nzAhm)
    rhomVol0=rhomVol
    rhoJun0=rhoJun
    um0=um
    hm0=hm

END SUBROUTINE Init
