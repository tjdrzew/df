SUBROUTINE Solve

    USE IntrType
    USE Solver
    USE SIMPLE

    IMPLICIT NONE

    INTEGER(sik) :: i
    INTEGER(sik) :: idum
    INTEGER(sik) :: error
    INTEGER(sik) :: iteration
    INTEGER(sik) :: idxu
    INTEGER(sik) :: idxh
    LOGICAL      :: Iterate

!    IF (vjuncs>0) THEN
!     WRITE(ioscn,'(A12,A6,A12,3(A12,A7))') 'time','iter','cont','u-mom','junc','v-mom','gap','enthalpy','vol'
!     ELSE
!                     WRITE(ioscn,'(A12,A6,A12,2(A12,A7))') 'time','iter','cont','u-mom','junc','enthalpy','vol'
!       END IF

    WRITE(*,*) ''
    WRITE(*,'(A12,A6,4A12)') 'time','iter','cont','u-mom','enthalpy','VF'

    i=1_sik
    maxit     = TCards(i)%maxit
    tbegin    = TCards(i)%tbegin
    tend      = TCards(i)%tend
    dt        = TCards(i)%dt
    epc       = TCards(i)%epc
    epu       = TCards(i)%epu
    epv       = TCards(i)%epv
    eph       = TCards(i)%eph
    epVF      = TCards(i)%epVF
    alphau    = TCards(i)%alphau
    alphav    = TCards(i)%alphav
    alphap    = TCards(i)%alphap
    alphah    = TCards(i)%alphah
    alphaVF   = TCards(i)%alphaVF
    umin      = TCards(i)%umin
    umax      = TCards(i)%umax
    vmin      = TCards(i)%vmin
    vmax      = TCards(i)%vmax
    hmin      = TCards(i)%hmin
    hmax      = TCards(i)%hmax
    pmin      = TCards(i)%pmin
    pmax      = TCards(i)%pmax

    time = tbegin

    iteration=0_sik
    Iterate=.TRUE.

    DO WHILE(Iterate)
        Iteration=Iteration+1_sik

       !solve for the guessed velocity field
        CALL setupbum
        CALL pardiso (ptum,1,1,11, 13, juncs, Aum, iAum, jAum, &
                      idum, 1, umparm, 0, bum, umg, error)
        CALL pardiso (ptum,1,1,11, -1, juncs, Aum, iAum, jAum, &
                      idum, 1, umparm, 0, bum, umg, error)
!         IF (vjuncs>0) THEN
!            CALL pardiso (ptv,1,1,11, 13,vjuncs, SCAv, SCiAv, SCjAv, &
!                 idum, 1, vparm, 0, SCbv, SCvg, error)
!            CALL pardiso (ptv,1,1,11, -1,vjuncs, SCAv, SCiAv, SCjAv, &
!                 idum, 1, vparm, 0, SCbv, SCvg, error)
!         END IF

       !solve for the pressure correction
        CALL setupbPmc
        CALL pardiso (ptpm,1,1,11, 13, vols, Apm, iApm, jApm, &
                     idum, 1, pmparm, 0, bpmc, pmc, error)
        CALL pardiso (ptpm,1,1,11, -1, vols, Apm, iApm, jApm, &
                     idum, 1, pmparm, 0, bpmc, pmc, error)

       !solve fluid energy
        CALL setupbhm
        CALL pardiso (pthm,1,1,11, 13, vols, Ahm, iAhm, jAhm, &
                      idum, 1, hmparm, 0, bhm, hmg, error)
        CALL pardiso (pthm,1,1,11, -1, vols, Ahm, iAhm, jAhm, &
                      idum, 1, hmparm, 0, bhm, hmg, error)

       !correct pressure, velocity, enthalpy, and update arrays
        CALL Calcuvmc
        um=(1-alphau)*um+alphau*(umg+umc)
!         IF (vjuncs>0) SCv=(1-alphav)*SCv+alphav*(SCvg+SCvc)
        pm=pm+alphap*pmc
        hm=(1-alphah)*hm+alphah*hmg
        CALL limitVars
        CALL UpdateArrays

       !calculate residuals
        CALL CalcResid(juncs,nzAum,Aum,iAum,jAum,um,bum,residu,idxu)
!         IF (vjuncs>0) CALL CalcResid(vjuncs,nzav,SCAv,SCiAv,SCjAv,SCv,SCbv,residv,idxv)
        CALL CalcResid(vols ,nzAhm,Ahm,iAhm,jAhm,hm,bhm,residh,idxh)
        CALL CalcResidC


        WRITE(*,'(ES12.4,I6,4ES12.4)') time,iteration,residc,residu,residh,residVF          
        IF ((residc<=epc).AND.(residu<=epu).AND.(residh<=eph)) THEN
            Iterate = .FALSE.
        ELSE IF (iteration >= maxit) THEN
            Iterate = .FALSE.
        ELSE IF (residc >  1.0D+50) THEN
            STOP 'Calculation Diverged'           
        END IF

    END DO



END SUBROUTINE Solve
