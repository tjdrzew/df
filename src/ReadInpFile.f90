SUBROUTINE ReadInpFile

    USE IntrType
    USE Param
    USE File
    USE Solver
    USE Table
    USE BlockStr
    USE Allocs

    IMPLICIT NONE
!
! THE PURPOSE OF THIS ROUTINE IS to scan the input file to obtain the
! the information about the memory allocation and variable initialization
!
! Programmed by ;
!   NAME         : Tim Drzewiecki
!   ORGANIZATION : Solo Mission
!   DATE         : 08/13/2012
!
! Subroutine Argument Descriptions:
!   N/A
!
! Variable Declarations;
!   Subroutine Arguments:
!    N/A
!   Local Variables:
    INTEGER(sik)         :: i
    INTEGER(sik)         :: j
    INTEGER(sik)         :: LinNum
    INTEGER(sik)         :: err
    INTEGER(sik)         :: FBidx
    INTEGER(sik)         :: BBidx
    INTEGER(sik)         :: JBidx
    INTEGER(sik)         :: GBidx
    INTEGER(sik)         :: HBidx
    INTEGER(sik)         :: TableNum
    INTEGER(sik)         :: TableIdx
    INTEGER(sik)         :: SLVRidx
    INTEGER(sik)         :: idumy1
    INTEGER(sik)         :: idumy2

    LOGICAL              :: EndOFile
    LOGICAL              :: ReadSlvrCards               ! Flag to start couting # of compositions
    LOGICAL              :: ReadTables                  ! Flag to start couting TableDim

   !Initialization
    EndOFile      = .FALSE.
    ReadTables    = .FALSE.
    ReadSlvrCards = .FALSE.
    LinNum        = 0_sik
    FBidx         = 0_sik
    BBidx         = 0_sik
    JBidx         = 0_sik
    GBidx         = 0_sik
    HBidx         = 0_sik
    TableNum      = 0_sik
    TableIdx      = 0_sik
    SLVRidx       = 0_sik

    DO WHILE(.NOT.EndOFile)
        READ(infile,'(a1024)', IOSTAT = err) OneLine
        LinNum = LinNum + 1
        IF(err < 0) THEN
            EndOFile = .TRUE.
        ELSE
            Probe = OneLine(1:1)
            IF(oneline /= BLANK .AND. probe /= BANG) THEN
                CALL DCompLine(OneLine, InpDatTyp, InpDatEnt, InpLinCom, NumDat)
                IF(NumDat >= 0) THEN
                    READ(InpDatEnt(1),*) CardName
                    CALL toupper(CardName)
                   ! Read in SLVR cards
                    IF(ReadSlvrCards .AND. (InpDatTyp(1) == 'NUM') ) THEN
                        SLVRidx = SLVRidx + 1_sik
                        IF(NumDat > 0) READ(InpDatEnt(1), *) TCards(SLVRidx)%maxit
                        ! set defaults
                        TCards(SLVRidx)%tbegin  = 0.0_sdk
                        TCards(SLVRidx)%tend    = 0.0_sdk
                        TCards(SLVRidx)%dt      = 0.0_sdk
                        TCards(SLVRidx)%epc     = 1.0E-6_sdk
                        TCards(SLVRidx)%epu     = 1.0E-6_sdk
                        TCards(SLVRidx)%epv     = 1.0E-6_sdk
                        TCards(SLVRidx)%eph     = 1.0E-6_sdk
                        TCards(SLVRidx)%epVF    = 1.0E-3_sdk
                        TCards(SLVRidx)%alphau  = 0.5_sdk
                        TCards(SLVRidx)%alphav  = 0.5_sdk
                        TCards(SLVRidx)%alphap  = 0.2_sdk
                        TCards(SLVRidx)%alphah  = 0.02_sdk
                        TCards(SLVRidx)%alphaVF = 0.5_sdk
                        TCards(SLVRidx)%umin    =-1000.0_sdk
                        TCards(SLVRidx)%umax    = 1000.0_sdk
                        TCards(SLVRidx)%vmin    =-1000.0_sdk
                        TCards(SLVRidx)%vmax    = 1000.0_sdk
                        TCards(SLVRidx)%hmin    = 10.0_sdk
                        TCards(SLVRidx)%hmax    = 4000.0_sdk
                        TCards(SLVRidx)%pmin    = 612.0_sdk
                        TCards(SLVRidx)%pmax    = 1.8E7_sdk
                        IF(NumDat > 1)  READ(InpDatEnt(2), *) TCards(SLVRidx)%tbegin
                        IF(NumDat > 2)  READ(InpDatEnt(3), *) TCards(SLVRidx)%tend
                        IF(NumDat > 3)  READ(InpDatEnt(4), *) TCards(SLVRidx)%dt
                        IF(NumDat > 4)  READ(InpDatEnt(5), *) TCards(SLVRidx)%epc
                        IF(NumDat > 5)  READ(InpDatEnt(6), *) TCards(SLVRidx)%epu
                        IF(NumDat > 6)  READ(InpDatEnt(7), *) TCards(SLVRidx)%epv
                        IF(NumDat > 7)  READ(InpDatEnt(8), *) TCards(SLVRidx)%eph
                        IF(NumDat > 8)  READ(InpDatEnt(9), *) TCards(SLVRidx)%epVF
                        IF(NumDat > 9)  READ(InpDatEnt(10),*) TCards(SLVRidx)%alphau
                        IF(NumDat > 10) READ(InpDatEnt(11),*) TCards(SLVRidx)%alphav
                        IF(NumDat > 11) READ(InpDatEnt(12),*) TCards(SLVRidx)%alphap
                        IF(NumDat > 12) READ(InpDatEnt(13),*) TCards(SLVRidx)%alphah
                        IF(NumDat > 13) READ(InpDatEnt(14),*) TCards(SLVRidx)%alphaVF
                        IF(NumDat > 14) READ(InpDatEnt(15),*) TCards(SLVRidx)%umin
                        IF(NumDat > 15) READ(InpDatEnt(16),*) TCards(SLVRidx)%umax
                        IF(NumDat > 16) READ(InpDatEnt(17),*) TCards(SLVRidx)%vmin
                        IF(NumDat > 17) READ(InpDatEnt(18),*) TCards(SLVRidx)%vmax
                        IF(NumDat > 18) READ(InpDatEnt(19),*) TCards(SLVRidx)%hmin
                        IF(NumDat > 19) READ(InpDatEnt(20),*) TCards(SLVRidx)%hmax
                        IF(NumDat > 20) READ(InpDatEnt(21),*) TCards(SLVRidx)%pmin
                        IF(NumDat > 21) READ(InpDatEnt(22),*) TCards(SLVRidx)%pmax
                    END IF
                    IF(ReadSlvrCards .AND. ((probe /= BLANK) .OR. (InpDatTyp(1) /= 'NUM')) ) THEN
                        ReadSlvrCards = .FALSE.
                    END IF
                   ! Read in TABL
                    IF(ReadTables .AND. (InpDatTyp(1) == 'NUM')) THEN
                        TableIdx = TableIdx + 1_sik
                        READ(InpDatEnt(1), *) IndVar(TableIdx)
                        READ(InpDatEnt(2), *) DepVar(TableIdx)
                    END IF
                    IF(ReadTables .AND. ((probe /= BLANK) .OR. (InpDatTyp(1) /= 'NUM')) ) THEN
                        TableBnds(TableNum,2) = TableIdx
                        ReadTables = .FALSE.
                    END IF
                    SELECT CASE(CardName)
                    CASE('SLVR')
                        ReadSlvrCards = .TRUE.
                    CASE('TBL')
                        TableNum = TableNum + 1_sik
                        TableBnds(TableNum,1) = TableIdx + 1_sik
                        ReadTables =  .TRUE.
                    CASE('FB10')
                        FBidx = FBidx + 1_sik
                        READ(InpDatEnt(2),*) FB(FBidx)%id
                        CALL toupper(FB(FBidx)%id)
                    CASE('FB20')
                        READ(InpDatEnt(2),*) FB(FBidx)%cells
                        ALLOCATE(FB(FBidx)%volume(FB(FBidx)%cells))
                        ALLOCATE(FB(FBidx)%area(FB(FBidx)%cells))
                        ALLOCATE(FB(FBidx)%length(FB(FBidx)%cells))
                        ALLOCATE(FB(FBidx)%height(FB(FBidx)%cells))
                        ALLOCATE(FB(FBidx)%hydDia(FB(FBidx)%cells))
                        ALLOCATE(FB(FBidx)%Pm(FB(FBidx)%cells))
                        ALLOCATE(FB(FBidx)%hm(FB(FBidx)%cells))
                        ALLOCATE(FB(FBidx)%alpha(FB(FBidx)%cells))
                        ALLOCATE(FB(FBidx)%jForm(FB(FBidx)%cells-1))
                        ALLOCATE(FB(FBidx)%jFlag(FB(FBidx)%cells-1))
                        ALLOCATE(FB(FBidx)%Kloss(FB(FBidx)%cells-1))
                        ALLOCATE(FB(FBidx)%uw(FB(FBidx)%cells-1))
                        ALLOCATE(FB(FBidx)%vidx(FB(FBidx)%cells))
                        ALLOCATE(FB(FBidx)%jidx(FB(FBidx)%cells-1))
                        ALLOCATE(FB(FBidx)%sidx(FB(FBidx)%cells))
                        FB(FBidx)%volume = 0.0_sdk
                        FB(FBidx)%area   = 0.0_sdk
                        FB(FBidx)%length = 0.0_sdk
                        FB(FBidx)%height = 0.0_sdk
                        FB(FBidx)%hydDia = 0.0_sdk
                        FB(FBidx)%Pm     = 0.0_sdk
                        FB(FBidx)%hm     = 0.0_sdk
                        FB(FBidx)%alpha  = 0.0_sdk
                        FB(FBidx)%jForm  = ''
                        FB(FBidx)%jFlag  = ''
                        FB(FBidx)%Kloss  = 0.0_sdk
                        FB(FBidx)%uw     = 0.0_sdk
                        FB(FBidx)%vidx   = 0_sik
                        FB(FBidx)%jidx   = 0_sik
                        FB(FBidx)%sidx   = 0_sik
                    CASE('FB25')
                        DO j=1,FB(FBidx)%cells
                           READ(InpDatEnt(j+1),*) FB(FBidx)%volume(j)
                        END DO
                    CASE('FB30')
                        DO j=1,FB(FBidx)%cells
                           READ(InpDatEnt(j+1),*) FB(FBidx)%area(j)
                        END DO
                    CASE('FB35')
                        DO j=1,FB(FBidx)%cells
                           READ(InpDatEnt(j+1),*) FB(FBidx)%length(j)
                        END DO
                    CASE('FB40')
                        DO j=1,FB(FBidx)%cells
                           READ(InpDatEnt(j+1),*) FB(FBidx)%height(j)
                        END DO
                    CASE('FB45')
                        DO j=1,FB(FBidx)%cells
                           READ(InpDatEnt(j+1),*) FB(FBidx)%hydDia(j)
                        END DO
                    CASE('FB50')
                        READ(InpDatEnt(2),*) FB(FBidx)%roughness
                    CASE('FB60')
                        DO j=1,FB(FBidx)%cells
                            READ(InpDatEnt(j+1),*) FB(FBidx)%Pm(j)
                        END DO
                    CASE('FB70')
                        DO j=1,FB(FBidx)%cells
                            READ(InpDatEnt(j+1),*) FB(FBidx)%hm(j)
                        END DO
                    CASE('FB80')
                        DO j=1,FB(FBidx)%cells
                            READ(InpDatEnt(j+1),*) FB(FBidx)%alpha(j)
                        END DO
                    CASE('FB90')
                        DO j=1,(FB(FBidx)%cells-1_sik)
                            READ(InpDatEnt(j+1),*) FB(FBidx)%jForm(j)
                            CALL toupper(FB(FBidx)%jForm(j))
                            READ(InpDatEnt(j+FB(FBidx)%cells),*) FB(FBidx)%KLoss(j)
                        END DO
                    CASE('FB91')
                        DO j=1,(FB(FBidx)%cells-1_sik)
                            READ(InpDatEnt(j+1),*) FB(FBidx)%jFlag(j)
                            CALL toupper(FB(FBidx)%jFlag(j))
                            READ(InpDatEnt(j+FB(FBidx)%cells),*) FB(FBidx)%uw(j)
                        END DO
                    CASE('BB10')
                        BBidx = BBidx + 1_sik
                        READ(InpDatEnt(2),*) BB(BBidx)%id
                        CALL toupper(BB(BBidx)%id)
                        READ(InpDatEnt(3),*) BB(BBidx)%bType
                        CALL toupper(BB(BBidx)%bType)
                        BB(BBidx)%Pmff  = 0_sik
                        BB(BBidx)%hmff  = 0_sik
                        BB(BBidx)%vidx = 0_sik
                    CASE('BB20')
                        READ(InpDatEnt(2),*) BB(BBidx)%Pmff
                        READ(InpDatEnt(3),*) BB(BBidx)%hmff
                        READ(InpDatEnt(4),*) BB(BBidx)%alphaff
                    CASE('JB10')
                        JBidx = JBidx + 1_sik
                        READ(InpDatEnt(2),*) JB(JBidx)%id
                        CALL toupper(JB(JBidx)%id)
                        READ(InpDatEnt(3),*) JB(JBidx)%inFace
                        CALL toupper(JB(JBidx)%inFace)
                        READ(InpDatEnt(4),*) JB(JBidx)%inId
                        CALL toupper(JB(JBidx)%inId)
                        READ(InpDatEnt(5),*) JB(JBidx)%exFace
                        CALL toupper(JB(JBidx)%exFace)
                        READ(InpDatEnt(6),*) JB(JBidx)%exId
                        CALL toupper(JB(JBidx)%exId)
                        JB(JBidx)%bFlag = ' '
                        JB(JBidx)%uwff  =  0_sik
                        JB(JBidx)%jForm = ' '
                        JB(JBidx)%jFlag = ' '
                        JB(JBidx)%KFLoss = 0.0_sdk
                        JB(JBidx)%KRLoss = 0.0_sdk
                        JB(JBidx)%uw = 0.0_sdk
                        JB(JBidx)%jidx = 0_sik
                    CASE('JB15')
                        READ(InpDatEnt(2),*) JB(JBidx)%bFlag
                        CALL toupper(JB(JBidx)%bFlag)
                        READ(InpDatEnt(3),*) JB(JBidx)%uwff  
                    CASE('JB20')
                        READ(InpDatEnt(2),*) JB(JBidx)%jForm
                        CALL toupper(JB(JBidx)%jForm)
                        READ(InpDatEnt(3),*) JB(JBidx)%KFLoss
                        READ(InpDatEnt(4),*) JB(JBidx)%KRLoss                     
                    CASE('JB25')
                        READ(InpDatEnt(2),*) JB(JBidx)%jFlag
                        CALL toupper(JB(JBidx)%jFlag)
                        READ(InpDatEnt(3),*) JB(JBidx)%uw  
                    CASE('GB10')
                        GBidx = GBidx + 1_sik
                        READ(InpDatEnt(2),*) GB(GBidx)%id
                        CALL toupper(GB(GBidx)%id)   
                        READ(InpDatEnt(3),*) GB(GBidx)%inId
                        CALL toupper(GB(GBidx)%inId)
                        READ(InpDatEnt(4),*) GB(GBidx)%exId
                        CALL toupper(GB(GBidx)%exId)
                    CASE('GB11')
                        READ(InpDatEnt(2),*) GB(GBidx)%inCell1
                        READ(InpDatEnt(3),*) GB(GBidx)%inCell2
                        READ(InpDatEnt(4),*) GB(GBidx)%exCell1
                        READ(InpDatEnt(5),*) GB(GBidx)%exCell2  
                        idumy1 = GB(GBidx)%inCell2 - GB(GBidx)%inCell1 + 1_sik
                        idumy2 = GB(GBidx)%exCell2 - GB(GBidx)%exCell1 + 1_sik
                        IF (idumy1==idumy2) THEN
                            GB(GBidx)%gaps=idumy1
                            ALLOCATE(GB(GBidx)%gFlag(GB(GBidx)%gaps)) 
                            ALLOCATE(GB(GBidx)%uw(GB(GBidx)%gaps)) 
                            ALLOCATE(GB(GBidx)%gidx(GB(GBidx)%gaps)) 
                            ALLOCATE(GB(GBidx)%KLoss(GB(GBidx)%gaps)) 
                            ALLOCATE(GB(GBidx)%Width(GB(GBidx)%gaps)) 
                            DO j=1,GB(GBidx)%gaps
                                GB(GBidx)%gFlag(j)=' '
                                GB(GBidx)%uw(j)=0.0_sdk
                                GB(GBidx)%gidx(j)=0_sik
                                GB(GBidx)%KLoss(j)=0.0_sdk
                                GB(GBidx)%Width(j)=0.0_sdk
                            END DO
                        ELSE
                            WRITE(*, '(a49,i6)') 'Num of incells must match num of excells for GB ', GBidx  
                            STOP                                
                        END IF
                    CASE('GB20')
                        DO j=1,(GB(GBidx)%gaps)
                            READ(InpDatEnt(j+1+GB(GBidx)%gaps),*) GB(GBidx)%KLoss(j)
                        END DO                       
                    CASE('GB25')
                        DO j=1,(GB(GBidx)%gaps)
                            READ(InpDatEnt(j+1),*) GB(GBidx)%gFlag(j)
                            CALL toupper(GB(GBidx)%gFlag(j))
                            READ(InpDatEnt(j+1+GB(GBidx)%gaps),*) GB(GBidx)%uw(j)
                        END DO    
                    CASE('GB30')
                        DO j=1,(GB(GBidx)%gaps)
                            READ(InpDatEnt(j+1),*) GB(GBidx)%Width(j)
                        END DO   
                    CASE('HB10')
                        HBidx = HBidx + 1_sik
                        READ(InpDatEnt(2),*) HB(HBidx)%id
                        CALL toupper(HB(HBidx)%id)
                        READ(InpDatEnt(3),*) HB(HBidx)%FBid
                        CALL toupper(HB(HBidx)%FBid)
                        READ(InpDatEnt(3),*) HB(HBidx)%Shape
                        CALL toupper(HB(HBidx)%Shape)
                    CASE('HB11')
                       READ(InpDatEnt(2),*) HB(HBidx)%Cell1
                       READ(InpDatEnt(3),*) HB(HBidx)%Cell2
                       HB(HBidx)%Cells=HB(HBidx)%Cell2-HB(HBidx)%Cell1+1_sik
                       ALLOCATE(HB(HBidx)%sidx(HB(HBidx)%Cells))
                       HB(HBidx)%sidx=0_sik
                    CASE('HB15')
                       READ(InpDatEnt(2),*) HB(HBidx)%Qff
                       ALLOCATE(HB(HBidx)%Qnorm(HB(HBidx)%Cells))
                       HB(HBidx)%Qnorm=1.0_sdk/HB(HBidx)%Cells 
                    CASE('HB16')
                        DO j=1,(HB(HBidx)%Cells)
                            READ(InpDatEnt(j+1),*) HB(HBidx)%Qnorm(j)
                        END DO
                    END SELECT
                END IF
            END IF
        END IF
    END DO

END SUBROUTINE ReadInpFile
