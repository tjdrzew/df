SUBROUTINE ScanInpFile

    USE IntrType
    USE Param
    USE File
    USE Solver
    USE Table
    USE BlockStr
    USE Allocs

    IMPLICIT NONE
!
! THE PURPOSE OF THIS ROUTINE IS to scan the input file to obtain the  
! the information about the memory allocation and variable initialization  
!
! Programmed by ;
!   NAME         : Tim Drzewiecki
!   ORGANIZATION : Solo Mission  
!   DATE         : 08/13/2012
!
! Subroutine Argument Descriptions:
!   N/A
!
! Variable Declarations;
!   Subroutine Arguments:
!    N/A
!   Local Variables:
    INTEGER(sik)         :: i                          ! loop index
    INTEGER(sik)         :: LinNum
    INTEGER(sik)         :: err

    LOGICAL              :: EndOFile
    LOGICAL              :: CntSlvrCards               ! Flag to start couting # of compositions       
    LOGICAL              :: CntTables                  ! Flag to start couting TableDim                  
 
   !Initialization
    EndOFile     = .FALSE.
    CntTables    = .FALSE.
    CntSlvrCards = .FALSE.    
    LinNum      = 0_sik           
    NumFB       = 0_sik
    NumBB       = 0_sik
    NumJB       = 0_sik
    NumGB       = 0_sik
    NumHB       = 0_sik
    NumOfTables = 0_sik
    

    DO WHILE(.NOT.EndOFile)
        READ(infile,'(a1024)', IOSTAT = err) OneLine
        LinNum = LinNum + 1
        IF(err < 0) THEN
            EndOFile = .TRUE.
        ELSE
            Probe = OneLine(1:1)
            IF(oneline /= BLANK .AND. probe /= BANG) THEN 
                CALL DCompLine(OneLine, InpDatTyp, InpDatEnt, InpLinCom, NumDat)
                IF(NumDat >= 0) THEN
                    READ(InpDatEnt(1),*) CardName  
                    CALL toupper(CardName)
                   ! Count the number of SOLVER cards
                    IF(CntSlvrCards .AND. (InpDatTyp(1) == 'NUM') ) NumOfSlvrCards = NumOfSlvrCards + 1_sik
                    IF(CntSlvrCards .AND. ((probe /= BLANK) .OR. (InpDatTyp(1) /= 'NUM')) ) THEN
                        CntSlvrCards = .FALSE.
                    END IF   
                   ! Determine TableDim
                    IF(CntTables .AND. (InpDatTyp(1) == 'NUM') ) TableDim = TableDim + 1_sik
                    IF(CntTables .AND. ((probe /= BLANK) .OR. (InpDatTyp(1) /= 'NUM')) ) THEN
                        CntTables = .FALSE.
                    END IF  
                    SELECT CASE(CardName)
                    CASE('SLVR')
                        CntSlvrCards = .TRUE.
                    CASE('TBL')
                        NumOfTables = NumOfTables + 1_sik
                        CntTables =  .TRUE.
                    CASE('FB10')
                        NumFB = NumFB + 1_sik
                    CASE('BB10')
                        NumBB = NumBB + 1_sik
                    CASE('JB10')
                        NumJB = NumJB + 1_sik
                    CASE('GB10')
                        NumGB = NumGB + 1_sik
                    CASE('HB10')
                        NumHB = NumHB + 1_sik 
                    END SELECT
                END IF
            END IF
        END IF
    END DO        
    REWIND(infile)

    CALL dmalloc(TableBnds,NumOfTables,2)
    CALL dmalloc(IndVar,TableDim)
    CALL dmalloc(DepVar,TableDim)
    ALLOCATE(TCards(NumOfSlvrCards))     
    ALLOCATE(FB(NumFB))
    ALLOCATE(BB(NumBB))
    ALLOCATE(JB(NumJB))
    ALLOCATE(GB(NumGB))
    ALLOCATE(HB(NumHB))

END SUBROUTINE ScanInpFile
