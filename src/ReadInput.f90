SUBROUTINE ReadInput

    USE File
    USE Param

    IMPLICIT NONE

   !Get Input File
    CALL getarg(1,InFileName)
    IF(InFileName == BLANK) THEN
       STOP 'Input file not specified'
    END IF

    OutFileName=trim(adjustl(InFileName))//'.out'
    WRITE(*,'(25x,a2)') 'DF'
    OPEN(InFile,file=InFileName, status='old')  
    CALL ScanInpFile
    CALL ReadInpFile

    CLOSE(InFile)
    RETURN
END SUBROUTINE ReadInput
