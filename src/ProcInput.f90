SUBROUTINE ProcInput

    USE IntrType
    USE PriVars
    USE BlockStr
    USE Allocs
    USE Param

! THE PURPOSE OF THIS ROUTINE IS to allocate the problem dependent memory after
! that is specific to the PMRFuids module
!
! Programmed by ;
!   NAME         : Tim Drzewiecki
!   ORGANIZATION : University of Michigan
!   DATE         : 07/22/2011
!
! Subroutine Argument Descriptions:
!   N/A
!
! Variable Declarations;
!   Subroutine Arguments:
!    N/A
!   Local Variables:
    IMPLICIT NONE
    INTEGER(sik)           :: i     !Loop Control Variable
    INTEGER(sik)           :: j     !Loop Control Variable
    INTEGER(sik)           :: k     !Loop Control Variable
    INTEGER(sik)           :: vidx
    INTEGER(sik)           :: vidx1
    INTEGER(sik)           :: vidx2
    INTEGER(sik)           :: sidx
    INTEGER(sik)           :: cell
    INTEGER(sik)           :: jidx
    INTEGER(sik)           :: idx
    INTEGER(sik)           :: inFaceCell
    INTEGER(sik)           :: exFaceCell
    INTEGER(sik)           :: vcifDim
    INTEGER(sik)           :: vcitDim
    INTEGER(sik)           :: FVolID
    INTEGER(sik)           :: TVolID
    INTEGER(sik)           :: VinNumOfIJs
    INTEGER(sik)           :: VinNumOfOJs
    INTEGER(sik)           :: VoutNumOfIJs
    INTEGER(sik)           :: VoutNumOfOJs
    INTEGER(sik)           :: nij
    INTEGER(sik)           :: noj
    INTEGER(sik)           :: nig
    INTEGER(sik)           :: nog
    INTEGER(sik)           :: iji
    INTEGER(sik)           :: oji

    CHARACTER              :: inFace
    CHARACTER(len=20)      :: inID
    CHARACTER              :: exFace
    CHARACTER(len=20)      :: exID


   !Place volume and junction indices into BB,JB,FB,GB, and HB data structures
    vidx = 1_sik
    jidx = 1_sik
!    vjidx = 1_sik
    sidx = 1_sik

    DO i=1,NumBB
        BB(i)%vidx = vidx
        vidx = vidx + 1_sik
    END DO

    DO i=1,NumJB
        JB(i)%jidx = jidx
        jidx = jidx + 1_sik
    END DO

    DO i=1,NumFB
        DO j=1,(FB(i)%Cells-1)
            FB(i)%vidx(j) = vidx
            FB(i)%jidx(j) = jidx
            vidx = vidx + 1_sik
            jidx = jidx + 1_sik
        END DO
        j=FB(i)%cells
        FB(i)%vidx(j) = vidx
        vidx = vidx + 1_sik
    END DO

!    DO i=1,NumGB
!        DO j=1,GB(i)%gaps
!            GB(i)%gidx(j)=vjidx
!            vjidx = vjidx + 1_sik
!        END DO
!    END DO

   DO i=1,NumHB
       CALL FindFluidBlock(HB(i)%FBid,idx)
       cell=HB(i)%Cell1
       DO j=1,HB(i)%Cells
           vidx=FB(idx)%vidx(cell)
           HB(i)%sidx(j)=sidx
           FB(idx)%sidx(cell)=sidx
           sidx=sidx+1_sik
           cell=cell+1_sik
       END DO
   END DO

  !Place volume data: Volume, VArea, Length, Height, VShape, HydDia, Roughness
    DO i=1,NumBB
        vidx = BB(i)%vidx
        Volume(vidx)    = 0.0_sdk
        VArea(vidx)     = 0.0_sdk
        Length(vidx)    = 0.0_sdk
        Height(vidx)    = 0.0_sdk
        HydDia(vidx)    = 0.0_sdk
        Roughness(vidx) = 0.0_sdk
    END DO

    DO i=1,NumFB
        DO j=1,FB(i)%Cells
            vidx = FB(i)%vidx(j)
            VArea(vidx)  = FB(i)%area(j)
            Length(vidx) = FB(i)%length(j)
            Height(vidx) = FB(i)%height(j)
            Volume(vidx) = FB(i)%volume(j)
            IF (Volume(vidx)< 0.0) Volume(vidx) = VArea(vidx)*Length(vidx)
            HydDia(vidx) = FB(i)%hydDia(j)
            Roughness(vidx) = FB(i)%roughness
        END DO
    END DO

   !Set up JCI
    DO i=1,NumJB
        jidx  = JB(i)%jidx
        inFace = JB(i)%inFace(1:1)
        inID   = JB(i)%inID
        exFace = JB(i)%exFace(1:1)
        exID   = JB(i)%exID
        SELECT CASE(inFace)
        CASE('V')
            CALL FindBoundaryBlock(inID,idx)
            IF (idx > 0_sik) THEN
                vidx = BB(idx)%vidx
                jci(jidx,1) = vidx
            ELSE
                WRITE(*, '(a19,a20,a10)') 'inFace on JB ',JB(i)%id,' not found'
                STOP
            END IF
        CASE('L')
            CALL FindFluidBlock(inID,idx)
            IF (idx > 0_sik) THEN
                inFaceCell = 1_sik
                vidx = FB(idx)%vidx(inFaceCell)
                jci(jidx,1) = vidx
            ELSE
                WRITE(*, '(a19,a20,a10)') 'inFace on JB ',JB(i)%id,' not found'
                STOP
            END IF
        CASE('R')
            CALL FindFluidBlock(inID,idx)
            IF (idx > 0_sik) THEN
                inFaceCell = FB(idx)%cells
                vidx = FB(idx)%vidx(inFaceCell)
                jci(jidx,1) = vidx
            ELSE
                WRITE(*, '(a19,a20,a10)') 'inFace on JB ',JB(i)%id,' not found'
                STOP
            END IF
        CASE('C')
            CALL FindFluidBlock(inID,idx)
            IF (idx > 0_sik) THEN
                READ(JB(i)%inFace(2:8),*) inFaceCell
                IF (inFaceCell > FB(idx)%cells) THEN
                    WRITE(*, '(a19,a20,a10)') 'inFace on JB ',JB(i)%id,' not found'
                    STOP
                ELSE
                    vidx = FB(idx)%vidx(inFaceCell)
                    jci(jidx,1) = vidx
                END IF
            ELSE
                WRITE(*, '(a19,a20,a10)') 'inFace on JB ',JB(i)%id,' not found'
                STOP
            END IF
        END SELECT

        SELECT CASE(exFace)
        CASE('V')
            CALL FindBoundaryBlock(exID,idx)
            IF (idx > 0_sik) THEN
                vidx = BB(idx)%vidx
                jci(jidx,2) = vidx
            ELSE
                WRITE(*, '(a19,a20,a10)') 'exFace on JB ',JB(i)%id,' not found'
                STOP
            END IF
        CASE('L')
            CALL FindFluidBlock(exID,idx)
            IF (idx > 0_sik) THEN
                exFaceCell = 1_sik
                vidx = FB(idx)%vidx(exFaceCell)
                jci(jidx,2) = vidx
            ELSE
                WRITE(*, '(a19,a20,a10)') 'exFace on JB ',JB(i)%id,' not found'
                STOP
            END IF
        CASE('R')
            CALL FindFluidBlock(exID,idx)
            IF (idx > 0_sik) THEN
                exFaceCell = FB(idx)%cells
                vidx = FB(idx)%vidx(exFaceCell)
                jci(jidx,2) = vidx
            ELSE
                WRITE(*, '(a19,a20,a10)') 'exFace on JB ',JB(i)%id,' not found'
                STOP
            END IF
        CASE('C')
            CALL FindFluidBlock(exID,idx)
            IF (idx > 0_sik) THEN
                READ(JB(i)%exFace(2:8),*) exFaceCell
                IF (exFaceCell > FB(idx)%cells) THEN
                    WRITE(*, '(a19,a20,a10)') 'exFace on JB ',JB(i)%id,' not found'
                    STOP
                ELSE
                    vidx = FB(idx)%vidx(exFaceCell)
                    jci(jidx,2) = vidx
                END IF
            ELSE
                WRITE(*, '(a19,a20,a10)') 'exFace on JB ',JB(i)%id,' not found'
                STOP
            END IF
        END SELECT
    END DO

    DO i=1,NumFB
        DO j=1,(FB(i)%cells-1)
            jidx = FB(i)%jidx(j)
            vidx = FB(i)%vidx(j)
            jci(jidx,1) = vidx
            vidx = FB(i)%vidx(j+1)
            jci(jidx,2) = vidx
        END DO
    END DO

!   !Set up GCI
!    DO i=1,NumGB
!        inFace = GB(i)%inFace(1:1)
!        inID   = GB(i)%inID
!        exFace = GB(i)%exFace(1:1)
!        exID   = GB(i)%exID
!        SELECT CASE(inFace)
!        CASE('C')
!            CALL FindFluidBlock(inID,idx)
!            IF (idx > 0_sik) THEN
!                IF (GB(i)%gaps > FB(idx)%cells) THEN
!                    WRITE(IOErrF, '(a21,a20)') 'Too many gaps on GB ',GB(i)%id
!                    STOP
!                ELSE
!                    cidx=GB(i)%inCell1
!                    DO j=1,GB(i)%gaps
!                        vjidx=GB(i)%gidx(j)
!                        gci(vjidx,1)=FB(idx)%vidx(cidx)
!                        cidx=cidx+1_sik
!                    END DO
!                END IF
!            ELSE
!                WRITE(IOErrF, '(a13,a20,a10)') 'inFace on GB ',GB(i)%id,' not found'
!                STOP
!            END IF
!        CASE('N')
!            CALL FindNodeBlock(inID,idx)
!            IF (idx > 0_sik) THEN
!                vjidx=GB(i)%gidx(1)
!                gci(vjidx,1)=NB(idx)%vidx
!            ELSE
!                WRITE(IOErrF, '(a13,a20,a10)') 'inFace on GB ',GB(i)%id,' not found'
!                STOP
!            END IF
!        END SELECT
!        SELECT CASE(exFace)
!        CASE('C')
!            CALL FindFluidBlock(exID,idx)
!            IF (idx > 0_sik) THEN
!                IF (GB(i)%gaps > FB(idx)%cells) THEN
!                    WRITE(IOErrF, '(a21,a20)') 'Too many gaps on GB ',GB(i)%id
!                    STOP
!                ELSE
!                    cidx=GB(i)%exCell1
!                    DO j=1,GB(i)%gaps
!                        vjidx=GB(i)%gidx(j)
!                        gci(vjidx,2)=FB(idx)%vidx(cidx)
!                        cidx=cidx+1_sik
!                    END DO
!                END IF
!            ELSE
!                WRITE(IOErrF, '(a13,a20,a10)') 'exFace on GB ',GB(i)%id,' not found'
!                STOP
!            END IF
!        CASE('N')
!            CALL FindNodeBlock(exID,idx)
!            IF (idx > 0_sik) THEN
!                vjidx=GB(i)%gidx(1)
!                gci(vjidx,2)=NB(idx)%vidx
!            ELSE
!                WRITE(IOErrF, '(a13,a20,a10)') 'exFace on GB ',GB(i)%id,' not found'
!                STOP
!            END IF
!        END SELECT
!    END DO

   !Allocate memory for vci matrices
    vciNF=0_sik
    vciNT=0_sik
    DO i=1,juncs
        vidx=jci(i,1)
        vciNF(vidx)=vciNF(vidx)+1_sik
        vidx=jci(i,2)
        vciNT(vidx)=vciNT(vidx)+1_sik
    END DO

    vcifDim = 0_sik
    DO i=1,vols
        vcifDim = MAX(vcifDim,vciNF(i))
    END DO
    CALL dmalloc(vciF,vols,vcifDim)

    vcitDim = 0_sik
    DO i=1,vols
        vcitDim = MAX(vcitDim,vciNT(i))
    END DO
    CALL dmalloc(vciT,vols,vcitDim)

   !Set up vci matrices
    vciNF=0_sik
    vciNT=0_sik
    DO i=1,juncs
        vidx = jci(i,1)
        vciNF(vidx)=vciNF(vidx)+1_sik
        vciF(vidx,vciNF(vidx))=i
        vidx = jci(i,2)
        vciNT(vidx)=vciNT(vidx)+1_sik
        vciT(vidx,vciNT(vidx))=i
    END DO

!   !Allocate memory for gvci matrices
!    gvciNF=0_sik
!    gvciNT=0_sik
!    DO i=1,vjuncs
!        vidx1=gci(i,1)
!        vidx2=gci(i,2)
!        gvciNF(vidx1)=gvciNF(vidx1)+1_sik
!        gvciNT(vidx2)=gvciNT(vidx2)+1_sik
!    END DO
!
!    vcifDim = 0_sik
!    DO i=1,vols
!        vcifDim = MAX(vcifDim,gvciNF(i))
!    END DO
!    CALL dmalloc(gvciF,vols,vcifDim)
!
!    vcitDim = 0_sik
!    DO i=1,vols
!        vcitDim = MAX(vcitDim,gvciNT(i))
!    END DO
!    CALL dmalloc(gvciT,vols,vcitDim)

!   !Set up gvci matrices
!    gvciNF=0_sik
!    gvciNT=0_sik
!    DO i=1,vjuncs
!        vidx1 = gci(i,1)
!        vidx2 = gci(i,2)
!        gvciNF(vidx1)=gvciNF(vidx1)+1_sik
!        gvciNT(vidx2)=gvciNT(vidx2)+1_sik
!        gvciF(vidx1,gvciNF(vidx1))=i
!        gvciT(vidx2,gvciNT(vidx2))=i
!    END DO

   !Setup f2sci
    DO i=1,numFB
        DO j=1,FB(i)%Cells
            vidx=FB(i)%vidx(j)
            sidx=FB(i)%sidx(j)
            IF (sidx>0_sik) s2fci(sidx)=vidx
        END DO
    END DO

   !Set up JAREA, JLength, JHeight, JHydDia, KFLoss, KRLoss, JForm
    DO i=1,NumJB
        jidx=JB(i)%jidx
        vidx1=jci(jidx,1)
        vidx2=jci(jidx,2)
        inFace = JB(i)%inFace(1:1)
        exFace = JB(i)%exFace(1:1)
        JLength(jidx)     = (Length(vidx1)+Length(vidx2))/2.0_sdk
        KFLoss(jidx)      = JB(i)%KFLoss
        KRLoss(jidx)      = JB(i)%KRLoss
        JForm(jidx)       = JB(i)%jform
        IF (inFace == 'V') THEN
            JArea(jidx)=VArea(vidx2)
            JHydDia(jidx) = HydDia(vidx2)
            JHeight(jidx) = Height(vidx2)/2.0_sdk
        ELSEIF (exFace == 'V') THEN
            JArea(jidx)=VArea(vidx1)
            JHydDia(jidx)=HydDia(vidx1)
            JHeight(jidx) = Height(vidx1)/2.0_sdk
        ELSE
            IF (VArea(vidx2)>=VArea(vidx1)) THEN
                JArea(jidx) =VArea(vidx1)
            ELSE
                JArea(jidx) =VArea(vidx2)
            END IF
            JHeight(jidx) = (Height(vidx1)+Height(vidx2))/2.0_sdk
            JHydDia(jidx) = (Length(vidx1)+Length(vidx2))/(Length(vidx1)/HydDia(vidx1)+Length(vidx2)/HydDia(vidx2))
        END IF
    END DO

    DO i=1,NumFB
        DO j=1,(FB(i)%cells-1)
            jidx=FB(i)%jidx(j)
            vidx1=jci(jidx,1)
            vidx2=jci(jidx,2)
            JArea(jidx)   = MIN(VArea(vidx1),VArea(vidx2))
            JLength(jidx) = (Length(vidx1)+Length(vidx2))/2.0_sdk
            IF ((ABS(Height(vidx1))<small).OR.(ABS(Height(vidx2))<small)) THEN
                JHeight(jidx)=0.0_sdk
            ELSE
                JHeight(jidx) = (Height(vidx1)+Height(vidx2))/2.0_sdk
            END IF
            JHydDia(jidx)  = (Length(vidx1)+Length(vidx2))/(Length(vidx1)/HydDia(vidx1)+Length(vidx2)/HydDia(vidx2))
            KFLoss(jidx)   = FB(i)%KLoss(j)
            KRLoss(jidx)   = FB(i)%KLoss(j)
            JForm(jidx)    = FB(i)%jform(j)
        END DO
    END DO
  
  ! !Set up WGap, LGap, SCKMult, SCKModel
  !  DO i=1,NumGB
  !      DO j=1,GB(i)%gaps
  !          vjidx=GB(i)%gidx(j)
  !          WGap(vjidx)=GB(i)%Width(j)
  !          LGap(vjidx)=GB(i)%Length(j)
  !          SCKModel(vjidx)=GB(i)%KModel(j)
  !          SCKMult(vjidx)=GB(i)%KMult(j)
  !      END DO
  !  END DO

  ! Check for neighbors of abrupt area changes
   DO i=1,juncs
        FVolID=jci(i,1)
        TVolID=jci(i,2)
        VinNumOfIJs  = vciNT(FVolID)
        VoutNumOfOJs = vciNF(TVolID)
        IF (JForm(i)=='A') THEN
            DO j=1,VinNumOfIJs
                jidx=vciT(FVolID,j)
                IF (JArea(jidx)/=JArea(i)) JForm(jidx)='A'
            END DO
            DO j=1,VoutNumOfOJs
                jidx=vciF(TVolID,j)
                IF (JArea(jidx)/=JArea(i)) JForm(jidx)='A'
            END DO
        END IF
   END DO


   !Classify Junctions
    DO i=1,numJB
        jidx=JB(i)%jidx
        FVolID=jci(jidx,1)
        TVolID=jci(jidx,2)
        VinNumOfIJs  = vciNT(FVolID)
        VinNumOfOJs  = vciNF(FVolID)
        VoutNumOfIJs = vciNT(TVolID)
        VoutNumOfOJs = vciNF(TVolID)

        IF ((JB(i)%bFlag == 'W').OR.(JB(i)%bFlag == 'U')) THEN
            JuncClass(jidx)=-1_sik
        ELSEIF (JForm(jidx) == 'A') THEN
            FVolID=jci(jidx,1)
            TVolID=jci(jidx,2)
            IF (VArea(FVolID)>=VArea(TVolID)) THEN !Contraction
                JuncClass(jidx)=8_sik
            ELSE  !Expansion
                JuncClass(jidx)=9_sik
            END IF
        ELSE
            IF    ((VinNumOfIJs==1_sik).AND.(VinNumOfOJs==1_sik).AND.(VoutNumOfIJs==1_sik).AND.(VoutNumOfOJs==1_sik))THEN
                JuncClass(jidx)=1_sik
            ELSEIF((VinNumOfIJs> 1_sik).AND.(VinNumOfOJs==1_sik).AND.(VoutNumOfIJs==1_sik).AND.(VoutNumOfOJs==1_sik))THEN
                JuncClass(jidx)=2_sik
            ELSEIF((VinNumOfIJs==1_sik).AND.(VinNumOfOJs> 1_sik).AND.(VoutNumOfIJs==1_sik).AND.(VoutNumOfOJs==1_sik))THEN
                JuncClass(jidx)=3_sik
            ELSEIF((VinNumOfIJs==1_sik).AND.(VinNumOfOJs==1_sik).AND.(VoutNumOfIJs> 1_sik).AND.(VoutNumOfOJs==1_sik))THEN
                JuncClass(jidx)=4_sik
            ELSEIF((VinNumOfIJs==1_sik).AND.(VinNumOfOJs==1_sik).AND.(VoutNumOfIJs==1_sik).AND.(VoutNumOfOJs> 1_sik))THEN
                JuncClass(jidx)=5_sik
            ELSEIF((VinNumOfIJs==0_sik).AND.(VinNumOfOJs>=1_sik).AND.(VoutNumOfIJs==1_sik).AND.(VoutNumOfOJs==1_sik))THEN
                JuncClass(jidx)=6_sik
            ELSEIF((VinNumOfIJs==1_sik).AND.(VinNumOfOJs==1_sik).AND.(VoutNumOfIJs>=1_sik).AND.(VoutNumOfOJs==0_sik))THEN
                JuncClass(jidx)=7_sik
            ELSE
                WRITE(*,'(a120,i20)') 'ERROR: Unable to Classify Junction', jidx
                STOP
            END IF
        END IF
    END DO

    DO i=1,numFB
        DO j=1,(FB(i)%cells-1_sik)
            jidx=FB(i)%jidx(j)
            FVolID=jci(jidx,1)
            TVolID=jci(jidx,2)
            VinNumOfIJs  = vciNT(FVolID)
            VinNumOfOJs  = vciNF(FVolID)
            VoutNumOfIJs = vciNT(TVolID)
            VoutNumOfOJs = vciNF(TVolID)

            IF (JForm(jidx)=='A') THEN
                FVolID=jci(jidx,1)
                TVolID=jci(jidx,2)
                IF (VArea(FVolID)>=VArea(TVolID)) THEN !Contraction
                    JuncClass(jidx)=8_sik
                ELSE  !Expansion
                    JuncClass(jidx)=9_sik
                END IF
            ELSE
                JuncClass(jidx)=1_sik
            END IF
        END DO
    END DO

   !Calculate nzAum, setup iAum, and allocate memory for Aum and jAum
    nzAum=0_sik
    IF (juncs > 0_sik) iAum(1)=1_sik
    DO i=1,juncs
        vidx1=jci(i,1)
        vidx2=jci(i,2)
        VinNumOfIJs=vciNT(vidx1)
        VoutNumOfOJs=vciNF(vidx2)
        IF ((JuncClass(i)<0).OR.(JuncClass(i)==8).OR.(JuncClass(i)==9)) THEN
            nzAum=nzAum+1_sik
        ELSE
            nzAum=nzAum+VinNumOfIJs+VoutNumOfOJs+1_sik
        END IF
        iAum(i+1)=nzAum+1_sik
    END DO
    CALL dmalloc(Aum,nzAum)
    CALL dmalloc(jAum,nzAum)

!   !Calulate nzAv, setup SCiAv and SCjAv
!    nzAv=vjuncs
!    DO i=1,vjuncs
!        SCiAv(i)=i
!        SCjAv(i)=i
!    END DO
!    SCiAv(vjuncs+1)=vjuncs+1_sik

   !Calculate nzAalpha,nzApm, and nzAhm - setup iAalpha,iApm, and iAhm - and allocate memory for Aalpha,jAalpha,Apm,jApm,Ahm, and jAhm
    nzAalpha=0_sik
    nzApm=0_sik
    nzAhm=0_sik
    IF (vols > 0_sik) THEN
        iAalpha(1)=1_sik
        iApm(1)=1_sik
        iAhm(1)=1_sik
    END IF
    DO i=1,vols
        IF (Volume(i)==0.0_sdk) THEN
            nzAalpha=nzAalpha+1_sik
            nzApm=nzApm+1_sik
            nzAhm=nzAhm+1_sik
        ELSE
            nij=vciNT(i)
            noj=vciNF(i)
            nig=0_sik
            nog=0_sik
            nzAalpha=nzAalpha+nij+noj+nig+nog+1_sik
            nzApm=nzApm+nig+nog+1_sik
            nzAhm=nzAhm+nij+noj+nig+nog+1_sik
            DO k=1,nij
                jidx = vciT(i,k)
                IF (JuncClass(jidx) > 0_sik) nzApm=nzApm+1_sik
            END DO
            DO k=1,noj
                jidx = vciF(i,k)
                IF (JuncClass(jidx) > 0_sik) nzApm=nzApm+1_sik
            END DO
        END IF
        iAalpha(i+1)=nzAalpha+1_sik
        iApm(i+1)=nzApm+1_sik
        iAhm(i+1)=nzAhm+1_sik
    END DO
    CALL dmalloc(Aalpha,nzAalpha)
    CALL dmalloc(jAalpha,nzAalpha)
    CALL dmalloc(Apm,nzApm)
    CALL dmalloc(jApm,nzApm)
    CALL dmalloc(Ahm,nzAhm)
    CALL dmalloc(jAhm,nzAhm)

END SUBROUTINE ProcInput
