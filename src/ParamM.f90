MODULE Param

    USE IntrType

    IMPLICIT NONE

    CHARACTER(len=1),PARAMETER :: DOT='.'
    CHARACTER(len=1),PARAMETER :: BANG='!'
    CHARACTER(len=1),PARAMETER :: BLANK = ' '
    CHARACTER(len=1),PARAMETER :: SLASH = '/'
    CHARACTER(len=1),PARAMETER :: AST = '*'

    REAL(sdk),PARAMETER        :: zero   = 0.0_sdk
    REAL(sdk),PARAMETER        :: small  = 1.0E-15_sdk
    REAL(sdk),PARAMETER        :: Pa2MPa = 1.0E-6_sdk
END MODULE Param
