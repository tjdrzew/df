MODULE Allocs

  USE Param

  PARAMETER (N_SGL_DIGITS=6)
  PARAMETER (N_DBL_DIGITS=15)
  PARAMETER (N_INT_ORDER=8)
  PARAMETER (NBFT=SELECTED_REAL_KIND(N_SGL_DIGITS))
  PARAMETER (NBDT=SELECTED_REAL_KIND(N_DBL_DIGITS))
  PARAMETER (NBIT=SELECTED_INT_KIND(N_INT_ORDER))
  INTERFACE dmalloc
     MODULE PROCEDURE mallocf1
     MODULE PROCEDURE mallocd1
     MODULE PROCEDURE malloci1
     MODULE PROCEDURE mallocl1
     MODULE PROCEDURE mallocf2
     MODULE PROCEDURE mallocd2
     MODULE PROCEDURE malloci2
     MODULE PROCEDURE mallocl2
     MODULE PROCEDURE mallocf3
     MODULE PROCEDURE mallocd3
     MODULE PROCEDURE malloci3
     MODULE PROCEDURE mallocf4
     MODULE PROCEDURE mallocd4
     MODULE PROCEDURE malloci4
     MODULE PROCEDURE mallocf5
     MODULE PROCEDURE mallocd5
     MODULE PROCEDURE malloci5
     MODULE PROCEDURE mallocf7
     MODULE PROCEDURE mallocd7
     MODULE PROCEDURE malloci7
  END INTERFACE
  INTERFACE dmalloc0
     MODULE PROCEDURE mallocf01
     MODULE PROCEDURE mallocd01
     MODULE PROCEDURE malloci01
     MODULE PROCEDURE mallocl01
     MODULE PROCEDURE mallocf02
     MODULE PROCEDURE mallocd02
     MODULE PROCEDURE malloci02
     MODULE PROCEDURE mallocl02
     MODULE PROCEDURE mallocf03
     MODULE PROCEDURE mallocd03
     MODULE PROCEDURE malloci03
     MODULE PROCEDURE mallocf04
     MODULE PROCEDURE mallocd04
     MODULE PROCEDURE malloci04
  END INTERFACE
!
CONTAINS
  
  SUBROUTINE mallocf1(a,n1)
    USE Memory
    USE IntrType
    USE Param
    REAL(NBFT),POINTER :: a(:)
    ALLOCATE(a(n1))
    a=zero
    nbytesf=nbytesf+SIZE(a)*rvp
  END SUBROUTINE mallocf1
  !
  SUBROUTINE mallocd1(a,n1)
    USE Memory
    USE IntrType
    USE Param
    REAL(NBDT),POINTER :: a(:)
    ALLOCATE(a(n1))
    a=zero
    nbytesf=nbytesf+SIZE(a)*NBDT
  END SUBROUTINE mallocd1
  !
  SUBROUTINE malloci1(a,n1)
    USE Memory
    USE IntrType
    USE Param
    INTEGER(NBIT),POINTER :: a(:)
    ALLOCATE(a(n1))
    a=0
    nbytesf=nbytesf+SIZE(a)*NBIT
  END SUBROUTINE malloci1
  !
  SUBROUTINE mallocl1(a,n1)
    USE IntrType
    USE Memory
    USE Param
    LOGICAL,POINTER :: a(:)
    ALLOCATE(a(n1))
    a=.FALSE.
    nbytesf=nbytesf+SIZE(a)
  END SUBROUTINE mallocl1
  !
  SUBROUTINE mallocf2(a,n1,n2)
    USE Memory
    USE Param
    USE IntrType
    REAL(NBFT),POINTER :: a(:,:)
    ALLOCATE(a(n1,n2))
    a=zero
    nbytesf=nbytesf+SIZE(a)*rvp
  END SUBROUTINE mallocf2
  !
  SUBROUTINE mallocd2(a,n1,n2)
    USE IntrType
    USE Memory
    USE Param
    REAL(NBDT),POINTER :: a(:,:)
    ALLOCATE(a(n1,n2))
    a=zero
    nbytesf=nbytesf+SIZE(a)*NBDT
  END SUBROUTINE mallocd2
  !
  SUBROUTINE malloci2(a,n1,n2)
    USE Memory
    USE Param
    USE IntrType
    INTEGER(NBIT),POINTER :: a(:,:)
    ALLOCATE(a(n1,n2))
    a=0
    nbytesf=nbytesf+SIZE(a)*NBIT
  END SUBROUTINE malloci2
  !
  SUBROUTINE mallocl2(a,n1,n2)
    USE Memory
    USE Param
    USE IntrType
    LOGICAL,POINTER :: a(:,:)
    ALLOCATE(a(n1,n2))
    a=.FALSE.
    nbytesf=nbytesf+SIZE(a)
  END SUBROUTINE mallocl2
  !
  SUBROUTINE mallocf3(a,n1,n2,n3)
    USE Memory
    USE Param
    USE IntrType
    REAL(NBFT),POINTER :: a(:,:,:)
    ALLOCATE(a(n1,n2,n3))
    a=zero
    nbytesf=nbytesf+SIZE(a)*rvp
  END SUBROUTINE mallocf3
!
  SUBROUTINE mallocd3(a,n1,n2,n3)
    USE Memory
    USE IntrType
    USE Param
    REAL(NBDT),POINTER :: a(:,:,:)
    ALLOCATE(a(n1,n2,n3))
    a=zero
    nbytesf=nbytesf+SIZE(a)*NBDT
  END SUBROUTINE mallocd3
  !
  SUBROUTINE malloci3(a,n1,n2,n3)
    USE Memory
    USE Param
    USE IntrType
    INTEGER(NBIT),POINTER :: a(:,:,:)
    ALLOCATE(a(n1,n2,n3))
    a=0
    nbytesf=nbytesf+SIZE(a)*NBIT
  END SUBROUTINE malloci3
  !
  SUBROUTINE mallocf4(a,n1,n2,n3,n4)
    USE Memory
    USE Param
    USE IntrType
    REAL(NBFT),POINTER :: a(:,:,:,:)
    ALLOCATE(a(n1,n2,n3,n4))
    a=zero
    nbytesf=nbytesf+SIZE(a)*rvp
  END SUBROUTINE mallocf4
  !
  SUBROUTINE mallocd4(a,n1,n2,n3,n4)
    USE Memory
    USE Param
    USE IntrType
    REAL(NBDT),POINTER :: a(:,:,:,:)
    ALLOCATE(a(n1,n2,n3,n4))
    a=zero
    nbytesf=nbytesf+SIZE(a)*NBDT
  END SUBROUTINE mallocd4
  !
  SUBROUTINE malloci4(a,n1,n2,n3,n4)
    USE Memory
    USE Param
    USE IntrType
    INTEGER(NBIT),POINTER :: a(:,:,:,:)
    ALLOCATE(a(n1,n2,n3,n4))
    a=0
    nbytesf=nbytesf+SIZE(a)*NBIT
  END SUBROUTINE malloci4
  !
  SUBROUTINE mallocf5(a,n1,n2,n3,n4,n5)
    USE Memory
    USE Param
    USE IntrType
    REAL(NBFT),POINTER :: a(:,:,:,:,:)
    ALLOCATE(a(n1,n2,n3,n4,n5))
    a=zero
    nbytesf=nbytesf+SIZE(a)*rvp
  END SUBROUTINE mallocf5
  !
  SUBROUTINE mallocd5(a,n1,n2,n3,n4,n5)
    USE Memory
    USE Param
    USE IntrType
    REAL(NBDT),POINTER :: a(:,:,:,:,:)
    ALLOCATE(a(n1,n2,n3,n4,n5))
    a=zero
    nbytesf=nbytesf+SIZE(a)*NBDT
  END SUBROUTINE mallocd5
  !
  SUBROUTINE malloci5(a,n1,n2,n3,n4,n5)
    USE Memory
    USE Param
    USE IntrType
    INTEGER(NBIT),POINTER :: a(:,:,:,:,:)
    ALLOCATE(a(n1,n2,n3,n4,n5))
    a=0
    nbytesf=nbytesf+SIZE(a)*NBIT
  END SUBROUTINE malloci5
  
  SUBROUTINE mallocf7(a,n1,n2,n3,n4,n5,n6,n7)
    USE Memory
    USE Param
    USE IntrType
    REAL(NBFT),POINTER :: a(:,:,:,:,:,:,:)
    ALLOCATE(a(n1,n2,n3,n4,n5,n6,n7))
    a=zero
    nbytesf=nbytesf+SIZE(a)*rvp
  END SUBROUTINE mallocf7
  !
  SUBROUTINE mallocd7(a,n1,n2,n3,n4,n5,n6,n7)
    USE Memory
    USE Param
    USE IntrType
    REAL(NBDT),POINTER :: a(:,:,:,:,:,:,:)
    ALLOCATE(a(n1,n2,n3,n4,n5,n6,n7))
    a=zero
    nbytesf=nbytesf+SIZE(a)*NBDT
  END SUBROUTINE mallocd7
  !
  SUBROUTINE malloci7(a,n1,n2,n3,n4,n5,n6,n7)
    USE Memory
    USE Param
    USE IntrType
    INTEGER(NBIT),POINTER :: a(:,:,:,:,:,:,:)
    ALLOCATE(a(n1,n2,n3,n4,n5,n6,n7))
    a=0
    nbytesf=nbytesf+SIZE(a)*NBIT
  END SUBROUTINE malloci7
  !
  SUBROUTINE mallocf01(a,nb1,ne1)
    USE Param
    USE Memory
    USE IntrType
    REAL(NBFT),POINTER :: a(:)
    ALLOCATE(a(nb1:ne1))
    a=zero
    nbytesf=nbytesf+SIZE(a)*rvp
  END SUBROUTINE mallocf01
  !
  SUBROUTINE mallocd01(a,nb1,ne1)
    USE Memory
    USE Param
    USE IntrType
    REAL(NBDT),POINTER :: a(:)
    ALLOCATE(a(nb1:ne1))
    a=zero
    nbytesf=nbytesf+SIZE(a)*NBDT
  END SUBROUTINE mallocd01
  !
  SUBROUTINE malloci01(a,nb1,ne1)
    USE Memory
    USE Param
    USE IntrType
    INTEGER(NBIT),POINTER :: a(:)
    ALLOCATE(a(nb1:ne1))
    a=0
    nbytesf=nbytesf+SIZE(a)*NBIT
  END SUBROUTINE malloci01
  !
  SUBROUTINE mallocl01(a,nb1,ne1)
    USE Memory
    USE Param
    USE IntrType
    LOGICAL,POINTER :: a(:)
    ALLOCATE(a(nb1:ne1))
    a=.FALSE.
    nbytesf=nbytesf+SIZE(a)
  END SUBROUTINE mallocl01
  !
  SUBROUTINE mallocf02(a,nb1,ne1,nb2,ne2)
    USE Memory
    USE Param
    USE IntrType
    REAL(NBFT),POINTER :: a(:,:)
    ALLOCATE(a(nb1:ne1,nb2:ne2))
    a=zero
    nbytesf=nbytesf+SIZE(a)*rvp
  END SUBROUTINE mallocf02
  !
  SUBROUTINE mallocd02(a,nb1,ne1,nb2,ne2)
    USE Memory
    USE Param
    USE IntrType
    REAL(NBDT),POINTER :: a(:,:)
    ALLOCATE(a(nb1:ne1,nb2:ne2))
    a=zero
    nbytesf=nbytesf+SIZE(a)*NBDT
  END SUBROUTINE mallocd02
  !
  SUBROUTINE malloci02(a,nb1,ne1,nb2,ne2)
    USE Memory
    USE Param
    USE IntrType
    INTEGER(NBIT),POINTER :: a(:,:)
    ALLOCATE(a(nb1:ne1,nb2:ne2))
    a=0
    nbytesf=nbytesf+SIZE(a)*NBIT
  END SUBROUTINE malloci02
  !
  SUBROUTINE mallocl02(a,nb1,ne1,nb2,ne2)
    USE Param
    USE Memory
    USE IntrType
   LOGICAL,POINTER :: a(:,:)
   ALLOCATE(a(nb1:ne1,nb2:ne2))
   a=.FALSE.
   nbytesf=nbytesf+SIZE(a)
 END SUBROUTINE mallocl02
 !
 SUBROUTINE mallocf03(a,nb1,ne1,nb2,ne2,nb3,ne3)
   USE Memory
   USE IntrType
   USE Param
   REAL(NBFT),POINTER :: a(:,:,:)
   ALLOCATE(a(nb1:ne1,nb2:ne2,nb3:ne3))
   a=zero
   nbytesf=nbytesf+SIZE(a)*rvp
 END SUBROUTINE mallocf03
 !
 SUBROUTINE mallocd03(a,nb1,ne1,nb2,ne2,nb3,ne3)
   USE Memory
   USE IntrType
   USE Param
   REAL(NBDT),POINTER :: a(:,:,:)
   ALLOCATE(a(nb1:ne1,nb2:ne2,nb3:ne3))
   a=zero
   nbytesf=nbytesf+SIZE(a)*NBDT
 END SUBROUTINE mallocd03
 !
 SUBROUTINE malloci03(a,nb1,ne1,nb2,ne2,nb3,ne3)
   USE Memory
   USE IntrType
   USE Param
   INTEGER(NBIT),POINTER :: a(:,:,:)
   ALLOCATE(a(nb1:ne1,nb2:ne2,nb3:ne3))
   a=0
   nbytesf=nbytesf+SIZE(a)*NBIT
 END SUBROUTINE malloci03
 !
 SUBROUTINE mallocf04(a,nb1,ne1,nb2,ne2,nb3,ne3,nb4,ne4)
   USE Memory
   USE IntrType
   USE Param
   REAL(NBFT),POINTER :: a(:,:,:,:)
   ALLOCATE(a(nb1:ne1,nb2:ne2,nb3:ne3,nb4:ne4))
   a=zero
   nbytesf=nbytesf+SIZE(a)*rvp
 END SUBROUTINE mallocf04
 !
 SUBROUTINE mallocd04(a,nb1,ne1,nb2,ne2,nb3,ne3,nb4,ne4)
   USE Memory
   USE IntrType
   USE Param
   REAL(NBDT),POINTER :: a(:,:,:,:)
   ALLOCATE(a(nb1:ne1,nb2:ne2,nb3:ne3,nb4:ne4))
   a=zero
   nbytesf=nbytesf+SIZE(a)*NBDT
 END SUBROUTINE mallocd04
 !
 SUBROUTINE malloci04(a,nb1,ne1,nb2,ne2,nb3,ne3,nb4,ne4)
   USE Memory
   USE IntrType
   USE Param
   INTEGER(NBIT),POINTER :: a(:,:,:,:)
   ALLOCATE(a(nb1:ne1,nb2:ne2,nb3:ne3,nb4:ne4))
   a=0
   nbytesf=nbytesf+SIZE(a)*NBIT
 END SUBROUTINE malloci04
 
END MODULE Allocs

