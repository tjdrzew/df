MODULE Table

    USE IntrType

    IMPLICIT NONE

    INTEGER(sik) :: NumOfTables
    INTEGER(sik) :: TableDim
    INTEGER(sik), POINTER, DIMENSION(:,:) :: TableBnds
    REAL(sdk),POINTER,DIMENSION(:)        :: IndVar
    REAL(sdk),POINTER,DIMENSION(:)        :: DepVar

    CONTAINS

    FUNCTION TableLookup(TableNum,x)
        IMPLICIT NONE
       !Arguments
        INTEGER(sik),INTENT(in) :: TableNum
        REAL(sdk),INTENT(in)    :: x
       !Local Variables
        REAL(sdk)    :: TableLookup
        REAL(sdk)    :: x1
        REAL(sdk)    :: x2
        REAL(sdk)    :: y1
        REAL(sdk)    :: y2
        INTEGER(sik) :: i
        INTEGER(sik) :: in
        INTEGER(sik) :: imin
        INTEGER(sik) :: imax
        LOGICAL      :: search 

        imin=TableBnds(TableNum,1)
        imax=TableBnds(TableNum,2)
        i=imin
        in=i+1_sik
        search=.TRUE.

        DO WHILE(search)
            IF ((x>=IndVar(i)).AND.(x<IndVar(in))) THEN
                x1=IndVar(i)
                x2=IndVar(in)
                y1=DepVar(i)
                y2=DepVar(in)
                TableLookup=y1 + (x-x1)*(y2-y1)/(x2-x1)
                search=.FALSE.
            ELSEIF (in>imax) THEN
                y2=DepVar(imax)
                TableLookup=y2
                search=.FALSE.
            ELSEIF (x<IndVar(imin)) THEN
                y1=DepVar(imin)
                TableLookup=y1
                search=.FALSE.
            ELSE
                i=in
                in=i+1_sik
            END IF     
        END DO

        RETURN
    END FUNCTION TableLookup

END MODULE Table