SUBROUTINE DCompLine(aline, types, entries, commentl, numdata)

      USE IntrType
      USE Param

      IMPLICIT NONE
!
! THE PURPOSE OF THIS ROUTINE IS to decompose a line (aline) read from 
! the input file. The number of data, data entries and entry types are  
! specified.
!
! Programmed by ;
!   NAME         : Volkan Seker
!   ORGANIZATION : University of Michigan  
!   DATE         : 03/01/2010
! Modified:
!   NAME         : Tim Drzewiecki
!   ORGANIZATION : University of Michigan  
!   DATE         : 08/13/2010  
!
! Subroutine Argument Descriptions:
!   aline    (IN)  : line to be decomposed
!   types    (OUT) : array to assign the data types
!   entries  (OUT) : array to assign the data
!   commentl (OUT) : comment part in the line
!   numdata  (OUT) : number of data in the line
!
! Variable Declarations;
!   Subroutine Arguments:
      CHARACTER(LEN=1024), INTENT(IN) :: aline           ! line to be decomposed
      CHARACTER(LEN=3),    INTENT(OUT):: types(512)      ! array to assign the data types
      CHARACTER(LEN=100),  INTENT(OUT):: entries(512)    ! array to assign the data
      CHARACTER(LEN=1024), INTENT(OUT):: commentl        ! comment part in the line
      INTEGER(sik),        INTENT(OUT):: numdata         ! number of data in the line
!   Local Variables:
      CHARACTER(LEN=100)              :: datam           ! temporary read data
      CHARACTER(LEN=3)                :: typem           ! temporary data type
      INTEGER(sik)                    :: i               ! loop index
      INTEGER(sik)                    :: j               ! loop index
      INTEGER(sik)                    :: multidcol       ! column where multiple data starts (e.g. 3*10)
      INTEGER(sik)                    :: nmult           ! number of multiple data
      INTEGER(sik)                    :: ns              ! starting colunmn for the data
      INTEGER(sik)                    :: ne              ! ending colunmn for the data
      LOGICAL                         :: nonblankd       ! flag if comments entered on the line
      LOGICAL                         :: nonblank        ! flag for non blank entry
      LOGICAL                         :: multidata       ! flag for mutliple data
      LOGICAL                         :: datas           ! flag for mutliple data
      LOGICAL                         :: datae           ! flag for mutliple data
      LOGICAL                         :: finread         ! flag to finish reading the line
!
!
!  Initialization
!
      nonblankd = .FALSE.
      multidata = .FALSE.
      nonblank  = .TRUE.
      datas     = .FALSE.
      datae     = .FALSE.
      finread   = .FALSE.
      numdata = 0 
      ns = 1_sik
      ne = 1_sik
      types = BLANK
      entries = BLANK
      commentl = BLANK

!
!loop over each column in the line
!
      LREAD: DO i = 1, 1024
         IF(aline(i:i) == BLANK   .OR. &      
              ICHAR(aline(i:i)) == 9  .OR. &        !ichar(tab)=9               
              ICHAR(aline(i:i)) == 10 .OR. &        !ichar(LF)=10               
              ICHAR(aline(i:i)) == 13) THEN         !ichar(CR)=13
            nonblank = .FALSE.
         ELSE
            nonblank = .TRUE.
!
! multiple data entry check
!
            IF(aline(i:i) == AST) THEN
               multidata = .TRUE.
               multidcol = i
               READ(aline(ns:multidcol-1),*) nmult
               nonblank  = .FALSE.
               IF(datas) datas = .FALSE. 
            ENDIF
!
! comment line check
!
            IF(aline(i:i) == BANG ) THEN
               READ(aline(i:1024),*) commentl
               IF(datas .AND. .NOT.datae) THEN
                  datae = .TRUE.
                  ne = i - 1
               END IF
               finread = .TRUE.
            END IF
         ENDIF
!
! starting column of a data
!
         IF(nonblank .AND. .NOT.datas) THEN 
            datas = .TRUE.
            ns = i
         END IF
!
! ending column of a data
!
         IF(datas .AND. .NOT.nonblank) THEN 
            datae = .TRUE.
            ne = i
         END IF
     
!
! countn the number of data, specify the data type and assign each data to entries array
!
         IF(datas .AND. datae) THEN
            READ(aline(ns:ne),*) datam
            IF((ICHAR(datam(1:1)) >= 48 .AND. ICHAR(datam(1:1)) <= 57) .OR. &
                 & ((ICHAR(datam(1:1)) == 43 .OR. ICHAR(datam(1:1)) == 45) .AND. & 
                 & (ICHAR(datam(2:2)) >= 48 .AND. ICHAR(datam(2:2)) <= 57)) ) THEN
               typem = 'NUM'
            ELSE
               typem = 'CHR'
            END IF
            IF(multidata) THEN
               DO j = 1, nmult
                  numdata = numdata + 1
                  entries(numdata) = datam
                  types(numdata)   = typem
               END DO
               multidata = .FALSE.
            ELSE
               numdata = numdata + 1
               entries(numdata) = datam 
               types(numdata)   = typem
            END IF
            datas = .FALSE.
            datae = .FALSE.
         END IF
!
! end of data read due to comment line 
!
         IF(finread) EXIT LREAD
      END DO LREAD
      
END SUBROUTINE DCompLine
