MODULE IntrType
!
      IMPLICIT NONE
!
!     THIS MODULE STORES THE COMMOM TYPE DECLERATION PARAMETERS (Selected kind)
!
! VARIABLE NAME         DESCRIPTION
! -------------         --------------------------------------------------------!
! sdk              :    real variable selected kind
! sik              :    integer variable selected kind
!
! ROUTINES      SUBROUTINE(S)/FUNCTION(F)  SHORT DESCRIPTION        
! ------------  -------------------------  -------------------------------------!
!    N/A                    N/A                   N/A
!
      INTEGER, PARAMETER :: sdk = SELECTED_REAL_KIND(13, 307)
      INTEGER, PARAMETER :: sik = KIND(10000000)
!
END MODULE IntrType
