MODULE PriVars

    USE IntrType

    INTEGER(sik) :: vols
    INTEGER(sik) :: juncs
!    INTEGER(sik) :: vjuncs
    INTEGER(sik) :: svols
    INTEGER(sik) :: nzAum
!    INTEGER(sik) :: nzAvm
    INTEGER(sik) :: nzApm
    INTEGER(sik) :: nzAhm
    INTEGER(sik) :: nzAalpha

   !VOLUME
    REAL(sdk), POINTER, DIMENSION(:)      :: Volume
    REAL(sdk), POINTER, DIMENSION(:)      :: VArea
    REAL(sdk), POINTER, DIMENSION(:)      :: Length
    REAL(sdk), POINTER, DIMENSION(:)      :: Height
    REAL(sdk), POINTER, DIMENSION(:)      :: HydDia
    REAL(sdk), POINTER, DIMENSION(:)      :: Roughness
    REAL(sdk), POINTER, DIMENSION(:)      :: qual
    REAL(sdk), POINTER, DIMENSION(:)      :: alpha
    REAL(sdk), POINTER, DIMENSION(:)      :: Pm
    REAL(sdk), POINTER, DIMENSION(:)      :: Pmc
    REAL(sdk), POINTER, DIMENSION(:)      :: hm
    REAL(sdk), POINTER, DIMENSION(:)      :: hmg
    REAL(sdk), POINTER, DIMENSION(:)      :: hm0
    REAL(sdk), POINTER, DIMENSION(:)      :: hf
    REAL(sdk), POINTER, DIMENSION(:)      :: hg
    REAL(sdk), POINTER, DIMENSION(:)      :: Tm
    REAL(sdk), POINTER, DIMENSION(:)      :: rhogVol
    REAL(sdk), POINTER, DIMENSION(:)      :: rhofVol
    REAL(sdk), POINTER, DIMENSION(:)      :: rhomVol
    REAL(sdk), POINTER, DIMENSION(:)      :: rhomVol0
    REAL(sdk), POINTER, DIMENSION(:)      :: viscgVol
    REAL(sdk), POINTER, DIMENSION(:)      :: viscfVol
    REAL(sdk), POINTER, DIMENSION(:)      :: viscmVol
    REAL(sdk), POINTER, DIMENSION(:)      :: VdjVol
    REAL(sdk), POINTER, DIMENSION(:)      :: Reyn
    REAL(sdk), POINTER, DIMENSION(:)      :: FDarcy
    REAL(sdk), POINTER, DIMENSION(:)      :: W
    REAL(sdk), POINTER, DIMENSION(:)      :: ThCond

   !JUNCTION
    REAL(sdk), POINTER, DIMENSION(:)      :: JArea
    REAL(sdk), POINTER, DIMENSION(:)      :: JLength
    REAL(sdk), POINTER, DIMENSION(:)      :: JHeight
    REAL(sdk), POINTER, DIMENSION(:)      :: JHydDia
    REAL(sdk), POINTER, DIMENSION(:)      :: KFLoss
    REAL(sdk), POINTER, DIMENSION(:)      :: KRLoss
    CHARACTER, POINTER, DIMENSION(:)      :: JForm
    INTEGER(sik), POINTER, DIMENSION(:)   :: JuncClass
    REAL(sdk), POINTER, DIMENSION(:)      :: rhoJun
    REAL(sdk), POINTER, DIMENSION(:)      :: rhoJun0
    REAL(sdk), POINTER, DIMENSION(:)      :: viscJun
    REAL(sdk), POINTER, DIMENSION(:)      :: VdjJun
    REAL(sdk), POINTER, DIMENSION(:)      :: JReyn
    REAL(sdk), POINTER, DIMENSION(:)      :: um
    REAL(sdk), POINTER, DIMENSION(:)      :: umg
    REAL(sdk), POINTER, DIMENSION(:)      :: umc
    REAL(sdk), POINTER, DIMENSION(:)      :: um0
    REAL(sdk), POINTER, DIMENSION(:)      :: FDarcyJ
    REAL(sdk), POINTER, DIMENSION(:)      :: WJ
    REAL(sdk), POINTER, DIMENSION(:)      :: ThCondJ

   !GAP PARAMETERS
!    REAL(sdk), POINTER, DIMENSION(:)      :: WGap
!    REAL(sdk), POINTER, DIMENSION(:)      :: LGap
!    REAL(sdk), POINTER, DIMENSION(:)      :: gReyn
!    REAL(sdk), POINTER, DIMENSION(:)      :: rhoGap
!    REAL(sdk), POINTER, DIMENSION(:)      :: viscGap
!    REAL(sdk), POINTER, DIMENSION(:)      :: SCv
!    REAL(sdk), POINTER, DIMENSION(:)      :: SCvg
!    REAL(sdk), POINTER, DIMENSION(:)      :: SCvc
!    REAL(sdk), POINTER, DIMENSION(:)      :: SCWG
!    REAL(sdk), POINTER, DIMENSION(:)      :: SCKGap
!    REAL(sdk), POINTER, DIMENSION(:)      :: SCKMult
!    CHARACTER(len=2),POINTER,DIMENSION(:) :: SCKModel

   !VOLUME COEFFICIENT MATRICES AND SOURCE VECTORS
    REAL(sdk), POINTER, DIMENSION(:)    :: Aalpha
    INTEGER(sik), POINTER, DIMENSION(:) :: iAalpha
    INTEGER(sik), POINTER, DIMENSION(:) :: jAalpha
    REAL(sdk), POINTER, DIMENSION(:)    :: balpha
    REAL(sdk), POINTER, DIMENSION(:)    :: Apm
    INTEGER(sik), POINTER, DIMENSION(:) :: iApm
    INTEGER(sik), POINTER, DIMENSION(:) :: jApm
    REAL(sdk), POINTER, DIMENSION(:)    :: bpmc
    REAL(sdk), POINTER, DIMENSION(:)    :: Ahm
    INTEGER(sik), POINTER, DIMENSION(:) :: iAhm
    INTEGER(sik), POINTER, DIMENSION(:) :: jAhm
    REAL(sdk), POINTER, DIMENSION(:)    :: bhm

   !JUNCTION COEFFICIENT MATRICES AND SOURCE VECTORS
    REAL(sdk), POINTER, DIMENSION(:) :: Aum
    INTEGER(sik), POINTER, DIMENSION(:) :: iAum
    INTEGER(sik), POINTER, DIMENSION(:) :: jAum
    REAL(sdk), POINTER, DIMENSION(:) :: bum
    REAL(sdk), POINTER, DIMENSION(:) :: dum

   !GAP COEFFICIENT MATRICES AND SOURCE VECTORS
!    REAL(sdk), POINTER, DIMENSION(:) :: SCAv
!    INTEGER(sik), POINTER, DIMENSION(:) :: SCiAv
!    INTEGER(sik), POINTER, DIMENSION(:) :: SCjAv
!    REAL(sdk), POINTER, DIMENSION(:) :: SCdv
!    REAL(sdk), POINTER, DIMENSION(:) :: SCbv

   !INDEXING INFORMATION
    INTEGER(sik), POINTER, DIMENSION(:,:) :: jci
    INTEGER(sik), POINTER, DIMENSION(:)   :: vciNF
    INTEGER(sik), POINTER, DIMENSION(:,:) :: vciF
    INTEGER(sik), POINTER, DIMENSION(:)   :: vciNT
    INTEGER(sik), POINTER, DIMENSION(:,:) :: vciT
    INTEGER(sik), POINTER, DIMENSION(:)   :: s2fci
!    INTEGER(sik), POINTER, DIMENSION(:,:) :: gci
!    INTEGER(sik), POINTER, DIMENSION(:)   :: gvciNF
!    INTEGER(sik), POINTER, DIMENSION(:,:) :: gvciF
!    INTEGER(sik), POINTER, DIMENSION(:)   :: gvciNT
!    INTEGER(sik), POINTER, DIMENSION(:,:) :: gvciT

END MODULE PriVars
