SUBROUTINE toupper(aa)
  ! convert lowercase string to uppercase
  USE IntrType
  IMPLICIT NONE
  
  INTEGER(sik), PARAMETER :: INDXA=97
  INTEGER(sik), PARAMETER :: IDNXZ=122
  CHARACTER                :: aa*(*)
  INTEGER(sik)             :: lenaa,i,ia
  
  lenaa=LEN_TRIM(aa)
  i=1
  DO WHILE (aa(i:i).NE.' ' .AND. i.LE.lenaa)
     ia=ICHAR(aa(i:i))
     IF(ia.GE.INDXA) aa(i:i)=CHAR(ia-32)
     i=i+1
     IF(i.GT.lenaa) EXIT
  ENDDO
  RETURN
END SUBROUTINE toupper
