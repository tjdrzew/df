MODULE ConstitutiveRel

    USE IntrType
    USE PriVars
    USE SteamProp, ONLY: x12,TR1,TR2,Tsat,rhoR1,rhoR2,mu
    USE Param, ONLY: small

    IMPLICIT NONE

CONTAINS

!*******************************************************************
    SUBROUTINE CalcQuality
        
        IMPLICIT NONE

        INTEGER(sik) :: i
        
        DO i=1,vols
            qual(i)=x12(hm(i),pm(i))
        END DO

    END SUBROUTINE CalcQuality
!*******************************************************************
    SUBROUTINE CalcTemperature
        
        IMPLICIT NONE

        INTEGER(sik) :: i
        
        DO i=1,vols
            IF (qual(i) < 0.0_sdk) THEN
                Tm(i)=TR1(pm(i),hm(i))
            ELSEIF (qual(i) > 1.0_sdk) THEN
                Tm(i)=TR2(pm(i),hm(i))
            ELSE
                Tm(i)=Tsat(pm(i))
            END IF
        END DO

    END SUBROUTINE CalcTemperature
!*******************************************************************
    SUBROUTINE CalcDensity

        IMPLICIT NONE

        INTEGER(sik) :: i
        INTEGER(sik) :: vfi
        INTEGER(sik) :: vti
        REAL(sdk)    :: x
        REAL(sdk)    :: p
        REAL(sdk)    :: T
        REAL(sdk)    :: vf
        REAL(sdk)    :: Ts
        REAL(sdk)    :: denf
        REAL(sdk)    :: deng


        DO i=1,vols
            x=qual(i)
            p=pm(i)
            T=Tm(i)
            vf=alpha(i)
            Ts=Tsat(p)
            IF (x<=0.0) THEN
                denf=rhoR1(p,T)
                deng=rhoR2(p,Ts)
            ELSE IF (x>=1.0_sdk) THEN
                denf=rhoR1(p,Ts)
                deng=rhoR2(p,T)
            ELSE
                denf=rhoR1(p,Ts)
                deng=rhoR2(p,Ts)            
            END IF
            rhofVol(i)=denf
            rhogVol(i)=deng
            rhomVol(i)=vf*deng+(1.0_sdk-vf)*denf
        END DO
        DO i=1,juncs
            vfi=jci(i,1)
            vti=jci(i,2)
            rhoJun(i)=(rhomVol(vfi)*Length(vfi)+rhomVol(vti)*Length(vti))/(Length(vfi)+Length(vti))
        END DO

    END SUBROUTINE CalcDensity
!*******************************************************************
    SUBROUTINE CalcVF


        INTEGER(sik) :: i
        INTEGER(sik) :: vfi
        INTEGER(sik) :: vti
        REAL(sdk)    :: x
        REAL(sdk)    :: denf
        REAL(sdk)    :: deng

        DO i=1,vols
            x=qual(i)
            x=MIN(MAX(x,0.0_sdk),1.0_sdk)
            denf=rhofVol(i)
            deng=rhogVol(i)
            alpha(i)=(x*denf)/(x*denf+(1-x)*deng)       
        END DO

    END SUBROUTINE CalcVF
!*******************************************************************
    SUBROUTINE CalcViscosity
 
        IMPLICIT NONE
 
        INTEGER(sik) :: i
        INTEGER(sik) :: vfi
        INTEGER(sik) :: vti
        REAL(sdk)    :: x
        REAL(sdk)    :: p
        REAL(sdk)    :: T
        REAL(sdk)    :: Ts
        REAL(sdk)    :: denf
        REAL(sdk)    :: deng
        REAL(sdk)    :: vf
        REAL(sdk)    :: muf
        REAL(sdk)    :: mug

        DO i=1,vols
            x=qual(i)
            p=pm(i)
            T=Tm(i)
            Ts=Tsat(p)
            denf=rhofVol(i)
            deng=rhogVol(i)
            vf=alpha(i)
            IF (x<=0.0) THEN
                muf=mu(T,denf)
                mug=mu(Ts,deng)
            ELSE IF (x>=1.0_sdk) THEN
                muf=mu(Ts,denf)
                mug=mu(T,deng)
            ELSE
                muf=mu(Ts,denf)
                mug=mu(Ts,deng)            
            END IF
            viscfVol(i)=muf
            viscgVol(i)=mug
            viscmVol(i)=vf*mug + (1.0_sdk-vf)*muf
        END DO

        DO i=1,juncs
            vfi=jci(i,1)
            vti=jci(i,2)
            viscJun(i)=(viscmVol(vfi)*Length(vfi)+viscmVol(vti)*Length(vti))/(Length(vfi)+Length(vti))
        END DO
 
    END SUBROUTINE CalcViscosity
!*******************************************************************
    SUBROUTINE SetupW

        IMPLICIT NONE

        INTEGER(sik) :: i
        INTEGER(sik) :: j
        INTEGER(sik) :: nij
        INTEGER(sik) :: noj
        INTEGER(sik) :: iji
        INTEGER(sik) :: oji
        REAL(sdk)    :: Win
        REAL(sdk)    :: Wout

        DO i=1,juncs
            WJ(i)=rhoJun(i)*um(i)*JArea(i)
        END DO

        DO i=1,vols
            IF (Volume(i)==0.0_sdk) THEN
                Win=0.0_sdk
                Wout=0.0_sdk
                nij=vciNT(i)
                noj=vciNF(i)

                DO j=1,nij
                    iji=vciT(i,j)
                    Win=Win+WJ(iji)
                END DO

                DO j=1,noj
                    oji=vciF(i,j)
                    Wout=Wout+WJ(oji)
                END DO

                IF (nij==0_sik) THEN
                    W(i)=Wout
                ELSEIF (noj==0_sik) THEN
                    W(i)=Win
                ELSE
                    W(i)=0.5_sdk*(Win+Wout)
                END IF

            ELSE
                Win=0.0_sdk
                Wout=0.0_sdk
                nij=vciNT(i)
                noj=vciNF(i)

                DO j=1,nij
                    iji=vciT(i,j)
                    Win=Win+WJ(iji)
                END DO

                DO j=1,noj
                    oji=vciF(i,j)
                    Wout=Wout+WJ(oji)
                END DO

                W(i)=0.5_sdk*(Win+Wout)

            END IF
        END DO

    END SUBROUTINE SetupW
!*******************************************************************
    SUBROUTINE CalcReynolds

        INTEGER(sik) :: i

        DO i=1,vols
            IF (Volume(i)==0.0_sdk) THEN
                Reyn(i)=small
            ELSE
                Reyn(i)=MAX(ABS(W(i))*HydDia(i)/(VArea(i)*viscmVol(i)),small)
            END IF
        END DO

        DO i=1,juncs
            JReyn(i)=MAX(rhoJun(i)*ABS(um(i))*JHydDia(i)/viscJun(i),small)
        END DO

    END SUBROUTINE CalcReynolds
!*******************************************************************
    SUBROUTINE CalcDarcyF

        IMPLICIT NONE

        INTEGER(sik) :: i
        INTEGER(sik) :: vfi
        INTEGER(sik) :: vti
        REAL(sdk)    :: temp
        REAL(sdk)    :: alfa
        REAL(sdk)    :: beta

        DO i=1,vols
            temp=(7.0_sdk/Reyn(i))**0.9_sdk + 0.27_sdk*Roughness(i)
            alfa=(2.457_sdk*log(1/temp))**16.0_sdk
            beta=(37530._sdk/Reyn(i))**16.0_sdk
            FDarcy(i)=8.0_sdk*(((8.0_sdk/Reyn(i))**12.0_sdk+(alfa+beta)**(-1.5_sdk))**(0.08333333333333_sdk))
        END DO

        DO i=1,juncs
            vfi=jci(i,1)
            vti=jci(i,2)
            IF (Volume(vfi)==0.0_sdk) THEN
                FDarcyJ(i)=FDarcy(vti)
            ELSEIF (Volume(vti)==0.0_sdk) THEN
                FDarcyJ(i)=FDarcy(vfi)
            ELSE
                FDarcyJ(i)=(FDarcy(vfi)*Length(vfi)+FDarcy(vti)*Length(vti))/(Length(vfi)+Length(vti))
            END IF
        END DO

    END SUBROUTINE CalcDarcyF
!*******************************************************************
    SUBROUTINE CalcThCond

        IMPLICIT NONE

        INTEGER(sik) :: i
        INTEGER(sik) :: vfi
        INTEGER(sik) :: vti

        DO i=1,vols
            ThCond(i)=1.0_sdk
        END DO

        DO i=1,juncs
            vfi=jci(i,1)
            vti=jci(i,2)
            ThCondJ(i)=(ThCond(vfi)*Length(vfi)+ThCond(vti)*Length(vti))/(Length(vfi)+Length(vti))
        END DO

    END SUBROUTINE CalcThCond
!*******************************************************************
    SUBROUTINE CalcVdj

        IMPLICIT NONE

        INTEGER(sik) :: i
        INTEGER(sik) :: vfi
        INTEGER(sik) :: vti

        DO i=1,vols
            VdjVol(i)=0.0_sdk
        END DO

        DO i=1,juncs
            vfi=jci(i,1)
            vti=jci(i,2)
            VdjJun(i)=(VdjVol(vfi)*Length(vfi)+VdjVol(vti)*Length(vti))/(Length(vfi)+Length(vti))
        END DO

    END SUBROUTINE CalcVdj
!*******************************************************************

END MODULE ConstitutiveRel
