SUBROUTINE WriteOutput
  
    USE IntrType
    USE BlockStr
    USE PriVars
    USE File
    USE Solver, ONLY: time

  IMPLICIT NONE
  
! THE PURPOSE OF THIS ROUTINE IS to calculate the residual for continuity.
!
! Programmed by ;
!   NAME         : Tim Drzewiecki 
!   ORGANIZATION : University of Michigan  
!   DATE         : 05/10/2012
!
! Subroutine Argument Descriptions:
! 
! Variable Declarations;
!   Subroutine Arguments:
!   Local Variables:
    INTEGER(sik) :: i
    INTEGER(sik) :: j
    INTEGER(sik) :: vidx
    INTEGER(sik) :: jidx

    OPEN(OutFile,file=OutFileName, status='old', access='append')  
  
    WRITE(OutFile,'(a100)') '*************************************** Fluid Block Output **************************************'
    DO i=1,numFB
        WRITE(OutFile,'(a10,a12)') 'ID','time, sec'
        WRITE(OutFile,'(a10,ES12.4)') trim(FB(i)%id), time        
        WRITE(OutFile,'(a4,8a12)') 'Cell','Pm,Pa','hm','quality','VF','rhof','rhog','rhom','Reyn'
        DO j=1,FB(i)%cells
            vidx=FB(i)%vidx(j)
            WRITE(OutFile,'(i4,8ES12.4)') j,Pm(vidx),hm(vidx),qual(vidx),alpha(vidx),rhofVol(vidx),rhogVol(vidx),rhomVol(vidx),Reyn(vidx)
        END DO   
        WRITE(OutFile,'(a4,2a12)') 'Junc','FDarcy','um,m/s'
        DO j=1,(FB(i)%cells-1)
            jidx=FB(i)%jidx(j)
            WRITE(OutFile,'(i4,2ES12.4)') j, FDarcyJ(jidx),um(jidx)  
        END DO
!        j=FB(i)%cells
!        vidx=FB(i)%vidx(j)
!        WRITE(OutFile,'(i4,ES14.6,6ES12.4)') j,Pm(vidx),hm(vidx),rhomVol(vidx),viscmVol(vidx),W(vidx),Reyn(vidx),FDarcy(vidx) 
        WRITE(OutFile,'(a1)') ' '    
    END DO

    WRITE(OutFile,'(a100)') '************************************* Boundary Block Output *************************************'
!    DO i=1,numBB
!        vidx=BB(i)%vidx
!        WRITE(outdev,'(a10,3a12)') 'ID','time, sec','P,Pa','T,K'
!        WRITE(outdev,'(a10,ES12.4,ES14.6,ES12.4)') trim(BB(i)%id),time,SCP(vidx),SCT(vidx)
!        WRITE(outdev,'(a1)') ' ' 
!    END DO

    WRITE(OutFile,'(a100)') '************************************* Junction Block Output *************************************'
!    DO i=1,numJB
!        jidx=JB(i)%jidx
!        WRITE(*,'(a10,3a12)') 'ID','time','W,kg/s','u,m/s'
!        WRITE(*,'(a10,3ES12.4)') trim(JB(i)%id),time,WJ(jidx),um(jidx)
!        WRITE(*,'(a1)') ' ' 
!    END DO

    CLOSE(OutFile)
      
END SUBROUTINE WriteOutput
