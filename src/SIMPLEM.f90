MODULE SIMPLE

      USE IntrType
      USE PriVars
      USE BlockStr
      USE Solver
      USE Table
      USE Param, ONLY: small
      USE ConstitutiveRel


      IMPLICIT NONE

CONTAINS
!****************************************************************************
    SUBROUTINE setupAum

        IMPLICIT NONE

! THE PURPOSE OF THIS ROUTINE IS to setup the axial momentum coefficient
!   matrix
!
! Programmed by ;
!   NAME         : Tim Drzewiecki
!   ORGANIZATION : University of Michigan
!   DATE         : 05/10/2012
!
! Subroutine Argument Descriptions:
!
!
! Variable Declarations;
!   Subroutine Arguments:
!   Local Variables:

        INTEGER(sik) :: i
        INTEGER(sik) :: j
        INTEGER(sik) :: k
        INTEGER(sik) :: ivi
        INTEGER(sik) :: ovi
        INTEGER(sik) :: iji
        INTEGER(sik) :: oji
        INTEGER(sik) :: nij
        INTEGER(sik) :: noj
        REAL(sdk) :: Aoj
        REAL(sdk) :: Aij
        REAL(sdk) :: Auc
        REAL(sdk) :: Fiv
        REAL(sdk) :: Fov
        REAL(sdk) :: uu
        REAL(sdk) :: Loss
        REAL(sdk) :: x
        REAL(sdk) :: LamLoss
        REAL(sdk) :: MinLoss
        REAL(sdk) :: MajLoss
        REAL(sdk) :: timeder
        REAL(sdk) :: WinJ
        REAL(sdk) :: WoutJ
        REAL(sdk) :: AreaFrac
        REAL(sdk) :: epsilon
        REAL(sdk) :: AreaEff
        REAL(sdk) :: ExpCont

        k=1_sik
        DO i=1,juncs
            SELECT CASE(JuncClass(i))
            CASE(-1)
                Auc=1.0_sdk
                Aum(k)=Auc
                jAum(k)=i
                k=k+1_sik
                dum(i)=0.0_sdk

            CASE(1)
                ivi  = jci(i,1)
                ovi  = jci(i,2)
                iji  = vciT(ivi,1)
                oji  = vciF(ovi,1)

               !Flux terms
                Fiv=W(ivi)
                Fov=W(ovi)
                Aij=MAX( Fiv,0.0_sdk)
                Aoj=MAX(0.0_sdk,-Fov)

               !Loss term
                uu=ABS(um(i))
                x=MIN(1.0_sdk,JReyn(i)/1000.0_sdk)
                LamLoss=32.0_sdk*viscJun(i)*JLength(i)/(JHydDia(i)*JHydDia(i))
                MajLoss=(1.0_sdk-x)*LamLoss + x*(FDarcyJ(i)*JLength(i)/JHydDia(i))*0.5_sdk*rhoJun(i)*uu
                IF (um(i) >= 0.0_sdk) THEN
                    MinLoss=0.5_sdk*KFLoss(i)*rhoJun(i)*uu
                ELSE
                    MinLoss=0.5_sdk*KRLoss(i)*rhoJun(i)*uu
                END IF
                Loss=(MinLoss+MajLoss)*JArea(i)

               !Time derivative term
                IF (dt==0.0_sdk) THEN
                    timeder=0.0_sdk
                ELSE
                    timeder=rhoJun(i)*JArea(i)*JLength(i)/dt
                END IF

               !Put it all together
                Auc=timeder+Aoj+Aij+Fov-Fiv+Loss
                Aum(k)=-Aij
                jAum(k)=iji
                k=k+1_sik
                Aum(k)=Auc
                jAum(k)=i
                k=k+1_sik
                Aum(k)=-Aoj
                jAum(k)=oji
                k=k+1_sik
                dum(i)=JArea(i)/Auc

            CASE(2)
                ivi = jci(i,1)
                ovi = jci(i,2)
                nij = vciNT(ivi)
                oji = vciF(ovi,1)

               !Flux terms
                Fiv=W(ivi)
                Fov=W(ovi)
                Aoj=MAX(0.0_sdk,-Fov)
                Auc=Aoj
                Aum(k)=-Aoj
                jAum(k)=oji
                k=k+1_sik

                WinJ=0.0_sdk
                DO j=1,nij
                    iji=vciT(ivi,j)
                    WinJ=WinJ+MAX(WJ(iji),0.0_sdk)
                END DO
                WinJ=MAX(WinJ,small)

                DO j=1,nij
                    iji=vciT(ivi,j)
                    AreaFrac=MAX(WJ(iji),0.0_sdk)/WinJ
                    Aij=MAX(AreaFrac*Fiv,0.0_sdk)
                    Auc=Auc+Aij
                    Aum(k)=-Aij
                    jAum(k)=iji
                    k=k+1_sik
                END DO

               !Loss term
                uu=ABS(um(i))
                x=MIN(1.0_sdk,JReyn(i)/1000.0_sdk)
                LamLoss=32.0_sdk*viscJun(i)*JLength(i)/(JHydDia(i)*JHydDia(i))
                MajLoss=(1.0_sdk-x)*LamLoss + x*(FDarcyJ(i)*JLength(i)/JHydDia(i))*0.5_sdk*rhoJun(i)*uu
                IF (um(i) >= 0.0_sdk) THEN
                    MinLoss=0.5_sdk*KFLoss(i)*rhoJun(i)*uu
                ELSE
                    MinLoss=0.5_sdk*KRLoss(i)*rhoJun(i)*uu
                END IF
                Loss=(MinLoss+MajLoss)*JArea(i)

               !Time derivative term
                IF (dt==0.0_sdk) THEN
                    timeder=0.0_sdk
                ELSE
                    timeder=rhoJun(i)*JArea(i)*JLength(i)/dt
                END IF

               !Put it all together
                Auc=timeder+Auc+Fov-Fiv+Loss
                Aum(k)=Auc
                jAum(k)=i
                k=k+1_sik
                dum(i)=JArea(i)/Auc

            CASE(3)
                ivi = jci(i,1)
                ovi = jci(i,2)
                iji = vciT(ivi,1)
                oji = vciF(ovi,1)

               !Flux terms
                Fiv=WJ(i)
                Fov=W(ovi)
                Aij=MAX(Fiv ,0.0_sdk)
                Aoj=MAX(0.0_sdk,-Fov)


               !Loss term
                uu=ABS(um(i))
                x=MIN(1.0_sdk,JReyn(i)/1000.0_sdk)
                LamLoss=32.0_sdk*viscJun(i)*JLength(i)/(JHydDia(i)*JHydDia(i))
                MajLoss=(1.0_sdk-x)*LamLoss + x*(FDarcyJ(i)*JLength(i)/JHydDia(i))*0.5_sdk*rhoJun(i)*uu
                IF (um(i) >= 0.0_sdk) THEN
                    MinLoss=0.5_sdk*KFLoss(i)*rhoJun(i)*uu
                ELSE
                    MinLoss=0.5_sdk*KRLoss(i)*rhoJun(i)*uu
                END IF
                Loss=(MinLoss+MajLoss)*JArea(i)

               !Time derivative term
                IF (dt==0.0_sdk) THEN
                    timeder=0.0_sdk
                ELSE
                    timeder=rhoJun(i)*JArea(i)*JLength(i)/dt
                END IF

               !Put it all together
                Auc=timeder+Aoj+Aij+Fov-Fiv+Loss
                Aum(k)=-Aij
                jAum(k)=iji
                k=k+1_sik
                Aum(k)=Auc
                jAum(k)=i
                k=k+1_sik
                Aum(k)=-Aoj
                jAum(k)=oji
                k=k+1_sik
                dum(i)=JArea(i)/Auc

            CASE(4)
                ivi = jci(i,1)
                ovi = jci(i,2)
                iji = vciT(ivi,1)
                oji = vciF(ovi,1)

               !Flux terms
                Fiv=W(ivi)
                Fov=WJ(i)
                Aij=MAX(Fiv,0.0_sdk)
                Aoj=MAX(0.0_sdk,-Fov)

               !Loss term
                uu=ABS(um(i))
                x=MIN(1.0_sdk,JReyn(i)/1000.0_sdk)
                LamLoss=32.0_sdk*viscJun(i)*JLength(i)/(JHydDia(i)*JHydDia(i))
                MajLoss=(1.0_sdk-x)*LamLoss + x*(FDarcyJ(i)*JLength(i)/JHydDia(i))*0.5_sdk*rhoJun(i)*uu
                IF (um(i) >= 0.0_sdk) THEN
                    MinLoss=0.5_sdk*KFLoss(i)*rhoJun(i)*uu
                ELSE
                    MinLoss=0.5_sdk*KRLoss(i)*rhoJun(i)*uu
                END IF
                Loss=(MinLoss+MajLoss)*JArea(i)

               !Time derivative term
                IF (dt==0.0_sdk) THEN
                    timeder=0.0_sdk
                ELSE
                    timeder=rhoJun(i)*JArea(i)*JLength(i)/dt
                END IF

               !Put it all together
                Auc=timeder+Aoj+Aij+Fov-Fiv+Loss
                Aum(k)=-Aij
                jAum(k)=iji
                k=k+1_sik
                Aum(k)=Auc
                jAum(k)=i
                k=k+1_sik
                Aum(k)=-Aoj
                jAum(k)=oji
                k=k+1_sik
                dum(i)=JArea(i)/Auc

            CASE(5)
                ivi = jci(i,1)
                ovi = jci(i,2)
                iji = vciT(ivi,1)
                noj = vciNF(ovi)

               !Flux terms
                Fiv=W(ivi)
                Fov=W(ovi)
                Aij=MAX(Fiv,0.0_sdk)
                Auc=Aij
                Aum(k)=-Aij
                jAum(k)=iji
                k=k+1_sik

                WoutJ=0.0_sdk
                DO j=1,noj
                    oji=vciF(ovi,j)
                    WoutJ=WoutJ+MAX(-WJ(oji),0.0_sdk)
                END DO
                WoutJ=MAX(WoutJ,small)

                DO j=1,noj
                    oji=vciF(ovi,j)
                    AreaFrac=MAX(-WJ(oji),0.0_sdk)/WoutJ
                    Aoj=MAX(0.0_sdk,-AreaFrac*Fov)
                    Auc=Auc+Aoj
                    Aum(k)=-Aoj
                    jAum(k)=oji
                    k=k+1_sik
                END DO

               !Loss term
                uu=ABS(um(i))
                x=MIN(1.0_sdk,JReyn(i)/1000.0_sdk)
                LamLoss=32.0_sdk*viscJun(i)*JLength(i)/(JHydDia(i)*JHydDia(i))
                MajLoss=(1.0_sdk-x)*LamLoss + x*(FDarcyJ(i)*JLength(i)/JHydDia(i))*0.5_sdk*rhoJun(i)*uu
                IF (um(i) >= 0.0_sdk) THEN
                    MinLoss=0.5_sdk*KFLoss(i)*rhoJun(i)*uu
                ELSE
                    MinLoss=0.5_sdk*KRLoss(i)*rhoJun(i)*uu
                END IF
                Loss=(MinLoss+MajLoss)*JArea(i)

               !Time derivative term
                IF (dt==0.0_sdk) THEN
                    timeder=0.0_sdk
                ELSE
                    timeder=rhoJun(i)*JArea(i)*JLength(i)/dt
                END IF

               !Put it all together
                Auc=timeder+Auc+Fov-Fiv+Loss
                Aum(k)=Auc
                jAum(k)=i
                k=k+1_sik
                dum(i)=JArea(i)/Auc

            CASE(6)
                ivi = jci(i,1)
                ovi = jci(i,2)
                oji = vciF(ovi,1)

               !Flux terms
                Fiv=WJ(i)
                Fov=W(ovi)
                Aij=MAX(Fiv ,0.0_sdk)
                Aoj=MAX(0.0_sdk,-Fov)

               !Loss term
                uu=ABS(um(i))
                x=MIN(1.0_sdk,JReyn(i)/1000.0_sdk)
                LamLoss=32.0_sdk*viscJun(i)*JLength(i)/(JHydDia(i)*JHydDia(i))
                MajLoss=(1.0_sdk-x)*LamLoss + x*(FDarcyJ(i)*JLength(i)/JHydDia(i))*0.5_sdk*rhoJun(i)*uu
                IF (um(i) >= 0.0_sdk) THEN
                    MinLoss=0.5_sdk*KFLoss(i)*rhoJun(i)*uu
                ELSE
                    MinLoss=0.5_sdk*KRLoss(i)*rhoJun(i)*uu
                END IF
                Loss=(MinLoss+MajLoss)*JArea(i)

               !Time derivative term
                IF (dt==0.0_sdk) THEN
                    timeder=0.0_sdk
                ELSE
                    timeder=rhoJun(i)*JArea(i)*JLength(i)/dt
                END IF

               !Put it all together
                Auc=timeder+Aoj+Aij+Fov-Fiv+Loss
                Aum(k)=Auc
                jAum(k)=i
                k=k+1_sik
                Aum(k)=-Aoj
                jAum(k)=oji
                k=k+1_sdk
                dum(i)=JArea(i)/Auc

            CASE(7)
                ivi = jci(i,1)
                ovi = jci(i,2)
                iji = vciT(ivi,1)

               !Flux terms
                Fiv=W(ivi)
                Fov=WJ(i)
                Aij=MAX(Fiv,0.0_sdk)
                Aoj=MAX(0.0_sdk,-Fov)
                
               !Loss term
                uu=ABS(um(i))
                x=MIN(1.0_sdk,JReyn(i)/1000.0_sdk)
                LamLoss=32.0_sdk*viscJun(i)*JLength(i)/(JHydDia(i)*JHydDia(i))
                MajLoss=(1.0_sdk-x)*LamLoss + x*(FDarcyJ(i)*JLength(i)/JHydDia(i))*0.5_sdk*rhoJun(i)*uu
                IF (um(i) >= 0.0_sdk) THEN
                    MinLoss=0.5_sdk*KFLoss(i)*rhoJun(i)*uu
                ELSE
                    MinLoss=0.5_sdk*KRLoss(i)*rhoJun(i)*uu
                END IF
                Loss=(MinLoss+MajLoss)*JArea(i)

               !Time derivative term
                IF (dt==0.0_sdk) THEN
                    timeder=0.0_sdk
                ELSE
                    timeder=rhoJun(i)*JArea(i)*JLength(i)/dt
                END IF

               !Put it all together
                Auc=timeder+Aoj+Aij+Fov-Fiv+Loss
                Aum(k)=-Aij
                jAum(k)=iji
                k=k+1_sdk
                Aum(k)=Auc
                jAum(k)=i
                k=k+1_sik
                dum(i)=JArea(i)/Auc

            CASE(8)
                ivi = jci(i,1)
                ovi = jci(i,2)

               !Loss term
                uu=ABS(um(i))
                x=MIN(1.0_sdk,JReyn(i)/1000.0_sdk)
                LamLoss=32.0_sdk*viscJun(i)*JLength(i)/(JHydDia(i)*JHydDia(i))
                MajLoss=(1.0_sdk-x)*LamLoss + x*(FDarcyJ(i)*JLength(i)/JHydDia(i))*0.5_sdk*rhoJun(i)*uu
                IF (um(i) >= 0.0_sdk) THEN
                    MinLoss=0.5_sdk*KFLoss(i)*rhoJun(i)*uu
                ELSE
                    MinLoss=0.5_sdk*KRLoss(i)*rhoJun(i)*uu
                END IF
                Loss=(MinLoss+MajLoss)*JArea(i)

               !Contraction term
                IF (ABS(W(ivi))>small) THEN
                    AreaEff=MAX(VArea(ovi),ABS(WJ(i)*VArea(ivi)/W(ivi)))   !see RELAP5 Theory Manual (Branch Component)
                    epsilon=VArea(ovi)/AreaEff
                    ExpCont=0.5_sdk*(1.0_sdk-epsilon*epsilon)*WJ(i)
                ELSE
                    ExpCont=0.0_sdk
                END IF

               !Put it all together
                Auc=Loss+ExpCont
                Aum(k)=Auc
                jAum(k)=i
                k=k+1_sik
                dum(i)=JArea(i)/Auc

            CASE(9)
                  ivi = jci(i,1)
                  ovi = jci(i,2)

               !Loss term
                uu=ABS(um(i))
                x=MIN(1.0_sdk,JReyn(i)/1000.0_sdk)
                LamLoss=32.0_sdk*viscJun(i)*JLength(i)/(JHydDia(i)*JHydDia(i))
                MajLoss=(1.0_sdk-x)*LamLoss + x*(FDarcyJ(i)*JLength(i)/JHydDia(i))*0.5_sdk*rhoJun(i)*uu
                IF (um(i) >= 0.0_sdk) THEN
                    MinLoss=0.5_sdk*KFLoss(i)*rhoJun(i)*uu
                ELSE
                    MinLoss=0.5_sdk*KRLoss(i)*rhoJun(i)*uu
                END IF
                Loss=(MinLoss+MajLoss)*JArea(i)

               !Expansion term
                IF (ABS(W(ovi))>small) THEN
                    AreaEff=MAX(VArea(ivi),ABS(WJ(i)*VArea(ovi)/W(ovi)))   !see RELAP5 Theory Manual (Branch Component)
                    epsilon=VArea(ivi)/AreaEff
                    ExpCont=-0.5_sdk*(1.0_sdk-epsilon*epsilon)*WJ(i)
                ELSE
                    ExpCont=0.0_sdk
                END IF

                Auc = Loss + ExpCont
                Aum(k) = Auc
                jAum(k) = i
                k=k+1_sik
                dum(i)=JArea(i)/Auc

            END SELECT
        END DO

    END SUBROUTINE setupAum
!****************************************************************************
    SUBROUTINE setupApm

        IMPLICIT NONE

! THE PURPOSE OF THIS ROUTINE IS to setup the axial momentum coefficient
!   matrix
!
! Programmed by ;
!   NAME         : Tim Drzewiecki
!   ORGANIZATION : University of Michigan
!   DATE         : 05/10/2012
!
! Subroutine Argument Descriptions:
!
!
! Variable Declarations;
!   Subroutine Arguments:
!   Local Variables:
        INTEGER(sik) :: i
        INTEGER(sik) :: j
        INTEGER(sik) :: k
        INTEGER(sik) :: nij
        INTEGER(sik) :: noj
        INTEGER(sik) :: iji
        INTEGER(sik) :: oji
        INTEGER(sik) :: ivi
        INTEGER(sik) :: ovi
        INTEGER(sik) :: nig
        INTEGER(sik) :: nog
        INTEGER(sik) :: igi
        INTEGER(sik) :: ogi
        REAL(sdk)    :: Aiv
        REAL(sdk)    :: Aov
        REAL(sdk)    :: Avc

        k=1_sik
        DO i=1,vols
            IF (Volume(i)>0.0_sdk) THEN
                nij=vciNT(i) !number of inlet junctions
                noj=vciNF(i) !number of outlet junctions
                Avc=0.0_sdk

                DO j=1,nij
                    iji=vciT(i,j)
                    IF (JuncClass(iji)>0_sik) THEN
                        ivi=jci(iji,1)
                        Aiv=rhoJun(iji)*JArea(iji)*dum(iji)
                        Avc=Avc+Aiv
                        Apm(k)=-Aiv
                        jApm(k)=ivi
                        k=k+1_sik
                    END IF
                END DO

                DO j=1,noj
                    oji=vciF(i,j)
                    IF (JuncClass(oji)>0_sik) THEN
                        ovi=jci(oji,2)
                        Aov=rhoJun(oji)*JArea(oji)*dum(oji)
                        Avc=Avc+Aov
                        Apm(k)=-Aov
                        jApm(k)=ovi
                        k=k+1_sik
                    END IF
                END DO

                Apm(k)=Avc
                jApm(k)=i
                k=k+1_sik
            ELSE
                Apm(k)=1.0_sdk
                jApm(k)=i
                k=k+1_sik
            END IF
        END DO
    END SUBROUTINE setupApm
!****************************************************************************
    SUBROUTINE setupbum

        IMPLICIT NONE

! THE PURPOSE OF THIS ROUTINE IS to setup the momentum source vector
!
! Programmed by ;
!   NAME         : Tim Drzewiecki
!   ORGANIZATION : University of Michigan
!   DATE         : 05/10/2012
!
! Subroutine Argument Descriptions:
!
!
! Variable Declarations;
!   Subroutine Arguments:
!   Local Variables:
!
        INTEGER(sik) :: i
        INTEGER(sik) :: jidx
        INTEGER(sik) :: TBLidx
        INTEGER(sik) :: ivi
        INTEGER(sik) :: ovi
        REAL(sdk)    :: UWsp
        REAL(sdk)    :: Fbody
        REAL(sdk)    :: Fdt
        REAL(sdk)    :: Fdp
        REAL(sdk)    :: Fdf

        DO i=1,numJB
            IF (JB(i)%bFlag=='U') THEN
                jidx=JB(i)%jidx
                TBLidx=JB(i)%uwff
                UWsp=TableLookup(TBLidx,time) 
                bum(i)=UWsp 
            ELSEIF (JB(i)%bFlag=='W') THEN
                jidx=JB(i)%jidx
                TBLidx=JB(i)%uwff
                UWsp=TableLookup(TBLidx,time) 
                bum(i)=UWsp/(JArea(jidx)*rhoJun(jidx))
            END IF
        END DO

       DO i=1,juncs
           IF (JuncClass(i)>0_sik) THEN
               ivi = jci(i,1)
               ovi = jci(i,2)
               Fbody=(-9.81_sdk*JHeight(i)/JLength(i))*rhoJun(i)*JLength(i)*JArea(i)
               Fdp=(Pm(ivi)-Pm(ovi))*JArea(i)
               IF (dt>small) THEN
                   Fdt= rhoJun0(i)*um0(i)*JArea(i)*JLength(i)/dt
               ELSE
                   Fdt=0.0_sdk
               END IF
               bum(i)=Fbody+Fdp+Fdt
           END IF 
       END DO

    END SUBROUTINE setupbum
!****************************************************************************
    SUBROUTINE setupbPmc

        IMPLICIT NONE

        INTEGER(sik) :: i
        INTEGER(sik) :: j
        INTEGER(sik) :: vidx
        INTEGER(sik) :: TBLidx
        INTEGER(sik) :: nij
        INTEGER(sik) :: noj
        INTEGER(sik) :: iji
        INTEGER(sik) :: oji
!        INTEGER(sik) :: nig
!        INTEGER(sik) :: nog
!        INTEGER(sik) :: igi
!        INTEGER(sik) :: ogi

        REAL(sdk)    :: Psp
        REAL(sdk)    :: Win
        REAL(sdk)    :: Wout
        REAL(sdk)    :: timeder

        DO i=1,numBB
            vidx=BB(i)%vidx
            TBLidx=BB(i)%Pmff
            Psp=TableLookup(TBLidx,time) 
            bpmc(vidx)=psp-pm(vidx)
        END DO

        DO i=1,vols
            IF (Volume(i)/=0.0_sdk) THEN
                nij=vciNT(i)
                noj=vciNF(i)
!                nig=gvciNT(i)
!                nog=gvciNF(i)
                Win=0.0_sdk
                Wout=0.0_sdk

                DO j=1,nij
                    iji=vciT(i,j)
                    Win=Win+rhoJun(iji)*umg(iji)*JArea(iji)
                END DO
                DO j=1,noj
                    oji=vciF(i,j)
                    Wout=Wout+rhoJun(oji)*umg(oji)*JArea(oji)
                END DO

!                DO j=1,nig
!                   igi = gvciT(i,j)
!                   ivi = gci(igi,1)
!                   Win=Win+rhoGap(igi)*SCvg(igi)*WGap(igi)*LGap(igi)
!                END DO
!
!                DO j=1,nog
!                    ogi = gvciF(i,j)
!                    ovi = gci(ogi,2)
!                    Wout=Wout+rhoGap(ogi)*SCvg(ogi)*WGap(ogi)*LGap(ogi)
!                END DO

                IF (dt<0.0001_sdk) THEN
                    timeder=0.0_sdk
                ELSE
                    timeder=(rhoJun0(i)-rhoJun(i))*VArea(i)*Length(i)/dt
                END IF
                bpmc(i)=timeder+Win-Wout

            END IF
        END DO
    END SUBROUTINE setupbPmc
!****************************************************************************
    SUBROUTINE Calcuvmc

        IMPLICIT NONE

        INTEGER(sik) :: i
        INTEGER(sik) :: ivi
        INTEGER(sik) :: ovi

        DO i=1,juncs
            ivi = jci(i,1)
            ovi = jci(i,2)
            umc(i)=(pmc(ivi)-pmc(ovi))*dum(i)
        END DO
!        DO i=1,vjuncs
!            ivi = gci(i,1)
!            ovi = gci(i,2)
!            SCvc(i)=(SCpc(ivi)-SCpc(ovi))*SCdv(i)
!        END DO

    END SUBROUTINE Calcuvmc
!****************************************************************************
    SUBROUTINE CalcResid(m,n,a,ia,ja,x,b,resid,idx)

        IMPLICIT NONE

! THE PURPOSE OF THIS ROUTINE IS to calculate the residual for a field variable.
!   This routine is used to calculate the residuals for all field variables
!   except for continuity.
!
! Programmed by ;
!   NAME         : Tim Drzewiecki
!   ORGANIZATION : University of Michigan
!   DATE         : 05/10/2012
!
! Subroutine Argument Descriptions:
!
! Variable Declarations;
!   Subroutine Arguments:
!   Local Variables:
!ARGUEMENTS
         INTEGER(sik),INTENT(IN)                :: m
         INTEGER(sik),INTENT(IN)                :: n
         REAL(sdk),DIMENSION(n),INTENT(IN)      :: a
         INTEGER(sik),DIMENSION(m+1),INTENT(IN) :: ia
         INTEGER(sik),DIMENSION(n),INTENT(IN)   :: ja
         INTEGER(sik),INTENT(OUT)               :: idx
         REAL(sdk),DIMENSION(m),INTENT(IN)      :: x
         REAL(sdk),DIMENSION(m),INTENT(IN)      :: b
         REAL(sdk),INTENT(OUT)                  :: resid
!LOCAL VARIABLES
         REAL(sdk) :: num
         REAL(sdk) :: numT
         REAL(sdk) :: numTMax
         REAL(sdk) :: den
         REAL(sdk) :: temp
         INTEGER(sik) :: i
         INTEGER(sik) :: j
         INTEGER(sik) :: k
         INTEGER(sik) :: elem1
         INTEGER(sik) :: elem2

         num = 0.0_sdk
         den = 0.0_sdk
         numTMax=0.0_sdk
         DO i=1,m
             elem1 = ia(i)
             elem2 = ia(i+1) - 1
             temp = 0.0_sdk
             DO j=elem1,elem2
                 k = ja(j)
                 IF (k==i) THEN
                     temp=temp-a(j)*x(k)
                     den=den+ABS(a(j)*x(k))
                 ELSE
                     temp=temp-a(j)*x(k)
                 END IF
             END DO
             numT=ABS(temp+b(i))
             num=num+numT
             IF (numT > numTMax) THEN
                 numTMax=numT
                 idx=i
             END IF
         END DO

         IF (den<1.0_sdk) THEN
             resid = num
         ELSE
             resid = num/den
         END IF

    END SUBROUTINE CalcResid
!****************************************************************************
    SUBROUTINE CalcResidC

        IMPLICIT NONE

! THE PURPOSE OF THIS ROUTINE IS to calculate the residual for continuity.
!
! Programmed by ;
!   NAME         : Tim Drzewiecki
!   ORGANIZATION : University of Michigan
!   DATE         : 05/10/2012
!
! Subroutine Argument Descriptions:
!
! Variable Declarations;
!   Subroutine Arguments:
!   Local Variables:
        INTEGER(sik) :: i

        residc = 0.0_sdk
        DO i=1,vols
            residc=residc+ABS(bpmc(i))
        END DO
        residc=residc/vols

    END SUBROUTINE CalcResidC
!****************************************************************************
    SUBROUTINE limitVars

        IMPLICIT  NONE
        INTEGER(sik) :: i
        DO i=1,juncs
            IF (um(i)>umax) um(i)=umax
            IF (um(i)<umin) um(i)=umin
        END DO

!        DO i=1,vjuncs
!            IF (SCvg(i)>vmax) SCvg(i)=vmax
!            IF (SCvg(i)<vmin) SCvg(i)=vmin
!        END DO

        DO i=1,vols
            IF (hm(i)>hmax) hm(i)=hmax
            IF (hm(i)<hmin) hm(i)=hmin
            IF (pm(i)>pmax) pm(i)=pmax
            IF (pm(i)<pmin) pm(i)=pmin
        END DO

    END SUBROUTINE limitVars
!****************************************************************************
    SUBROUTINE setupAhm
 
        IMPLICIT NONE
        INTEGER(sik) :: i
        INTEGER(sik) :: j
        INTEGER(sik) :: k
        INTEGER(sik) :: nij
        INTEGER(sik) :: noj
        INTEGER(sik) :: iji
        INTEGER(sik) :: oji
        INTEGER(sik) :: ivi
        INTEGER(sik) :: ovi
!        INTEGER(sik) :: nig
!        INTEGER(sik) :: nog
!        INTEGER(sik) :: igi
!        INTEGER(sik) :: ogi
 
        REAL(sdk)    :: Fin
        REAL(sdk)    :: Fout
        REAL(sdk)    :: Din
        REAL(sdk)    :: Dout
        REAL(sdk)    :: Aiv
        REAL(sdk)    :: Aov
        REAL(sdk)    :: Avc
        REAL(sdk)    :: timeder
 !       REAL(sdk)    :: qwall
 
        k=1_sik
        DO i=1,vols
            IF (Volume(i) == 0.0_sdk) THEN
                Ahm(k)=1.0_sdk
                jAhm(k)=i
                k=k+1_sik
            ELSE
                nij=vciNT(i)  ! number of inlet junctions
                noj=vciNF(i)  ! number of outlet junctions
!                nig=gvciNT(i) ! number of inlet gaps
!                nog=gvciNF(i) ! number of outlet gaps
                Avc=0.0_sdk
 
                DO j=1,nij
                    iji=vciT(i,j)
                    ivi=jci(iji,1)
                    Fin=WJ(iji)
                    Aiv=MAX(Fin,0.0_sdk)
                    Avc=Avc+Aiv-Fin
                    Ahm(k)=-Aiv
                    jAhm(k)=ivi
                    k=k+1_sik
                END DO
 
                DO j=1,noj
                    oji = vciF(i,j)
                    ovi = jci(oji,2)
                    Fout=WJ(oji)
                    Aov=MAX(0.0_sdk,-Fout)
                    Avc=Avc+Aov+Fout
                    Ahm(k)=-Aov
                    jAhm(k)=ovi
                    k=k+1_sik
                END DO
 
 !               DO j=1,nig
 !                   igi=gvciT(i,j)
 !                   ivi=gci(igi,1)
 !                   Fin=SCWG(igi)*SCCp(ivi)
 !                   Aiv=MAX(Fin,0.0_sdk)
 !                   Avc=Avc+Aiv-Fin
 !                   SCAT(k)=-Aiv
 !                   SCjAT(k)=ivi
 !                   k=k+1_sik
 !               END DO
 !
 !               DO j=1,nog
 !                   ogi=gvciF(i,j)
 !                   ovi=gci(ogi,2)
 !                   Fout=SCWG(ogi)*SCCp(i)
 !                   Aov=MAX(-Fout,0.0_sdk)
 !                   Avc=Avc+Aov+Fout
 !                   SCAT(k)=-Aov
 !                   SCjAT(k)=ovi
 !                   k=k+1_sik
 !               END DO
 
               !Time derivative term
                IF (dt<0.0001_sdk) THEN
                    timeder=0.0_sdk
                ELSE
                    timeder=rhomVol(i)*Volume(i)/dt
                END IF
                Avc=Avc+timeder
 
                Ahm(k)=Avc
                jAhm(k)=i
                k=k+1_sik
 
            END IF
        END DO
 
    END SUBROUTINE setupAhm
!****************************************************************************
!     SUBROUTINE setupSCbTq
!
!        USE ComGeo
!        USE Param
!
!        IMPLICIT NONE
!        INTEGER(sik) :: i
!        INTEGER(sik) :: j
!        INTEGER(sik) :: vidx
!        INTEGER(sik) :: sidx
!
!        REAL(sdk) :: Ts
!
!        DO i=1,vols
!            IF (VShape(i)=='C') THEN
!                DO j=1,2
!                    sidx=F2SCI(i,j)
!                    IF (sidx>0_sik) THEN
!                        Ts = TCCWCM(sidx,j)
!                        SCbTq(i,j)=HTC(i,j)*Ts*HTArea(i,j)
!                    END IF
!                END DO
!            ELSEIF (VShape(i)=='S') THEN
!                DO j=1,2
!                    sidx=F2SCI(i,j)
!                    IF (sidx>0_sik) THEN
!                        Ts = THWCM(sidx)
!                        SCbTq(i,j)=HTC(i,j)*Ts*HTArea(i,j)
!                    END IF
!                END DO
!            END IF
!        END DO
!
!    END SUBROUTINE setupSCbTq
!****************************************************************************
    SUBROUTINE setupbhm

        IMPLICIT NONE

        INTEGER(sik) :: i
        INTEGER(sik) :: j
        INTEGER(sik) :: vidx
        INTEGER(sik) :: sidx
        INTEGER(sik) :: TBLidx
        REAL(sdk)    :: hsp
        REAL(sdk)    :: qheat
 
        DO i=1,numBB
            vidx=BB(i)%vidx
            TBLidx=BB(i)%hmff
            hsp=TableLookup(TBLidx,time)
            bhm(vidx)=hsp
        END DO

        DO i=1,numHB
            TBLidx=HB(i)%Qff
            qheat=TableLookup(TBLidx,time)
            DO j=1,HB(i)%Cells
                sidx=HB(i)%sidx(j)
                vidx=s2fci(sidx)
                bhm(vidx)=qheat*HB(i)%Qnorm(j)
            END DO
        END DO

!        DO i=1,vols
!            IF (Volume(i)/=0.0_sdk) THEN
!                IF (dt<0.0001_sdk) THEN
!                    timeder=0.0_sdk
!                ELSE
!                    timeder=rhomVol0(i)*Volume(i)*hm0(i)/dt
!                END IF
!                bhm(i)=timeder
!            END IF
!        END DO

    END SUBROUTINE setupbhm
!****************************************************************************
    SUBROUTINE UpdateArrays

        IMPLICIT NONE

! THE PURPOSE OF THIS ROUTINE IS to update the field variables and associatd
!   coefficient arrays
!
! Programmed by ;
!   NAME         : Tim Drzewiecki
!   ORGANIZATION : University of Michigan
!   DATE         : 05/10/2012
!
! Subroutine Argument Descriptions:
!
!
! Variable Declarations;
!   Subroutine Arguments:
!   Local Variables:

        CALL CalcQuality
        CALL CalcTemperature
        CALL CalcDensity 
        CALL CalcVF
        CALL CalcViscosity
        CALL SetupW
        CALL CalcReynolds
        CALL CalcDarcyF
        CALL CalcThCond
        CALL CalcVdj
        CALL SetupAum
        CALL sortja(Aum,jAum,iAum,juncs,nzAum)
        CALL SetupApm
        CALL sortja(Apm,jApm,iApm,vols,nzApm)
        CALL SetupAhm
        CALL sortja(Ahm,jAhm,iAhm,vols,nzAhm)

    END SUBROUTINE UpdateArrays
!****************************************************************************
END MODULE SIMPLE
