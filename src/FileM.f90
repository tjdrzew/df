MODULE File
  
    USE IntrType

    IMPLICIT NONE

    CHARACTER(len=1024)               :: OneLine
    CHARACTER(len=16)                 :: CardName
    CHARACTER(len=16)                 :: BlockName
    CHARACTER                         :: Probe
    CHARACTER(len=80)                 :: caseid
    CHARACTER(len=200)                :: InFileName
    CHARACTER(len=200)                :: OutFileName
    CHARACTER(LEN=3),DIMENSION(512)   :: InpDatTyp  
    CHARACTER(LEN=100),DIMENSION(512) :: InpDatEnt
    CHARACTER(LEN=1024)               :: InpLinCom
    INTEGER(sik)                      :: InFile
    INTEGER(sik)                      :: OutFile
    INTEGER(sik)                      :: NumDat 

END MODULE File
