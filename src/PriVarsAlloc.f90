SUBROUTINE PriVarsAlloc

    USE IntrType
    USE Allocs
    USE PriVars
    USE BlockStr
    USE Solver

    IMPLICIT NONE
!
! THE PURPOSE OF THIS ROUTINE IS to allocate the problem dependent memory 
! for the primary variables
!
! Programmed by ;
!   NAME         : Tim Drzewiecki
!   ORGANIZATION : University of Michigan  
!   DATE         : 08/15/2011
!
! Subroutine Argument Descriptions:
!   N/A
!
! Variable Declarations;
!   Subroutine Arguments:
!    N/A
!   Local Variables:
    INTEGER(sik)           :: i     !Loop Control Variable
    INTEGER(sik)           :: j     !Loop Control Variable

   !Count the volumes and junction in the Blocks
    vols  = NumBB 
    juncs = NumJB 
    DO i = 1,NumFB
        vols  = vols  + FB(i)%Cells
        juncs = juncs + FB(i)%Cells - 1_sik
    END DO

  !Count the number of solid volumes
   svols=0_sik
   DO i=1,NumHB
       svols=HB(i)%Cells
   END DO

   !VOLUME PARAMETERS
    CALL dmalloc(Volume,vols)
    CALL dmalloc(VArea,vols)
    CALL dmalloc(Length,vols)
    CALL dmalloc(Height,vols)
    CALL dmalloc(HydDia,vols)
    CALL dmalloc(Roughness,vols)
    CALL dmalloc(qual,vols)
    CALL dmalloc(alpha,vols)
    CALL dmalloc(Pm,vols)
    CALL dmalloc(Pmc,vols)
    CALL dmalloc(hm,vols)
    CALL dmalloc(hmg,vols)
    CALL dmalloc(hm0,vols)
    CALL dmalloc(hg,vols)
    CALL dmalloc(hf,vols)
    CALL dmalloc(Tm,vols)
    CALL dmalloc(rhogVol,vols)
    CALL dmalloc(rhofVol,vols)
    CALL dmalloc(rhomVol,vols)
    CALL dmalloc(rhomVol0,vols)
    CALL dmalloc(viscgVol,vols)
    CALL dmalloc(viscfVol,vols)
    CALL dmalloc(viscmVol,vols)
    CALL dmalloc(VdjVol,vols)
    CALL dmalloc(Reyn,vols)
    CALL dmalloc(FDarcy,vols)
    CALL dmalloc(W,vols)
    CALL dmalloc(ThCond,vols)

   !JUNCTION PARAMETERS
    CALL dmalloc(JArea,juncs)
    CALL dmalloc(JLength,juncs)
    CALL dmalloc(JHeight,juncs)
    CALL dmalloc(JHydDia,juncs)
    CALL dmalloc(KFLoss,juncs)
    CALL dmalloc(KRLoss,juncs)
    CALL dmalloc(JuncClass,juncs)
    ALLOCATE(JForm(juncs))
    JForm=''
    CALL dmalloc(rhoJun,juncs)
    CALL dmalloc(rhoJun0,juncs)
    CALL dmalloc(viscJun,juncs)
    CALL dmalloc(VdjJun,juncs)
    CALL dmalloc(JReyn,juncs)
    CALL dmalloc(um,juncs)
    CALL dmalloc(umg,juncs)
    CALL dmalloc(umc,juncs)
    CALL dmalloc(um0,juncs)
    CALL dmalloc(FDarcyJ,juncs)
    CALL dmalloc(WJ,juncs)
    CALL dmalloc(ThCondJ,juncs)

   !VOLUME COEFFICIENT MATRICES AND SOURCE VECTORS
    CALL dmalloc(iAalpha,(vols+1))
    CALL dmalloc(balpha,vols)
    CALL dmalloc(iApm,(vols+1))
    CALL dmalloc(bpmc,vols)
    CALL dmalloc(iAhm,(vols+1))
    CALL dmalloc(bhm,vols)

   !JUNCTION COEFFICIENT MATRICES AND SOURCE VECTORS
    CALL dmalloc(iAum,(juncs+1))
    CALL dmalloc(bum,juncs)
    CALL dmalloc(dum,juncs)

   !INDEXING INFORMATION
    CALL dmalloc(jci,juncs,2)
    CALL dmalloc(s2fci,svols)
    CALL dmalloc(vciNF,vols)
    CALL dmalloc(vciNT,vols)

   !Allocate memory for PARDISO
    CALL dmalloc(ptum,64)
    CALL dmalloc(ptpm,64)
    CALL dmalloc(pthm,64)  
    CALL dmalloc(ptalpha,64)
    CALL dmalloc(umparm,64)
    CALL dmalloc(pmparm,64)
    CALL dmalloc(hmparm,64)
    CALL dmalloc(alphaparm,64)   


END SUBROUTINE PriVarsAlloc
