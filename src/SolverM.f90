MODULE Solver

    USE IntrType

    IMPLICIT NONE

    INTEGER(sik) :: NumOfSlvrCards
    INTEGER(sik) :: maxit

    REAL(sdk) :: time

    REAL(sdk) :: tbegin
    REAL(sdk) :: tend
    REAL(sdk) :: dt
    REAL(sdk) :: epc
    REAL(sdk) :: epu
    REAL(sdk) :: epv
    REAL(sdk) :: eph
    REAL(sdk) :: epVF
    REAL(sdk) :: alphau
    REAL(sdk) :: alphav
    REAL(sdk) :: alphap
    REAL(sdk) :: alphah
    REAL(sdk) :: alphaVF
    REAL(sdk) :: umin
    REAL(sdk) :: umax
    REAL(sdk) :: vmin
    REAL(sdk) :: vmax
    REAL(sdk) :: hmin
    REAL(sdk) :: hmax
    REAL(sdk) :: pmin
    REAL(sdk) :: pmax

    REAL(sdk) :: residu
    REAL(sdk) :: residv
    REAL(sdk) :: residc
    REAL(sdk) :: residh
    REAL(sdk) :: residVF

   !SOLVER PARAMETERS
    TYPE :: STRUCTtcard
        INTEGER(sik) :: maxit
        REAL(sdk) :: tbegin
        REAL(sdk) :: tend
        REAL(sdk) :: dt
        REAL(sdk) :: epc
        REAL(sdk) :: epu
        REAL(sdk) :: epv
        REAL(sdk) :: eph
        REAL(sdk) :: epVF
        REAL(sdk) :: alphau
        REAL(sdk) :: alphav
        REAL(sdk) :: alphap
        REAL(sdk) :: alphah
        REAL(sdk) :: alphaVF
        REAL(sdk) :: umin
        REAL(sdk) :: umax
        REAL(sdk) :: vmin
        REAL(sdk) :: vmax
        REAL(sdk) :: hmin
        REAL(sdk) :: hmax
        REAL(sdk) :: pmin
        REAL(sdk) :: pmax
    END TYPE STRUCTtcard

    TYPE (STRUCTtcard), POINTER, DIMENSION(:) :: TCards

   !PARDISO
    INTEGER(sik), POINTER, DIMENSION(:) :: ptum
    INTEGER(sik), POINTER, DIMENSION(:) :: ptpm
    INTEGER(sik), POINTER, DIMENSION(:) :: pthm
    INTEGER(sik), POINTER, DIMENSION(:) :: ptalpha
    INTEGER(sik), POINTER, DIMENSION(:) :: umparm
    INTEGER(sik), POINTER, DIMENSION(:) :: pmparm
    INTEGER(sik), POINTER, DIMENSION(:) :: hmparm
    INTEGER(sik), POINTER, DIMENSION(:) :: alphaparm

CONTAINS

    SUBROUTINE sortja(coefa,ja,ia,n,nz)

! THE PURPOSE OF THIS ROUTINE IS to order the coefficient and
!   column index matrices
!
! Programmed by ;
!   NAME         : Tim Drzewiecki
!   ORGANIZATION : University of Michigan
!   DATE         : 05/10/2012
!
! Subroutine Argument Descriptions:
!
!
! Variable Declarations;
!   Subroutine Arguments:
!   Local Variables:

        IMPLICIT NONE

!ARGUMENTS
        INTEGER(sik), INTENT(IN) :: n
        INTEGER(sik), INTENT(IN) :: nz
        INTEGER(sik), INTENT(IN), DIMENSION(n+1) :: ia
        INTEGER(sik), INTENT(INOUT), DIMENSION(nz) :: ja
        REAL(sdk), INTENT(INOUT), DIMENSION(nz) :: coefa

!LOCAL VARIABLES
        INTEGER(sik) :: i
        INTEGER(sik) :: j
        INTEGER(sik) :: k
        INTEGER(sik) :: elem1
        INTEGER(sik) :: elem2
        INTEGER(sik) :: nzr
        INTEGER(sik) :: minj
        INTEGER(sik) :: swp
        INTEGER(sik) :: tempi
        REAL(sdk)    :: tempr

        DO i=1,n
            elem1=ia(i)
            elem2=ia(i+1)-1
            nzr=elem2-elem1+1
            IF (elem1==elem2) THEN
                CYCLE
            ELSE
                DO j=1,(nzr-1)
                    swp=elem1+j-1
                    minj=ja(swp)
                    DO k=(elem1+j),elem2
                        IF (ja(k) < minj) THEN
                           minj=ja(k)
                           swp=k
                        END IF
                    END DO
                    tempi=ja(elem1+j-1)
                    ja(elem1+j-1)=ja(swp)
                    ja(swp)=tempi

                    tempr=coefa(elem1+j-1)
                    coefa(elem1+j-1)=coefa(swp)
                    coefa(swp)=tempr
                END DO
            END IF
        END DO

    END SUBROUTINE sortja

END MODULE Solver