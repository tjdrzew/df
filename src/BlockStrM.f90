MODULE BlockStr

    USE IntrType

    IMPLICIT NONE
!
! THIS MODULE STORES THE FOLLOWING VARIABLES AND 
! CONTAINS THE FOLLOWING ROUTINES for boundary conditions
! 
!
! VARIABLE NAME         DESCRIPTION
! -------------         --------------------------------------------------------!
!  
!  
! 
! 
!
! ROUTINES      SUBROUTINE(S)/FUNCTION(F)  SHORT DESCRIPTION        
! ------------  -------------------------  -------------------------------------!
!
    INTEGER(sik)                    :: NumFB
    INTEGER(sik)                    :: NumBB
    INTEGER(sik)                    :: NumJB
    INTEGER(sik)                    :: NumGB
    INTEGER(sik)                    :: NumHB
    CHARACTER(len=20)               :: InletPlenum
    CHARACTER(len=20)               :: OutletPlenum    

    TYPE FluidBlockT
        CHARACTER(len=20)            :: id
        INTEGER(sik)                 :: cells
        REAL(sdk)                    :: roughness
        REAL(sdk),POINTER            :: volume(:)=>NULL()
        REAL(sdk),POINTER            :: area(:)=>NULL()
        REAL(sdk),POINTER            :: length(:)=>NULL()
        REAL(sdk),POINTER            :: height(:)=>NULL()
        REAL(sdk),POINTER            :: hydDia(:)=>NULL()
        REAL(sdk),POINTER            :: Pm(:)=>NULL()
        REAL(sdk),POINTER            :: hm(:)=>NULL()
        REAL(sdk),POINTER            :: alpha(:)=>NULL()
        CHARACTER,POINTER            :: jForm(:)
        CHARACTER,POINTER            :: jFlag(:)
        REAL(sdk),POINTER            :: KLoss(:)=>NULL()
        REAL(sdk),POINTER            :: uw(:)=>NULL()
        INTEGER(sik),POINTER         :: vidx(:)=>NULL()
        INTEGER(sik),POINTER         :: jidx(:)=>NULL()
        INTEGER(sik),POINTER         :: sidx(:)=>NULL()
    END TYPE FluidBlockT

    TYPE BoundaryBlockT
        CHARACTER(len=20)            :: id
        CHARACTER(len=2)             :: bType
        INTEGER(sik)                 :: Pmff
        INTEGER(sik)                 :: hmff
        INTEGER(sik)                 :: alphaff
        INTEGER(sik)                 :: vidx
    END TYPE BoundaryBlockT

    TYPE JunctionBlockT
        CHARACTER(len=20)             :: id
        CHARACTER(len=8)              :: inFace
        CHARACTER(len=20)             :: inId
        CHARACTER(len=8)              :: exFace
        CHARACTER(len=20)             :: exId
        CHARACTER                     :: bFlag
        INTEGER(sik)                  :: uwff
        CHARACTER                     :: jForm
        CHARACTER                     :: jFlag
        REAL(sdk)                     :: KFLoss
        REAL(sdk)                     :: KRLoss
        REAL(sdk)                     :: uw
        INTEGER(sik)                  :: jidx
    END TYPE JunctionBlockT

    TYPE GapBlockT
        CHARACTER(len=20)             :: id
        CHARACTER(len=20)             :: inId
        CHARACTER(len=20)             :: exId
        INTEGER(sik)                  :: gaps
        INTEGER(sik)                  :: inCell1
        INTEGER(sik)                  :: inCell2
        INTEGER(sik)                  :: exCell1
        INTEGER(sik)                  :: exCell2
        REAL(sdk),POINTER             :: KLoss(:)=>NULL()
        REAL(sdk),POINTER             :: Width(:)=>NULL()
        CHARACTER,POINTER             :: gFlag(:)
        REAL(sdk),POINTER             :: uw(:)=>NULL()
        INTEGER(sik),POINTER          :: gidx(:)=>NULL()
    END TYPE GapBlockT

    TYPE HeatStructureBlockT
        CHARACTER(len=20)             :: id
        CHARACTER(len=20)             :: FBid
        CHARACTER                     :: Shape
        INTEGER(sik)                  :: Cells
        INTEGER(sik)                  :: Cell1
        INTEGER(sik)                  :: Cell2
        INTEGER(sik)                  :: Qff
        REAL(sdk),POINTER             :: Qnorm(:)=>NULL()
        INTEGER(sik),POINTER          :: sidx(:)=>NULL()
    END TYPE  HeatStructureBlockT
     
    TYPE(FluidBlockT),POINTER         :: FB(:)
    TYPE(BoundaryBlockT),POINTER      :: BB(:)
    TYPE(JunctionBlockT),POINTER      :: JB(:)
    TYPE(GapBlockT),POINTER           :: GB(:)
    TYPE(HeatStructureBlockT),POINTER :: HB(:)   

CONTAINS
!**********************************************************************
    SUBROUTINE FindBoundaryBlock(id,idx)

        IMPLICIT NONE

       !PARAMETERS
        CHARACTER(len=20), INTENT(in) :: id
        INTEGER(sik), INTENT(out)     :: idx

       !LOCAL VARIABLES
        INTEGER(sik)                  :: i    

        idx = -1_sik
        DO i=1,NumBB
            IF (id == BB(i)%id) THEN
                idx = i
                EXIT
            END IF        
        END DO

    END SUBROUTINE FindBoundaryBlock
!**********************************************************************
    SUBROUTINE FindFluidBlock(id,idx)

        IMPLICIT NONE

       !PARAMETERS
        CHARACTER(len=20), INTENT(in) :: id
        INTEGER(sik), INTENT(out)     :: idx

       !LOCAL VARIABLES
        INTEGER(sik)                  :: i    

        idx = -1_sik
        DO i=1,NumFB
            IF (id == FB(i)%id) THEN
                idx = i
                EXIT
            END IF        
        END DO

    END SUBROUTINE FindFluidBlock
!**********************************************************************
END MODULE BlockStr
